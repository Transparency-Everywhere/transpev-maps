<?php
	
	function authorized(){
		return true;
	}
	
	
	function strToGeolocation($country=Germany, $zip, $city, $street){
	    //this function uses the openstreetmap api to convert an adress
	    //into the geolocation
	    
	    
	    
	    //load xml data from openstreetmap.org
	    $adress= "$street $zip $city $country";
		$adress = htmlentities($adress);
		$adress = urlencode($adress);
	    $xml = simplexml_load_file("http://nominatim.openstreetmap.org/search?q=$adress&format=xml&polygon=1");
	    
	    //answer returns in some cases >1 value, so we only pay attention to the first([0]) place 
	    $xml=  $xml->place[0];
	    
	    
	    //define return values
	    
	      //get lat
	      $return[lat] = $xml['lat'];
	
	      //get lon
	      $return[lon] = $xml['lon'];
	
	
	      //get boundingbox(values of limitation coordinates for embed map)
	      $return[boundingbox] = $xml['boundingbox'];
	    
	      
	    return $return;
	}
	
	
	class categories{
		public $mysqli;
		function __construct() {
					$this->mysqli = new MySQLi(
					  '85.214.203.132',
					  'wilhelmsmap',
					  'asd456fgh765asd',
					  'wilhelmsmap'
					);
		}
		
		function insert($category, $title, $icon){
			mysql_query("INSERT INTO `categories` (`category`, `title`, `icon`) VALUES ('$category', '$title', '$icon')");
			return mysql_insert_id();
			
		}
		
		function update($id, $category, $title, $icon){
			if(mysql_query("UPDATE `categories` SET `category`='$category', `title`='$title', `icon`='$icon' WHERE `id` ='$id'")){
				return true;
			}
		}
		
		function delete($id){
			$result = $this -> mysqli->query("DELETE FROM `categories` WHERE id='$id'");
			if($result){
				return true;
			}
			
		}
		
		function get($id=NULL, $type=Null){
			
			if($type == "json"){
				if(!empty($id)){
					
				}else{
					$result = $this -> mysqli->query('SELECT * FROM categories');
					if ($result) {
					  while ($row = $result->fetch_assoc()) {
					  	
						//check if category has children to set var children true or false
						$checkResult = $this -> mysqli->query("SELECT id FROM categories WHERE category='$row[id]'");
						$checkResultData = $checkResult->fetch_assoc();
						
						$row[children] = $checkResult->num_rows;
						
						//stupid case
						if($checkResultData[id] == $row[id]){
							$row[children]--;
						}
						
					    $json[] = $row;
					  }
					  
					  echo json_encode($json);
					 
					  $result->close();
					}
					
				}
			}else{
				
					$result = $this -> mysqli->query('SELECT * FROM categories');
					if ($result) {
					  while ($row = $result->fetch_assoc()) {
					  	
						$return[] = $row;
						
					  }
					}
					
					$result->close();
					
					return $return;
				
			}
		}

		function showDropdown($preselected=null){
			
			$locations = $this->get();
			
			echo"<select name=\"category\">";
			foreach($locations AS $location){
				if($preselected == $location)
					$selected = 'selected="selected"';
				else
					unset($selected);
				echo"<option value=\"$location[id]\" $selected>";
				echo"$location[title]";
				echo"</option>";
			}
			echo"</select>";
			
		}
	}
	
	
	class locations{
		
		public $mysqli;
		function __construct() {
					$this->mysqli = new MySQLi(
					  '85.214.203.132',
					  'wilhelmsmap',
					  'asd456fgh765asd',
					  'wilhelmsmap'
					);
		}
		
		function insert($category, $title, $street, $streetNo, $zip, $city, $hours){
			
			$loc = strToGeolocation("", $zip, $city, "$street $streetNo");	
				
			mysql_query("INSERT INTO `locations` (`category`, `title`, `street`, `streetNo`, `zip`, `city`, `hours`, `lon`, `lat`) VALUES ('$category', '$title', '$street', '$streetNo', '$zip', '$city', '$hours', '$loc[lon]', '$loc[lat]')");
			return mysql_insert_id();
		}
		
		function update($id, $category, $title, $street, $streetNo, $zip, $city, $hours){
			
			$loc = strToGeolocation('', $zip, $city, "$street $streetNo");
			
			if(mysql_query("UPDATE `locations` SET  `category` =  '$category', `title` =  '$title', `street` =  '$street', `streetNo` =  '$streetNo', `zip` =  '$zip', `city` =  '$city',`hours` = '$hours',`lon` = '$loc[lon]',`lat` =  '$loc[lat]' WHERE `id` ='$id'")){
				return true;
			}
		}
		
		function delete($id){
			
			$result = $this -> mysqli->query("DELETE FROM `locations` WHERE id='$id'");
			if($result){
				return true;
			}
			
		}
		
		function get($id=NULL){
			
			if(!empty($id)){
				$result = $this->mysqli->query("SELECT * FROM locations WHERE id='$id'");
				if ($result) {
				  while ($row = $result->fetch_assoc()) {
				    $return = $row;
				  }
				  
				  return $return;
				 
				  $result->close();
				}
				
			}else{
				$result = $this->mysqli->query('SELECT * FROM locations');
				if ($result) {
				  while ($row = $result->fetch_assoc()) {
				    $return[] = $row;
				  }
				  
				  return $return;
				 
				  $result->close();
				}
				
			}
		}
		
		function show($id=NULL){
			
			if(!empty($id)){
				
			}else{
				$result = $this->mysqli->query('SELECT * FROM locations');
				if ($result) {
				  while ($row = $result->fetch_assoc()) {
				    $json[] = $row;
				  }
				  
				  echo json_encode($json);
				 
				  $result->close();
				}
				
			}
		}
	}

	class customers{
		
		public $mysqli;
		function __construct() {
					$this->mysqli = new MySQLi(
					  '85.214.203.132',
					  'wilhelmsmap',
					  'asd456fgh765asd',
					  'wilhelmsmap'
					);
					
					
					session_start();
		}
		
		function insert($email, $password, $salutation ,$name ,$surname ,$street , $streetNo, $zip, $city){
			
				$password = sha1($password);
				if (true) {
					
					mysql_query("INSERT INTO `customers` (`id`, `email`, `password`, `salutation`, `name`, `surname`, `street`, `streetNo`, `zip`, `city`) VALUES (NULL, '$email', '$password', '$salutation', '$name', '$surname', '$street', '$streetNo', '$zip', '$city');");
					return mysql_insert_id();
					
				}
				
			
		}
		
		function update($id, $salutation ,$name ,$surname ,$street , $streetNo, $zip, $city){
			
			if(mysql_query("UPDATE `customers` SET  `salutation` =  '$salutation', `name` =  '$name', `surname` =  '$surname',`street` =  '$street', `streetNo` =  '$streetNo',`zip` =  '$zip', `city` =  '$city' WHERE `id` ='$id'")){
				return true;
			}
					
		}
		
		function updateMail($id, $email){
			
			if(mysql_query("UPDATE `customers` SET  `email` =  '$email' WHERE `id` ='$id'")){
				return true;
			}
			
		}
		
		function updatePassword($id, $passwordOld, $passwordNew){
			
				$result = $this->mysqli->query("SELECT `id`, `password` FROM `customers` WHERE `id`='".mysql_real_escape_string($id)."'");
				$row = $result->fetch_assoc();
				if($row[password] == sha1($passwordOld)){
					
					$passwordNew = sha1($passwordNew);
					if(mysql_query("UPDATE `customers` SET  `password` =  '$passwordNew' WHERE `id` ='$id'")){
						return true;
					}
				}else{
					return "wrong password";
				}
			
		}
		
		function updateBankInformation($id, $bankName, $BIC, $IBAN){
			
			if(mysql_query("UPDATE `customers` SET  `bInfo_bankName` =  '$bankName', `bInfo_bankBIC` =  '$BIC', `bInfo_IBAN` =  '$IBAN' WHERE `id` ='$id'")){
				return true;
			}
			
		}
		
		
		function get($id=NULL){
			
			if(!empty($id)){
				$result = $this->mysqli->query("SELECT * FROM customers WHERE id='$id'");
				if ($result) {
				  while ($row = $result->fetch_assoc()) {
				    $return = $row;
				  }
				  
				  return $return;
				 
				  $result->close();
				}
				
			}else{
				$result = $this->mysqli->query('SELECT * FROM customers');
				if ($result) {
				  while ($row = $result->fetch_assoc()) {
				    $return[] = $row;
				  }
				  
				  return $return;
				 
				  $result->close();
				}
				
			}
		}
		function authorize($email, $password){
			
				$result = $this->mysqli->query("SELECT id, password FROM customers WHERE email='".mysql_real_escape_string($email)."'");
				$row = $result->fetch_assoc();
				if($row[password] == sha1($password)){
					
					$_SESSION['customer'] = $row[id];
					$_SESSION['hash'] = sha1($password);
					
					return true;
				}else
					return false;
			
		}
		
		function authorized(){
			if(isset($_SESSION['customer']) && !empty($_SESSION['customer']))
				return true;
		}
	}
  ?>
