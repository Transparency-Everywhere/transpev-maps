var searchData=
[
  ['service_3a_20autoscout24',['Service: AutoScout24',['../as24.html',1,'']]],
  ['service_3a_20conference_20call',['Service: Conference Call',['../confcall.html',1,'']]],
  ['service_20environments',['Service Environments',['../developergarden_environments.html',1,'developergarden']]],
  ['sdk_20vs_20direct_20invocation',['SDK vs Direct Invocation',['../developergarden_sdk_vs_api.html',1,'developergarden']]],
  ['service_3a_20ip_20location',['Service: IP Location',['../iplocation.html',1,'']]],
  ['service_3a_20phone_20number_20validation_20_28smsvalidation_29',['Service: Phone Number Validation (SmsValidation)',['../phonenumbervalidation.html',1,'']]],
  ['service_3a_20administration_20_28quota_29',['Service: Administration (Quota)',['../quota.html',1,'']]],
  ['service_3a_20send_20mms',['Service: Send MMS',['../sendmms.html',1,'']]],
  ['service_3a_20global_20sms',['Service: Global SMS',['../sendsms.html',1,'']]],
  ['service_3a_20speech_20to_20text',['Service: Speech to Text',['../speech2text.html',1,'']]],
  ['service_3a_20voice_20call',['Service: Voice Call',['../voicecall.html',1,'']]]
];
