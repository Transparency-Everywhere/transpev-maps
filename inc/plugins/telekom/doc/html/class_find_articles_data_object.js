var class_find_articles_data_object =
[
    [ "__construct", "class_find_articles_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getBuild", "class_find_articles_data_object.html#a59e449ef0b0d191eead528ec959b3c74", null ],
    [ "getRequestId", "class_find_articles_data_object.html#ad7b1ab1c5a44c5672b2ffc46f7c062b1", null ],
    [ "getResponseStatus", "class_find_articles_data_object.html#a4aa92fe8e0e09564d44ec1cd3e44ec21", null ],
    [ "getRootPaths", "class_find_articles_data_object.html#acf025e017a7489e082c8b55a8fcc7517", null ],
    [ "getUsedVehicleSearchParameters", "class_find_articles_data_object.html#ac940ea8e0921b11806ee6c2cd1ec161c", null ],
    [ "getVehicles", "class_find_articles_data_object.html#aae82350b1d6e7fbf83683b70092f15a8", null ],
    [ "getVehiclesById", "class_find_articles_data_object.html#a16936630fe82916ce413c6269e50b266", null ],
    [ "getVehiclesFound", "class_find_articles_data_object.html#a13f19b44db7d6ab28c02d2ab7b079aac", null ],
    [ "$build", "class_find_articles_data_object.html#a502b15db084ae270b52e03c8075916de", null ],
    [ "$requestId", "class_find_articles_data_object.html#af08ad4863c91d7baccfd43e73bd6a1c7", null ],
    [ "$responseStatus", "class_find_articles_data_object.html#affd17c0644c7157ff189382c752133a1", null ],
    [ "$rootPaths", "class_find_articles_data_object.html#a436baeaf59261b9f3a36eaf2fe5bf53d", null ],
    [ "$usedVehicleSearchParameters", "class_find_articles_data_object.html#a27d7b4fdf74233444a31578a383ff88f", null ],
    [ "$vehicles", "class_find_articles_data_object.html#a7cc33344b6af08c8c162455be73338b2", null ],
    [ "$vehiclesFound", "class_find_articles_data_object.html#abf828d810451694aacb4e9a27606ab8a", null ]
];