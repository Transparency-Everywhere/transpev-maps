var class_send_sms_receive_parameters =
[
    [ "__construct", "class_send_sms_receive_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "getParametersArray", "class_send_sms_receive_parameters.html#ac62710dabfc7938b17c2dc35c0284c0a", null ],
    [ "getRegistrationId", "class_send_sms_receive_parameters.html#aae9f4de68e11a7f8c93b0360d1b29a2a", null ],
    [ "hasRequiredFields", "class_send_sms_receive_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAccount", "class_send_sms_receive_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setMaxBatchSize", "class_send_sms_receive_parameters.html#a6b1f3e3e009c84362de25679ed2d3ce2", null ],
    [ "setRegistrationId", "class_send_sms_receive_parameters.html#a43853c51555d7fae150e420bfc9b0740", null ]
];