var class_location_data_object =
[
    [ "__construct", "class_location_data_object.html#a8a96949cb203568ae3771d7b511a53e0", null ],
    [ "getCountryCode", "class_location_data_object.html#a8fe94d882c3cee6143e9eac8c0a7e715", null ],
    [ "getIpAddress", "class_location_data_object.html#a16029531e7dd7d43f777ea9d24359942", null ],
    [ "getRadius", "class_location_data_object.html#a46ed26029924e916f771089ac7c3291f", null ],
    [ "getRegionCode", "class_location_data_object.html#a89ed99a52113e6e96639ae4987c504f6", null ],
    [ "getRegionName", "class_location_data_object.html#a340dd2f0440915114dc349918093f478", null ]
];