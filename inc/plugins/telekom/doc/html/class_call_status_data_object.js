var class_call_status_data_object =
[
    [ "__construct", "class_call_status_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getBe164", "class_call_status_data_object.html#a7b3557a88b16bd657fb0d6fb853f14d8", null ],
    [ "getBindex", "class_call_status_data_object.html#a1f3e428f650ce150332a30003df653c3", null ],
    [ "getConnectionTimeA", "class_call_status_data_object.html#a973e98721259435efef0a0ca04210341", null ],
    [ "getConnectionTimeB", "class_call_status_data_object.html#a693113c1d313db32e913078ac320e776", null ],
    [ "getDescriptionA", "class_call_status_data_object.html#ace74a3032a032e8ef8b9752f6fb9554b", null ],
    [ "getDescriptionB", "class_call_status_data_object.html#a3e87b06625bc3905230206ea8d27ec66", null ],
    [ "getReasonA", "class_call_status_data_object.html#ab7d27f169023fa491a0b391a0d653aac", null ],
    [ "getReasonB", "class_call_status_data_object.html#a119967adc7cd7b2ac8000275614a7c5f", null ],
    [ "getStateA", "class_call_status_data_object.html#abd77e576094f98a95791d54d38eb20a7", null ],
    [ "getStateB", "class_call_status_data_object.html#ae075fe03e5565856de9426ab2a1df30d", null ]
];