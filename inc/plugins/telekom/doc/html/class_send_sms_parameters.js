var class_send_sms_parameters =
[
    [ "__construct", "class_send_sms_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "getParametersArray", "class_send_sms_parameters.html#ac62710dabfc7938b17c2dc35c0284c0a", null ],
    [ "hasRequiredFields", "class_send_sms_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAccount", "class_send_sms_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setAddress", "class_send_sms_parameters.html#af38dae8c3d156194affe2d55ba7e6ee7", null ],
    [ "setCallbackData", "class_send_sms_parameters.html#a6221fa3aa9b16b6376e4718035aa4cec", null ],
    [ "setClientCorrelator", "class_send_sms_parameters.html#a06714fd02019903a008861963a1954ed", null ],
    [ "setMessage", "class_send_sms_parameters.html#a6991eb53548e7180a3a8e6f418fbb234", null ],
    [ "setMessageType", "class_send_sms_parameters.html#acf90a2701912e7887ffc5cd43c4feab0", null ],
    [ "setSenderAddress", "class_send_sms_parameters.html#a6160ea8b50c9c16a5b5baf7062169c4d", null ]
];