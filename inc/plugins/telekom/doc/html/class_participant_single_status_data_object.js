var class_participant_single_status_data_object =
[
    [ "__construct", "class_participant_single_status_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getInitiatorMuted", "class_participant_single_status_data_object.html#a1f753ab941e7e23bf4b2b6403dc4eb9e", null ],
    [ "getLastAccessTime", "class_participant_single_status_data_object.html#aee9346bfa38a3d899ee84725bb1f9e55", null ],
    [ "getLastReason", "class_participant_single_status_data_object.html#abccf4d1cf3e9612ab8e802348a0401ad", null ],
    [ "getMuted", "class_participant_single_status_data_object.html#a5348a8ff01973c92633f45e20d505e07", null ],
    [ "getNumber", "class_participant_single_status_data_object.html#a489ab44b15f7f39df2904e0bcdfc8955", null ],
    [ "getPrevStatus", "class_participant_single_status_data_object.html#aec028e885c3ac8fb9c24035de65d79c0", null ],
    [ "getStatus", "class_participant_single_status_data_object.html#a9d21636071f529e2154051d3ea6e5921", null ]
];