var class_conference_announcement_client =
[
    [ "deleteAnnouncementSet", "class_conference_announcement_client.html#a333a65dfcedff9aaf9ebf0880919875a", null ],
    [ "getAnnouncementSets", "class_conference_announcement_client.html#a5ca5e143f5505635f7b167ffa8f25490", null ],
    [ "uploadAnnouncement", "class_conference_announcement_client.html#a06418fc1f2f3b576ca63c3cbe6aae704", null ],
    [ "RESPONSE_KEY_ANNOUNCEMENT_SET", "class_conference_announcement_client.html#a29abdcb603990e6525327a717e33da29", null ],
    [ "URL_KEY", "class_conference_announcement_client.html#ad704b183cd4f6d2ca73a09b1a14f4cba", null ]
];