var basicusage =
[
    [ "Package Contents", "basicusage_contents.html", null ],
    [ "Project Template", "basicusage_firstproject.html", [
      [ "Include Required Files", "basicusage_firstproject.html#basicusage_firstproject_include", null ],
      [ "Initialization", "basicusage_firstproject.html#basicusage_firstproject_init", null ],
      [ "Error Handling", "basicusage_firstproject.html#basicusage_firstproject_errorhandling", null ],
      [ "Billing Sub-Accounts", "basicusage_firstproject.html#basicusage_firstproject_accounts", null ]
    ] ],
    [ "Configure the SSL Certificate", "basicusage_certificate.html", [
      [ "Installing CA Bundle for Windows Apache", "basicusage_certificate.html#basicusage_certificate_install", null ],
      [ "Disable Certificate Validation", "basicusage_certificate.html#basicusage_certificate_disable", null ]
    ] ]
];