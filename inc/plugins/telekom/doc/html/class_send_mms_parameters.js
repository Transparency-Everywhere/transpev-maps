var class_send_mms_parameters =
[
    [ "__construct", "class_send_mms_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_send_mms_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAccount", "class_send_mms_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setAttachment", "class_send_mms_parameters.html#a87e25c54c819e3b969b699add73cd42d", null ],
    [ "setContentType", "class_send_mms_parameters.html#a945b1a5744b810fcb31a05f827356cfa", null ],
    [ "setFilename", "class_send_mms_parameters.html#ab441a5ddefb06b8324cc8e8a583295a9", null ],
    [ "setMessage", "class_send_mms_parameters.html#a6991eb53548e7180a3a8e6f418fbb234", null ],
    [ "setNumber", "class_send_mms_parameters.html#a097c2c268472e0d57ea04eaf3d88b9e2", null ],
    [ "setOriginator", "class_send_mms_parameters.html#ae947753bc6793650711cf2c200d8066c", null ],
    [ "setSubject", "class_send_mms_parameters.html#a971f2abb85a2e0fc85954322896d6b80", null ]
];