var class_send_number_for_validation_parameters =
[
    [ "__construct", "class_send_number_for_validation_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_send_number_for_validation_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAccount", "class_send_number_for_validation_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setMessage", "class_send_number_for_validation_parameters.html#a6991eb53548e7180a3a8e6f418fbb234", null ],
    [ "setNumber", "class_send_number_for_validation_parameters.html#a097c2c268472e0d57ea04eaf3d88b9e2", null ],
    [ "setOriginator", "class_send_number_for_validation_parameters.html#ae947753bc6793650711cf2c200d8066c", null ]
];