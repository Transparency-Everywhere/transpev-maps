var class_conference_call_status_data_object =
[
    [ "__construct", "class_conference_call_status_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getAccount", "class_conference_call_status_data_object.html#a36547c0dee37da704ddb0115dfe5e021", null ],
    [ "getDetail", "class_conference_call_status_data_object.html#affd2d36f02618aa2972a9ee5cc2a6f69", null ],
    [ "getParticipants", "class_conference_call_status_data_object.html#ae6148d07b08e1a1d5f6b58d561ac7bd1", null ],
    [ "getSchedule", "class_conference_call_status_data_object.html#a3dbae8cba869fb348612f375aab7f350", null ],
    [ "getStartTime", "class_conference_call_status_data_object.html#a568a7fafd601b520ff8c4bd91c962e29", null ],
    [ "$account", "class_conference_call_status_data_object.html#a19ec872392da5d6131f9ad03b6c98339", null ],
    [ "$detail", "class_conference_call_status_data_object.html#aa7315da686d1ed5995019b7a00057ea4", null ],
    [ "$participants", "class_conference_call_status_data_object.html#a6d19776f7785e12716cedd38fea82f88", null ],
    [ "$schedule", "class_conference_call_status_data_object.html#ab5dd2f4095f936782d11344a4293c3dd", null ],
    [ "$startTime", "class_conference_call_status_data_object.html#a404df05d2efcd6bb0551ecd8e7ae2315", null ]
];