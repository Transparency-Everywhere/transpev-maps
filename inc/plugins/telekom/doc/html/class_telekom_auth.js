var class_telekom_auth =
[
    [ "__construct", "class_telekom_auth.html#a41d781660dab58abf8ebb89cd59850c4", null ],
    [ "getAccessToken", "class_telekom_auth.html#a5d251c70a3f9f9daaff7f00ec5f894c0", null ],
    [ "getAccessTokenValidUntil", "class_telekom_auth.html#a3f0dbf378177e23670b2d3a9b79ef07b", null ],
    [ "hasValidToken", "class_telekom_auth.html#a087b074da32403801e71583a9f8fac2d", null ],
    [ "setAccessToken", "class_telekom_auth.html#ae8fe9044b119dc108d41fc282fa6350e", null ],
    [ "setAccessTokenValidUntil", "class_telekom_auth.html#ad06ef91819160604ba60937482ae9abe", null ],
    [ "$accessToken", "class_telekom_auth.html#a14696362b35353b3280dfbec3f88861d", null ],
    [ "$accessTokenValidUntil", "class_telekom_auth.html#a8276c44bc98bfe92489a4e6546dff510", null ],
    [ "$service", "class_telekom_auth.html#abb8d1943d2cf9b6a3d54a4fed1ad2d9d", null ]
];