var class_template_details_data_object =
[
    [ "__construct", "class_template_details_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getAnnouncementSet", "class_template_details_data_object.html#a750fef34f3be4af4e825da16bc9452ea", null ],
    [ "getDescription", "class_template_details_data_object.html#a2e7bb35c71bf1824456ceb944cb7a845", null ],
    [ "getDialInLocked", "class_template_details_data_object.html#a89ae7e39d19414ccde7ea632dc8dfdf8", null ],
    [ "getDuration", "class_template_details_data_object.html#ab46fe3d2750728c84bd71ae1bfd08ee8", null ],
    [ "getInitiatorAccessPin", "class_template_details_data_object.html#ae20ded9ecb581d5d52c1625ba0b255e3", null ],
    [ "getJoinConfirm", "class_template_details_data_object.html#a621e1ce426d7820ff845fce3eb911ea7", null ],
    [ "getLanguage", "class_template_details_data_object.html#afcef2403c4111bc44ef0530f1e493909", null ],
    [ "getName", "class_template_details_data_object.html#a3d0963e68bb313b163a73f2803c64600", null ],
    [ "getParticipantAccessPin", "class_template_details_data_object.html#a2ac9d32de56cec33bd78429cb7bc6631", null ],
    [ "getServiceCid", "class_template_details_data_object.html#af145ce34b9f366ade85fa680d7bed8ed", null ]
];