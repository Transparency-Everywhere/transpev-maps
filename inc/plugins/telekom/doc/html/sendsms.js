var sendsms =
[
    [ "Environmental information", "sendsms.html#environmental_infos", null ],
    [ "Difference between 'basic' and 'premium'", "sendsms.html#difference", null ],
    [ "Basic Information", "sendsms.html#basic_info", null ],
    [ "Code", "sendsms.html#code", null ],
    [ "Further Knowledge", "sendsms.html#further_knowledge", null ],
    [ "Features and Restrictions", "sendsms_features.html", null ],
    [ "Example: Send an SMS", "sendsms_code.html", [
      [ "Include Required Files", "sendsms_code.html#sendsms_code_include", null ],
      [ "Init the SDK Classes", "sendsms_code.html#sendsms_code_client", null ],
      [ "Fetch an Access Token", "sendsms_code.html#sendsms_code_token", null ],
      [ "Send the SMS", "sendsms_code.html#sendsms_code_send", null ]
    ] ],
    [ "Example: Subscribe to Notifications", "sendsms_notification_subscribe.html", [
      [ "Init the SDK Client", "sendsms_notification_subscribe.html#sendsms_notification_subscribe_client", null ],
      [ "Prepare the Request", "sendsms_notification_subscribe.html#sendsms_notification_subscribe_prepare", null ],
      [ "Send the request", "sendsms_notification_subscribe.html#sendsms_notification_subscribe_send", null ]
    ] ],
    [ "Example: Unsubscribe to Notifications", "sendsms_notification_unsubscribe.html", [
      [ "Init the SDK Client", "sendsms_notification_unsubscribe.html#sendsms_notification_unsubscribe_client", null ],
      [ "Prepare the Request", "sendsms_notification_unsubscribe.html#sendsms_notification_unsubscribe_prepare", null ],
      [ "Send the request", "sendsms_notification_unsubscribe.html#sendsms_notification_unsubscribe_send", null ]
    ] ],
    [ "Example: Query Report", "sendsms_queryreport.html", [
      [ "Init the SDK Client", "sendsms_queryreport.html#sendsms_queryreport_client", null ],
      [ "Prepare the Request", "sendsms_queryreport.html#sendsms_queryreport_prepare", null ],
      [ "Send the request", "sendsms_queryreport.html#sendsms_queryreport_send", null ]
    ] ],
    [ "Example: Receive an SMS", "sendsms_receive.html", [
      [ "Init the SDK Client", "sendsms_receive.html#sendsms_receive_client", null ],
      [ "Prepare the Request", "sendsms_receive.html#sendsms_code_prepare", null ],
      [ "Get response data", "sendsms_receive.html#sendsms_receive_callapi", null ]
    ] ],
    [ "Reference: Error Codes", "sendsms_errorcodes.html", null ]
];