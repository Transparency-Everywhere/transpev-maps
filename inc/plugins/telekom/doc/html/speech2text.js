var speech2text =
[
    [ "Basic information", "speech2text.html#basic_features", null ],
    [ "Code", "speech2text.html#code", null ],
    [ "Further Knowledge", "speech2text.html#further_knowledge", null ],
    [ "Features and Restrictions", "speech2text_features.html", [
      [ "Transcription modes", "speech2text_features.html#mode", null ],
      [ "Supported languages", "speech2text_features.html#supportedlanguages", null ],
      [ "Audiofile content", "speech2text_features.html#contenttype", null ]
    ] ],
    [ "Example: Transcribe an audiofile", "speech2text_code.html", [
      [ "Init the SDK Client", "speech2text_code.html#speech2text_code_client", null ],
      [ "Prepare the Request", "speech2text_code.html#speech2text_code_prepare", null ],
      [ "Send the audiofile", "speech2text_code.html#speech2text_code_send", null ]
    ] ],
    [ "Reference: Error Codes", "speech2text_errorcodes.html", null ]
];