var voicecall =
[
    [ "Introduction", "voicecall_intro.html", null ],
    [ "Features and Restrictions", "voicecall_features.html", null ],
    [ "Example: Create a Call", "voicecall_code.html", [
      [ "Include Required Files", "voicecall_code.html#voicecall_code_include", null ],
      [ "Init the SDK Classes", "voicecall_code.html#voicecall_code_client", null ],
      [ "Fetch an Access Token", "voicecall_code.html#voicecall_code_token", null ],
      [ "Create the Call", "voicecall_code.html#voicecall_code_create", null ],
      [ "Query Call Status", "voicecall_code.html#voicecall_code_status", null ],
      [ "End a Call", "voicecall_code.html#voicecall_code_teardown", null ]
    ] ],
    [ "Reference: Error Codes", "voicecall_errorcodes.html", null ]
];