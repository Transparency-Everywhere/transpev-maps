var class_conference_template_update_parameters =
[
    [ "__construct", "class_conference_template_update_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_conference_template_update_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAnnouncementSet", "class_conference_template_update_parameters.html#a600440c7b09e0d1cb6a1803ea33e7f03", null ],
    [ "setDescription", "class_conference_template_update_parameters.html#a31fad3e39336ea079ea758e051866627", null ],
    [ "setDialInLocked", "class_conference_template_update_parameters.html#ab7f701a4c6d7a884f415ff315282b74e", null ],
    [ "setDuration", "class_conference_template_update_parameters.html#a4d67bc7722e9feb8af0df6ba2125d184", null ],
    [ "setInitiatorAccessPin", "class_conference_template_update_parameters.html#a9ce43a215a932e7f33bfcdc0a4d8f169", null ],
    [ "setInitiatorId", "class_conference_template_update_parameters.html#a120bee058617b3ce23e5b091ead90845", null ],
    [ "setJoinConfirm", "class_conference_template_update_parameters.html#a13d6994c0e7570a3d1f5d22b4353fb37", null ],
    [ "setLanguage", "class_conference_template_update_parameters.html#a8a788ae31fddd03d8bd8bd78b01a4686", null ],
    [ "setName", "class_conference_template_update_parameters.html#a2fe666694997d047711d7653eca2f132", null ],
    [ "setParticipantAccessPin", "class_conference_template_update_parameters.html#ab36ee8bc5e33464acdcb954c28351702", null ],
    [ "setServiceCid", "class_conference_template_update_parameters.html#a6424e414a4f394a4f652dc6fc1d65139", null ]
];