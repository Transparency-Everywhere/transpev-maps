var class_speech2_text_send_parameters =
[
    [ "__construct", "class_speech2_text_send_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_speech2_text_send_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAcceptTopic", "class_speech2_text_send_parameters.html#a09a4eabde9d47619c4ba9036a989a811", null ],
    [ "setAccount", "class_speech2_text_send_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setAudioFileContentType", "class_speech2_text_send_parameters.html#ab21ec27395352e2accfaa707286d1c8f", null ],
    [ "setContentType", "class_speech2_text_send_parameters.html#a945b1a5744b810fcb31a05f827356cfa", null ],
    [ "setFile", "class_speech2_text_send_parameters.html#a786ea2bbab26bd4a0ecae24b253d17fe", null ],
    [ "setLanguage", "class_speech2_text_send_parameters.html#a8a788ae31fddd03d8bd8bd78b01a4686", null ]
];