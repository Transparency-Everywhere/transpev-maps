var sendmms =
[
    [ "Features and Restrictions", "sendmms_features.html", null ],
    [ "Example: Send an MMS", "sendmms_code.html", [
      [ "Include Required Files", "sendmms_code.html#sendmms_code_include", null ],
      [ "Init the SDK Classes", "sendmms_code.html#sendmms_code_client", null ],
      [ "Fetch an Access Token", "sendmms_code.html#sendmms_code_token", null ],
      [ "Send the MMS", "sendmms_code.html#sendmms_code_send", null ]
    ] ],
    [ "FAQ", "sendmms_faq.html", null ],
    [ "Reference: Error Codes", "sendmms_errorcodes.html", null ]
];