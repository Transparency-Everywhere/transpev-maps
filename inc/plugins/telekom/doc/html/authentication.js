var authentication =
[
    [ "Background on Authentication", "authentication_intro.html", null ],
    [ "OAuth 2.0 Clientflow", "authentication_oauth2_clientflow.html", [
      [ "Authorization", "authentication_oauth2_clientflow.html#authentication_oauth2_clientflow_authorization", null ]
    ] ],
    [ "OAuth 2.0 Webflow", "authentication_oauth2_webflow.html", [
      [ "Authorization", "authentication_oauth2_webflow.html#authentication_oauth2_webflow_authorize", null ],
      [ "Refresh an Expired Access Token", "authentication_oauth2_webflow.html#authentication_oauth2_webflow_refreshtoken", null ],
      [ "Revoke a Refresh Token", "authentication_oauth2_webflow.html#authentication_oauth2_webflow_revoketoken", null ]
    ] ],
    [ "Example: OAuth 2.0 Methods", "oauth2_clientflow_code_standalone.html", [
      [ "Configuration", "oauth2_clientflow_code_standalone.html#oauth2_clientflow_code_standalone_requirements", null ],
      [ "Init OAuth 2 Classes", "oauth2_clientflow_code_standalone.html#oauth2_clientflow_code_standalone_client", null ],
      [ "Client Credential Authentication", "oauth2_clientflow_code_standalone.html#oauth2_clientflow_code_standalone_authentication", null ]
    ] ],
    [ "Example: OAuth 2.0 Webflow Methods", "oauth2_webflow_code_standalone.html", null ]
];