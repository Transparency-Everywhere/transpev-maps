var class_conference_template_create_parameters =
[
    [ "__construct", "class_conference_template_create_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_conference_template_create_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAnnouncementSet", "class_conference_template_create_parameters.html#a600440c7b09e0d1cb6a1803ea33e7f03", null ],
    [ "setDescription", "class_conference_template_create_parameters.html#a31fad3e39336ea079ea758e051866627", null ],
    [ "setDialInLocked", "class_conference_template_create_parameters.html#ab7f701a4c6d7a884f415ff315282b74e", null ],
    [ "setDialOut", "class_conference_template_create_parameters.html#a167f1648a8626ac52b0b414bd466890a", null ],
    [ "setDuration", "class_conference_template_create_parameters.html#a4d67bc7722e9feb8af0df6ba2125d184", null ],
    [ "setEmail", "class_conference_template_create_parameters.html#a5ef76eef42d2624386442eeb636d338c", null ],
    [ "setFirstName", "class_conference_template_create_parameters.html#a83e84c312d983f847f8e48d6e457d081", null ],
    [ "setInitiatorAccessPin", "class_conference_template_create_parameters.html#a9ce43a215a932e7f33bfcdc0a4d8f169", null ],
    [ "setJoinConfirm", "class_conference_template_create_parameters.html#a13d6994c0e7570a3d1f5d22b4353fb37", null ],
    [ "setLanguage", "class_conference_template_create_parameters.html#a8a788ae31fddd03d8bd8bd78b01a4686", null ],
    [ "setLastName", "class_conference_template_create_parameters.html#a90ee65f53f523c6ec0abb74c014c3e2a", null ],
    [ "setName", "class_conference_template_create_parameters.html#a2fe666694997d047711d7653eca2f132", null ],
    [ "setNumber", "class_conference_template_create_parameters.html#a097c2c268472e0d57ea04eaf3d88b9e2", null ],
    [ "setOwnerId", "class_conference_template_create_parameters.html#a55296772337cde8750a28eb8a7231f55", null ],
    [ "setParticipantAccessPin", "class_conference_template_create_parameters.html#ab36ee8bc5e33464acdcb954c28351702", null ],
    [ "setServiceCid", "class_conference_template_create_parameters.html#a6424e414a4f394a4f652dc6fc1d65139", null ],
    [ "$requiredFields", "class_conference_template_create_parameters.html#ac0958a455dad0cbdc71d89886a2e803b", null ]
];