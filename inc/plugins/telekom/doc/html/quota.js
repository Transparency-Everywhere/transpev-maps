var quota =
[
    [ "Introduction", "quota_intro.html", null ],
    [ "Example: Query Account Balance", "quota_code_getaccountbalance.html", [
      [ "Include Required Files", "quota_code_getaccountbalance.html#quota_code_getaccountbalance_include", null ],
      [ "Init the SDK Classes", "quota_code_getaccountbalance.html#quota_code_getaccountbalance_client", null ],
      [ "Fetch an Access Token", "quota_code_getaccountbalance.html#quota_code_getaccountbalance_token", null ],
      [ "Query Balances", "quota_code_getaccountbalance.html#quota_code_getaccountbalance_query", null ]
    ] ],
    [ "Example: User Quota", "quota_code_userquota.html", [
      [ "Include Required Files", "quota_code_userquota.html#quota_code_getaccountbalance_include", null ],
      [ "Init the SDK Classes", "quota_code_userquota.html#quota_code_getaccountbalance_client", null ],
      [ "Fetch an Access Token", "quota_code_userquota.html#quota_code_getaccountbalance_token", null ],
      [ "Query Current Quota", "quota_code_userquota.html#quota_code_userquota_get", null ],
      [ "Set a New Quota Value", "quota_code_userquota.html#quota_code_userquota_set", null ]
    ] ],
    [ "Example: Get Charged Amount", "quota_code_chargedamount.html", [
      [ "Include Required Files", "quota_code_chargedamount.html#quota_code_getaccountbalance_include", null ],
      [ "Init the SDK Classes", "quota_code_chargedamount.html#quota_code_getaccountbalance_client", null ],
      [ "Fetch an Access Token", "quota_code_chargedamount.html#quota_code_getaccountbalance_token", null ],
      [ "Query Charged Amount", "quota_code_chargedamount.html#quota_code_chargedamount_query", null ]
    ] ],
    [ "FAQ", "quota_faq.html", null ],
    [ "Reference: Error Codes", "quota_errorcodes.html", null ]
];