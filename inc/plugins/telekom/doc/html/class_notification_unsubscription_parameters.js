var class_notification_unsubscription_parameters =
[
    [ "__construct", "class_notification_unsubscription_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "getParametersArray", "class_notification_unsubscription_parameters.html#ac62710dabfc7938b17c2dc35c0284c0a", null ],
    [ "getSubscriptionId", "class_notification_unsubscription_parameters.html#a0d4009e0ec39e73a0e971e375878ddfd", null ],
    [ "hasRequiredFields", "class_notification_unsubscription_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setSubscriptionId", "class_notification_unsubscription_parameters.html#a7c054900d01f23b618f3f48df66a690b", null ]
];