var class_notification_subscription_parameters =
[
    [ "__construct", "class_notification_subscription_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "getParametersArray", "class_notification_subscription_parameters.html#ac62710dabfc7938b17c2dc35c0284c0a", null ],
    [ "getSenderAddress", "class_notification_subscription_parameters.html#a6a3b8f87d1d37f067a9f894b5cff5d9e", null ],
    [ "hasRequiredFields", "class_notification_subscription_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setCallbackData", "class_notification_subscription_parameters.html#a6221fa3aa9b16b6376e4718035aa4cec", null ],
    [ "setClientCorrelator", "class_notification_subscription_parameters.html#a06714fd02019903a008861963a1954ed", null ],
    [ "setNotifyURL", "class_notification_subscription_parameters.html#a6851026af87ed854109fe49173731c73", null ],
    [ "setSenderAddress", "class_notification_subscription_parameters.html#a6160ea8b50c9c16a5b5baf7062169c4d", null ]
];