var class_sms_validation_client =
[
    [ "getValidatedNumbers", "class_sms_validation_client.html#a802014aac44c19aa21ff410422fd9e5f", null ],
    [ "invalidateNumber", "class_sms_validation_client.html#a8b525c0e5e603cd1a57469557f23baf9", null ],
    [ "sendNumberForValidation", "class_sms_validation_client.html#a68e0877aec78dc53211396ff69def005", null ],
    [ "validateNumberWithKeyword", "class_sms_validation_client.html#a068f01b393ed5e6470561bb8bd545ba8", null ],
    [ "RESPONSE_KEY_VALIDATE_NUMBER_KEYWORD", "class_sms_validation_client.html#a176cdbe9a79f7c7ab9de949313b8bf03", null ],
    [ "RESPONSE_KEY_VALIDATION_KEYWORD", "class_sms_validation_client.html#a310f6f163f1955150a0260c40db47b9e", null ],
    [ "URL_KEY", "class_sms_validation_client.html#ad704b183cd4f6d2ca73a09b1a14f4cba", null ]
];