var confcall =
[
    [ "Introduction", "confcall_intro.html", null ],
    [ "Features and Restrictions", "confcall_features.html", null ],
    [ "Custom Spoken Announcements", "confcall_announcements.html", null ],
    [ "Example: Manage a Conference", "confcall_code_conference.html", [
      [ "Include Required Files", "confcall_code_conference.html#confcall_code_conference_include", null ],
      [ "Init the SDK Classes", "confcall_code_conference.html#confcall_code_conference_client", null ],
      [ "Fetch an Access Token", "confcall_code_conference.html#confcall_code_conference_token", null ],
      [ "Create a Conference", "confcall_code_conference.html#confcall_code_conference_create", null ],
      [ "Add a Participant", "confcall_code_conference.html#confcall_code_conference_add", null ],
      [ "Commit / Start the Conference", "confcall_code_conference.html#confcall_code_conference_commit", null ],
      [ "Check if the Conference is Running", "confcall_code_conference.html#confcall_code_conference_getrunning", null ],
      [ "Retrieve a List of Conferences", "confcall_code_conference.html#confcall_code_conference_getlist", null ],
      [ "Update Conference Details", "confcall_code_conference.html#confcall_code_conference_update", null ],
      [ "Remove a Participant", "confcall_code_conference.html#confcall_code_conference_remove", null ],
      [ "Fetch Participant Status", "confcall_code_conference.html#confcall_code_conference_participantstatus", null ],
      [ "Update Participant Details", "confcall_code_conference.html#confcall_code_conference_updateparticipant", null ],
      [ "Teardown a Running Conference", "confcall_code_conference.html#confcall_code_conference_teardown", null ]
    ] ],
    [ "Example: Conference Templates", "confcall_code_templates.html", [
      [ "Include Required Files", "confcall_code_templates.html#confcall_code_templates_include", null ],
      [ "Init the SDK Classes", "confcall_code_templates.html#confcall_code_templates_client", null ],
      [ "Fetch an Access Token", "confcall_code_templates.html#confcall_code_templates_token", null ],
      [ "Create a Conference Template", "confcall_code_templates.html#confcall_code_templates_create", null ],
      [ "Retrieve Template Details", "confcall_code_templates.html#confcall_code_templates_get", null ],
      [ "Update a Conference Template", "confcall_code_templates.html#confcall_code_templates_update", null ],
      [ "Retrieve a List of Templates", "confcall_code_templates.html#confcall_code_templates_list", null ],
      [ "Delete a Template", "confcall_code_templates.html#confcall_code_templates_remove", null ],
      [ "Add a Participant", "confcall_code_templates.html#confcall_code_templates_add", null ],
      [ "Update a Participant", "confcall_code_templates.html#confcall_code_templates_updateParticipant", null ],
      [ "Retrieve Participant Details", "confcall_code_templates.html#confcall_code_templates_getParticipant", null ],
      [ "Remove a Participant", "confcall_code_templates.html#confcall_code_templates_removeParticipant", null ]
    ] ],
    [ "Example: Upload Announcements", "confcall_code_uploadannouncement.html", [
      [ "Include Required Files", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_include", null ],
      [ "Init the SDK Classes", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_client", null ],
      [ "Fetch an Access Token", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_token", null ],
      [ "Create a Set", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_createset", null ],
      [ "Upload a File", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_upload", null ],
      [ "Retrieve a List of Sets", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_list", null ],
      [ "Delete a Set", "confcall_code_uploadannouncement.html#confcall_code_uploadannouncement_delete", null ]
    ] ],
    [ "About Time Zones", "confcall_timezones.html", null ],
    [ "FAQ", "confcall_faq.html", null ],
    [ "Reference: Announcement Types", "confcall_announcementtypes.html", null ],
    [ "Reference: Error Codes", "confcall_errorcodes.html", null ],
    [ "Reference: DTMF Control Codes", "confcall_dtmfcodes.html", null ]
];