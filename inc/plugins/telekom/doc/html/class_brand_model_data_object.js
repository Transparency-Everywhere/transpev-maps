var class_brand_model_data_object =
[
    [ "__construct", "class_brand_model_data_object.html#af0529f971a2143bfe5ea8058c2634722", null ],
    [ "getBrandId", "class_brand_model_data_object.html#add18f53a24c8f07efe15aba115a73043", null ],
    [ "getBrandName", "class_brand_model_data_object.html#a5ef8ba35af597502647bcd129f007099", null ],
    [ "getBrandText", "class_brand_model_data_object.html#a4a3c954446457d0945068a27ad132165", null ],
    [ "getModels", "class_brand_model_data_object.html#a85257a6026e75945f9e50ece1077c10c", null ]
];