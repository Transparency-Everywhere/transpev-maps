var class_voice_call_parameters =
[
    [ "__construct", "class_voice_call_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_voice_call_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setAccount", "class_voice_call_parameters.html#a1347dc567b2e0e7a1361237be8ae12c2", null ],
    [ "setANumber", "class_voice_call_parameters.html#ad156902e32d8f685f55e89018fc133cf", null ],
    [ "setAprivacy", "class_voice_call_parameters.html#afc9207bc89ead514ae91061a6729b0c0", null ],
    [ "setBNumber", "class_voice_call_parameters.html#a7a9a4228147791544a7ab8d1a8879c0a", null ],
    [ "setBprivacy", "class_voice_call_parameters.html#a379c51dc74a127b32427eeca88626db6", null ],
    [ "setExpiration", "class_voice_call_parameters.html#a26bb008b7ee52c1ea080b1cb38dacb77", null ],
    [ "setMaxduration", "class_voice_call_parameters.html#a235131d2ee88e8f68f201e66e2c710d4", null ],
    [ "setMaxwait", "class_voice_call_parameters.html#a49b2f5a26b0387cdfe5fa6112c5aa7da", null ]
];