var class_quota_client =
[
    [ "getAccountBalance", "class_quota_client.html#a107e356aa65afec9529eb017b93a2667", null ],
    [ "getChargedAmount", "class_quota_client.html#a37b592c3b922ce549f75f42a0e33d1e7", null ],
    [ "getQuotaInformation", "class_quota_client.html#aba880d50cd5eac2d64a98ebe5c406e9e", null ],
    [ "setQuota", "class_quota_client.html#abfe8366a1a08d4d1351c3ca00f88efa3", null ],
    [ "RESPONSE_KEY_ACCOUNT_BALANCE", "class_quota_client.html#a98ef69a21531fc02f77bb7c2c5594a16", null ],
    [ "RESPONSE_KEY_CHARGED_AMOUNT", "class_quota_client.html#a8b3658a6ea882685f532f34197b5f569", null ],
    [ "RESPONSE_KEY_QUOTA_INFO", "class_quota_client.html#a96e88a0af1875dfe659f695622219fae", null ],
    [ "RESPONSE_KEY_USER_QUOTA", "class_quota_client.html#a0e41dd54bbfb10a5a7f392170642cb28", null ],
    [ "URL_KEY", "class_quota_client.html#ad704b183cd4f6d2ca73a09b1a14f4cba", null ]
];