var phonenumbervalidation =
[
    [ "Introduction", "phonenumbervalidation_intro.html", null ],
    [ "Example: Number Validation", "phonenumbervalidation_code.html", [
      [ "Include Required Files", "phonenumbervalidation_code.html#confcall_code_conference_include", null ],
      [ "Init the SDK Classes", "phonenumbervalidation_code.html#confcall_code_conference_client", null ],
      [ "Fetch an Access Token", "phonenumbervalidation_code.html#confcall_code_conference_token", null ],
      [ "List of Validated Numbers", "phonenumbervalidation_code.html#phonenumbervalidation_code_list", null ],
      [ "Step 1: Send Validation Code", "phonenumbervalidation_code.html#phonenumbervalidation_code_send", null ],
      [ "Step 2: Validate the Number", "phonenumbervalidation_code.html#phonenumbervalidation_code_validate", null ],
      [ "Invalidate a Number", "phonenumbervalidation_code.html#phonenumbervalidation_code_invalidate", null ]
    ] ],
    [ "Reference: Error Codes", "phonenumbervalidation_errorcodes.html", null ]
];