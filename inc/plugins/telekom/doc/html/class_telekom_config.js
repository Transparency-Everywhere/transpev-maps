var class_telekom_config =
[
    [ "__construct", "class_telekom_config.html#a665a71946f9c1be0f77bfeb78ac0a827", null ],
    [ "__call", "class_telekom_config.html#a6ca24d0cc830839104a34951755a1f27", null ],
    [ "getConfigByKey", "class_telekom_config.html#a21f52790a9d07da07a32b7a23bf1d13b", null ],
    [ "getConstant", "class_telekom_config.html#af9981b88191f1e838662a3ea66f1132a", null ],
    [ "setConfigData", "class_telekom_config.html#a07ad8df1bb1571ee206065341d2fb22d", null ],
    [ "SDK_ACCEPT", "class_telekom_config.html#a29bb8e0dd829431b511b1183cdf43bb2", null ],
    [ "SDK_AUTH", "class_telekom_config.html#a67b36d11c76c5e91b57fe7cd56aedbcf", null ],
    [ "SDK_CONTENT_TYPE", "class_telekom_config.html#ab4e131ba5aa7903b8918bc69b2a6154f", null ],
    [ "SDK_VERSION", "class_telekom_config.html#a0ca9c5d56ae49c7befe17c47a309c5aa", null ]
];