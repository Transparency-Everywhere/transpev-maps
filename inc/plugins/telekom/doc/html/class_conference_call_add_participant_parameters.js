var class_conference_call_add_participant_parameters =
[
    [ "__construct", "class_conference_call_add_participant_parameters.html#a095c5d389db211932136b53f25f39685", null ],
    [ "hasRequiredFields", "class_conference_call_add_participant_parameters.html#a8fe6455769124d0352035f9bd88047a4", null ],
    [ "setDialOut", "class_conference_call_add_participant_parameters.html#a167f1648a8626ac52b0b414bd466890a", null ],
    [ "setEmail", "class_conference_call_add_participant_parameters.html#a5ef76eef42d2624386442eeb636d338c", null ],
    [ "setFirstName", "class_conference_call_add_participant_parameters.html#a83e84c312d983f847f8e48d6e457d081", null ],
    [ "setIsInitiator", "class_conference_call_add_participant_parameters.html#aef8055a32ba34d8a3ff851e6a774902b", null ],
    [ "setJoinConfirmDialIn", "class_conference_call_add_participant_parameters.html#ad2b987111654d30702f4bc95c142d3fb", null ],
    [ "setJoinConfirmDialOut", "class_conference_call_add_participant_parameters.html#ad09b4d612ebcd40eb5e5a1aced09aa06", null ],
    [ "setLastName", "class_conference_call_add_participant_parameters.html#a90ee65f53f523c6ec0abb74c014c3e2a", null ],
    [ "setNumber", "class_conference_call_add_participant_parameters.html#a097c2c268472e0d57ea04eaf3d88b9e2", null ],
    [ "$requiredFields", "class_conference_call_add_participant_parameters.html#ac0958a455dad0cbdc71d89886a2e803b", null ]
];