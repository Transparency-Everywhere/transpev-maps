var class_conference_single_template_data_object =
[
    [ "__construct", "class_conference_single_template_data_object.html#aa1468ededa9467412081dd42a9b691c7", null ],
    [ "getTemplateDetails", "class_conference_single_template_data_object.html#a020822aaf23da86a8d07849ee44e0f00", null ],
    [ "getTemplateParticipants", "class_conference_single_template_data_object.html#a50f7cae36628cd5d4c889256c9f22bb2", null ],
    [ "$templateDetails", "class_conference_single_template_data_object.html#a7c93f8a2e5f6716b314117c3201352d7", null ],
    [ "$templateParticipants", "class_conference_single_template_data_object.html#a469691d54167914309f95a6b97a04157", null ]
];