<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the speech2text results.
 */
class Speech2TextDataObject extends TelekomDataObject {
	

	private $links;
	private $transcription;

	/**
	 * init the status and request ID
	 * @param array $objectsArray
	 */
	public function initResponseStatus($objectsArray) {
		if (isset($objectsArray['links'])) {
			$this->links = $objectsArray['links'];
		}
		if (isset($objectsArray['transcription'])) {
			$this->transcription = $objectsArray['transcription'];
		}
		parent::initResponseStatus($objectsArray);
	}

	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
	}


	/**
	 * Retrieves the transcription response object.
	 * @return string
	 */
	public function getTranscription() {
		return $this->transcription;
	}


	/**
	 * Retrieves the link response object.
	 * @return string[]
	 */
	public function getLink() {
		return $this->links;
	}
}
