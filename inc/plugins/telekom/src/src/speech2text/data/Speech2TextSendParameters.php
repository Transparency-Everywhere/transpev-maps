<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for send MMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class Speech2TextSendParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'file' 				=> null,
/*			'acceptTopic'			=> null,
			'language' 				=> null,
			'audioFileContentType' 	=> null,
			'account' 				=> null,
			'contentType' 			=> null,*/
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		
		foreach ($this->sendParameters as $key => $val){
			if (empty($this->sendParameters[$key])){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * ...
	 * @param string file
	 */
	public function setFile($file){
		$this->sendParameters['file'] = '@' . $file;
	}

	/**
	 * ...
	 * @param string acceptTopic
	 */
	public function setAcceptTopic($acceptTopic){
		$this->sendParameters['acceptTopic'] = $acceptTopic;
	}

	/**
	 * ...
	 * @param string language
	 */
	public function setLanguage($language){
		$this->sendParameters['language'] = $language;
	}

	/**
	 * ...
	 * @param string audioFileContentType
	 */
	public function setAudioFileContentType($audioFileContentType){
		$this->sendParameters['audioFileContentType'] = $audioFileContentType;
	}

	/**
	 * ...
	 * @param string account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}

	/**
	 * ...
	 * @param string contentType
	 */
	public function setContentType($contentType){
		$this->sendParameters['contentType'] = $contentType;
	}
}
