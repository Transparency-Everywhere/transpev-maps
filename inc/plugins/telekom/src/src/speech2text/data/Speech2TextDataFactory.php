<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the Speech2Text data object.
 */
class Speech2TextDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates a Telekom data object for Speech2Text from the response array.
	 * @param array $dataArray Data array
	 * @return Speech2TextDataObject Data object
	 */
	public static function createSpeech2TextDataObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);

		if(isset($dataArray['links']))
			$objectsArray['links']  = $dataArray['links'];
		if(isset($dataArray['transcription']))
			$objectsArray['transcription'] = $dataArray['transcription'];
		
		return new Speech2TextDataObject($objectsArray);
	}
}
