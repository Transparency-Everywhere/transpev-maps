<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Send SMS Client to send a SMS
 */
class Speech2TextClient extends TelekomClient {
	
	/**
	 * Constant of API SendSMS Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_speech2text_base_url';
	
	/**
	 * Constant of speech2text discovery response key, used to build the request URL.
	 */
	const RESPONSE_KEY_DISCOVERY = 'api_speech2text_discovery_url'; 

	/**
	 * Constant of speech2text transcription response key, used to build the request URL.
	 */
	const RESPONSE_KEY_TRANSCRIPTION = 'api_speech2text_transcription_url'; 
	
	/**
	 * speech2text discovery
	 * @param string $secureToken Security token we got from authentication class.
	 * @return SendSmsDataObject $dataObj The send SMS data object.
	 */
	public function discovery($secureToken){
			
		// build url and get response
		$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_DISCOVERY);

		$response = $this->service->getResponseJSONData($url, $secureToken, null, 'GET');
		$responseArray = $this->service->transformResponseDataToArray($response);

		// create data object
		$dataObj = Speech2TextDataFactory::createSpeech2TextDataObject($responseArray);
		return $dataObj;
	}

	/**
	 * speech2text discovery
	 * @param string $secureToken Security token we got from authentication class.
	 * @return SendSmsDataObject $dataObj The send SMS data object.
	 */
	public function transcription($secureToken, $sendParameters){
			
		// build url and get response
		$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_TRANSCRIPTION);

		$url = sprintf($url, $this->service->config->getEnvironment());

		$additionalHeaders = array();

		$paramsArray = $sendParameters->getParametersArray();

		if(isset($paramsArray['acceptTopic']))
			array_push($additionalHeaders, 'Accept-Topic: ' . $paramsArray['acceptTopic']);
		
		if(isset($paramsArray['audioFileContentType']))
			array_push($additionalHeaders, 'Audio-File-Content-Type: ' . $paramsArray['audioFileContentType']);

		if(isset($paramsArray['language']))
			array_push($additionalHeaders, 'Accept-Language: ' . $paramsArray['language']);

		$response = $this->service->getResponseAdditionalHeaderData($url, $secureToken, $paramsArray, 'POST', $additionalHeaders);
		$responseArray = $this->service->transformResponseDataToArray($response);

		// create data object
		$dataObj = Speech2TextDataFactory::createSpeech2TextDataObject($responseArray);
		return $dataObj;
	}
	
	/**
	 * Builds the response URL.
	 * @param $urlKey Config key to get the right URL
	 * @param $responseKey Config key to get the additional response
	 * @return string
	 * @Override TelekomClient::buildResponseUrl($urlKey, $responseKey)
	 */
	protected function buildResponseUrl($urlKey, $responseKey){
		$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey($urlKey);
		$apiUrl = $apiUrl . $this->service->config->getConfigByKey($responseKey);
		return $apiUrl;
	}
}
