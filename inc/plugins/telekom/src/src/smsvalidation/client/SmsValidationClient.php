<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for SMS Validation.
 */
class SmsValidationClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_validation_keyword_rest_url';
	
	/**
	 * Constant of sending number for response key, used to build the request URL.
	 */
	const RESPONSE_KEY_VALIDATION_KEYWORD = 'send';
	
	/**
	 * Constant of validating number key, used to build the request URL.
	 */
	const RESPONSE_KEY_VALIDATE_NUMBER_KEYWORD = 'validatednumbers'; 
	
	/**
	 * Send the validation number to get a keyword.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param SendNumberForValidationParameters $sendParameters send parameters including all set send data.
	 * @return SmsValidationDataObject SMS data object.
	 */
	public function sendNumberForValidation($secureToken, $sendParameters){
		
		// build url
		$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
		$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VALIDATION_KEYWORD;
		
		return $this->sendRequest($url, $secureToken, $sendParameters);
	}
	
	/**
	 * Send the validation number with keyword to be validated.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ValidateNumberWithKeywordParameters $sendParameters send parameters including all set send data.
	 * @return SmsValidationDataObject SMS data object.
	 */
	public function validateNumberWithKeyword($secureToken, $sendParameters){
		
		// build url
		$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
		$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VALIDATE_NUMBER_KEYWORD . '/' . urlencode($sendParameters->getNumber());
		
		return $this->sendRequest($url, $secureToken, $sendParameters);
	}
	
	/**
	 * Invalidate number.
	 * @param string $phoneNumber Phone number
	 * @param string $secureToken Security token we got from authentication class.
	 * @return SmsValidationDataObject $dataObj SMS data object
	 */
	public function invalidateNumber($phoneNumber, $secureToken){
		
		if (!empty($phoneNumber)){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VALIDATE_NUMBER_KEYWORD . '/' . urlencode($phoneNumber);
			
			// get response
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = SmsValidationDataFactory::createSmsValidationDataObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $phoneNumber in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Get validated numbers.
	 * @param string $secureToken Security token we got from authentication class.
	 * @return SmsValidationDataObject $dataObj SMS data object
	 */
	public function getValidatedNumbers($secureToken){
		
		// build url
		$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
		$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VALIDATE_NUMBER_KEYWORD;
		
		// get response
		$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
		$responseArray = $this->service->transformResponseDataToArray($response);
		
		// create data object
		$dataObj = SmsValidationDataFactory::createSmsValidationNumbersDataObject($responseArray);
		return $dataObj;
	}
	
	/**
	 * Send the prepared request via service.
	 * @param string $url URL
	 * @param string $secureToken Secure token
	 * @param object $sendParameters parameters object
	 * @return SmsValidationDataObject $dataObj SMS data object
	 */
	private function sendRequest($url, $secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// get response
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = SmsValidationDataFactory::createSmsValidationDataObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
