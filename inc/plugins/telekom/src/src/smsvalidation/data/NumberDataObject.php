<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for a single number.
 */
class NumberDataObject {
	
	private $number;
	
	private $validUntil;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param string $number number
	 */
	public function __construct($data){
		$this->number = $data['number'];
		$this->validUntil = $data['validUntil'];
	}
	
	/**
	 * The phone number
	 * @return string number
	 */
	public function getNumber(){
		return $this->number;
	}
	
	/**
	 * Expiration date. Null if the validation does not expire.
	 * @return date or null
	 */
	public function getValidUntil() {
		return $this->validUntil;
	}
}
