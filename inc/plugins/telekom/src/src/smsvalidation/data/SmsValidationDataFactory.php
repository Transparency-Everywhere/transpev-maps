<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data Factory to build the Send validation keyword data object.
 */
class SmsValidationDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates a Telekom data object for the SMS validation response array.
	 * @param array $dataArray Data array
	 * @return SmsValidationDataObject Data object
	 */
	public static function createSmsValidationDataObject($dataArray) {
	
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		return new SmsValidationDataObject($objectsArray);
	}
	
	/**
	 * Creates a Telekom data object for the SMS validation get numbers response array.
	 * @param array $dataArray Data array
	 * @return SmsValidationNumbersDataObject Data object
	 */
	public static function createSmsValidationNumbersDataObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		if (isset($dataArray['numbers'])){
			$objectsArray['numbers'] = self::createValidatedNumbers($dataArray['numbers']);
		}
		
		return new SmsValidationNumbersDataObject($objectsArray);
	}
	
	/**
	 * Creates an array including all number data objects.
	 * @param array $dataArray Data array
	 * @return array[NumberDataObject] $validatedNumbers Array with number objects
	 */
	public static function createValidatedNumbers($dataArray) {
		
		$validatedNumbers = array();
		foreach($dataArray as $number) {
			$validatedNumbers[] = new NumberDataObject($number);
		}
		
		return $validatedNumbers;
	}
}
