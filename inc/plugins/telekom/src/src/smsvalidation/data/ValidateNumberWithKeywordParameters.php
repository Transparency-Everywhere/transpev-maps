<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for validation number with keyword.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ValidateNumberWithKeywordParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'number' 	=> null,
			'key' 	=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['number']) && !empty($this->sendParameters['key'])){
			return true;
		}
		return false;
	}
	
	/**
	 * The phone number
	 * @return string number
	 */
	public function getNumber(){
		return $this->sendParameters['number'];
	}
	
	/**
	 * Number to confirm
	 * @param string $number Mobile phone number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}
	
	/**
	 * Validation key received
	 * @param string $key Key string
	 */
	public function setKey($key){
		$this->sendParameters['key'] = $key;
	}
}
