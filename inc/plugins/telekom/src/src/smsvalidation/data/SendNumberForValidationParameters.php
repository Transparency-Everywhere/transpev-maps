<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class SendNumberForValidationParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'number' 	=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['number'])){
			return true;
		}
		return false;
	}
	
	/**
	 * Number to send the keyword to
	 * @param string $number Mobile phone number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}
	
	/**
	 * The accompanying message that should be sent with the validation code.
	 * This message must contain two placeholders as shown in the following example:
	 * "The keyword for validating your phone number with example.com is #key# and is valid until #validUntil#."
	 * @param string $message SMS Text message
	 */
	public function setMessage($message){
		$this->sendParameters['message'] = $message;
	}
	
	/**
	 * Originator of the message as shown on receiver's phone
	 * @param string $originator Originator
	 */
	public function setOriginator($originator){
		$this->sendParameters['originator'] = $originator;
	}
	
	/**
	 * Sub-account to bill. If omitted, the main account is selected.
	 * @param string $account Account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}
}
