<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for quota.
 */
class QuotaClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_quota_rest_url';
	
	/**
	 * Constant of quota information, used to build the request URL.
	 */
	const RESPONSE_KEY_QUOTA_INFO = 'quotainfo';
	
	/**
	 * Constant of setting quota key, used to build the request URL.
	 */
	const RESPONSE_KEY_USER_QUOTA = 'userquota';
	
	/**
	 * Constant of quota account balance key, used to build the request URL.
	 */
	const RESPONSE_KEY_ACCOUNT_BALANCE = 'account/balance';
	
	/**
	 * Constant of quota charged amount, used to build the request URL.
	 */
	const RESPONSE_KEY_CHARGED_AMOUNT = 'charge';
	
	/**
	 * Get quota information.
	 * @param string $moduleId Module ID.
	 * @param string $secureToken Security token we got from authentication class.
	 * @return QuotaInformationDataObject $dataObj data object.
	 */
	public function getQuotaInformation($moduleId, $secureToken){
		
		if (!empty($moduleId)){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_QUOTA_INFO . '/' . urlencode($moduleId);
			
			// get response, in this case this is a custom GET request
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = QuotaDataFactory::createQuotaInformationObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $moduleId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Set max. quota.
	 * @param string $moduleId Module ID.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param QuotaSetParameters $sendParameters send parameters including required send data.
	 * @return QuotaDataObject $dataObj data object.
	 */
	public function setQuota($moduleId, $secureToken, $sendParameters){
		
		if (!empty($moduleId) && $sendParameters->hasRequiredFields()){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_USER_QUOTA . '/' . urlencode($moduleId);
			
			// get response, in this case this is a custom PUT request
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'PUT');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = QuotaDataFactory::createSetQuotaObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing send parameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Query Account Balance
	 * @param string $secureToken Security token we got from authentication class.
	 * @param QuotaAccountBalanceParameters $sendParameters send parameters including required send data.
	 * @return QuotaAccountBalanceDataObject $dataObj data object.
	 */
	public function getAccountBalance($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// get response array
			$responseUrl = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_ACCOUNT_BALANCE);
			$response = $this->service->getResponseStandardData($responseUrl, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = QuotaDataFactory::createGetAccountBalanceObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing send parameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Get the charged amount of a voice call or conference call.
	 * @param string $callId Call ID.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param QuotaChargedAmountParameters $sendParameters send parameters including required send data.
	 * @return QuotaChargedAmountDataObject $dataObj data object.
	 */
	public function getChargedAmount($callId, $secureToken, $sendParameters){
		
		if (!empty($callId)){
			
			// get response array
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CHARGED_AMOUNT . '/' . urlencode($callId));
			$responseUrl = $this->addParameterToUrl($url, $sendParameters->getParametersArray());
			$response = $this->service->getResponseCustomData($responseUrl, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = QuotaDataFactory::createGetChargedAmountObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $callId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
