<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for a single account.
 */
class AccountDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param string $account account
	 * @param string $credits credits
	 */
	public function __construct($account, $credits){
		
		$this->data['account'] = $account;
		$this->data['credits'] = $credits;
	}
	
	/**
	 * Account ID of a sub-account
	 * @return string account
	 */
	public function getAccount(){
		return $this->data['account'];
	}
	
	/**
	 * Account balance of the sub-account
	 * @return string credits
	 */
	public function getCredits(){
		return $this->data['credits'];
	}
}
