<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the quota data object.
 */
class QuotaDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates an object including the quota information.
	 * @param array $dataArray Data array
	 * @return QuotaInformationDataObject quota
	 */
	public static function createQuotaInformationObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status'			=> parent::createStatus($dataArray['status']),
			'quotaInformation'	=> array(),
		);
		
		
		// set response data
		$dataKeys = array('maxQuota', 'maxUserQuota', 'quotaLevel');
		foreach ($dataKeys as $key){
			if (isset($dataArray[$key])){
				$objectsArray['quotaInformation'][$key] = $dataArray[$key];
			}
		}
		
		return new QuotaInformationDataObject($objectsArray);
	}
	
	/**
	 * Creates an object including the set quota status object.
	 * @param array $dataArray Data array
	 * @return QuotaDataObject
	 */
	public static function createSetQuotaObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		return new QuotaDataObject($objectsArray);
	}
	
	/**
	 * Creates an object including the quota account balance object.
	 * @param array $dataArray Data array
	 * @return QuotaAccountBalanceDataObject account balance object
	 */
	public static function createGetAccountBalanceObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' 	=> parent::createStatus($dataArray['status']),
			'accounts'	=> array(),
		);
		
		if (!empty($dataArray['accounts']) && is_array($dataArray['accounts'])){
			$objectsArray['accounts'] = self::createAccounts($dataArray['accounts']);
		}
		
		return new QuotaAccountBalanceDataObject($objectsArray);
	}
	
	/**
	 * Build a array with account objects.
	 * @param array $accountArray account data array
	 * @return array[AccountDataObject] $accounts
	 */
	private static function createAccounts($accountArray){
		
		$accounts = array();
		foreach ($accountArray as $item){
			$accounts[$item['account']] = new AccountDataObject($item['account'], $item['credits']);
		}
		return $accounts;
	}
	
	/**
	 * Creates an object including the charged amount status object.
	 * @param array $dataArray Data array
	 * @return QuotaChargedAmountDataObject
	 */
	public static function createGetChargedAmountObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' 	=> parent::createStatus($dataArray['status']),
			'charge'	=> null,
		);
		
		if (!empty($dataArray['charge'])){
			$objectsArray['charge'] = $dataArray['charge'];
		}
		
		return new QuotaChargedAmountDataObject($objectsArray);
	}
	
	
}
