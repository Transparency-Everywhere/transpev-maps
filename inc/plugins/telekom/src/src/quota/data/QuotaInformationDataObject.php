<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the quota information results.
 */
class QuotaInformationDataObject extends TelekomDataObject {
	
	/**
	 * max. quota information data.
	 * @var string
	 */
	private $maxQuota;
	
	/**
	 * max. user quota information data.
	 * @var string
	 */
	private $maxUserQuota;
	
	/**
	 * quota level information data.
	 * @var string
	 */
	private $quotaLevel;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
		
		// set the quota data
		if (!empty($objectsArray['quotaInformation'])){
			$this->maxQuota 	= $objectsArray['quotaInformation']['maxQuota'];
			$this->maxUserQuota = $objectsArray['quotaInformation']['maxUserQuota'];
			$this->quotaLevel 	= $objectsArray['quotaInformation']['quotaLevel'];
		}
	}

	/**
	 * System-defined limit of maximum quota points that can be consumed per day.
	 * This system quota cannot be exceeded by the user quota (maxUserQuota).
	 * @return StdDataObject
	 */
	public function getMaxQuota(){
		return $this->maxQuota;
	}

	/**
	 * User-defined limit of maximum quota points that can be consumed per day.
	 * This user quota cannot exceed the system quota (maxQuota).
	 * @return StdDataObject
	 */
	public function getMaxUserQuota(){
		return $this->maxUserQuota;
	}

	/**
	 * Number of quota points consumed today.
	 * @return StdDataObject
	 */
	public function getQuotaLevel(){
		return $this->quotaLevel;
	}
}
