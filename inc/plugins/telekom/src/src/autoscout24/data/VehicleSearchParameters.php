<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed search parameters data for vehicle search.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class VehicleSearchParameters extends TelekomSendParameters implements TelekomParameters {
	
	/**
	 * Key to handle the search parameters.
	 * @var string
	 */
	protected $parametersMainKey = 'vehicle_search_parameters';
	
	/**
	 * Set the culture code (following RFC 1766, e.g. "de-DE")
	 * @param string $cultureId Culture ID
	 */
	public function __construct($cultureId){
	
		$this->sendParameters = array(
			'culture_id' => null,
			$this->parametersMainKey => array(),
		);
		$this->setCultureId($cultureId);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['culture_id']) && is_array($this->sendParameters[$this->parametersMainKey])){
			return true;
		}
		return false;
	}
	
	/**
	 * Set the culture ID.
	 * @param string $cultureId culture ID
	 */
	public function setCultureId($cultureId){
		$this->sendParameters['culture_id'] = $cultureId;
	}
	
	/**
	 * Sets the adress data.
	 * @param array $countries Countries value
	 * @param string $radius Radius
	 * @param string $zipCode ZIP Code
	 * @param string $zipCountryId ZIP Country ID
	 */
	public function setAddress($countries = null, $radius = null, $zipCode = null, $zipCountryId = null){
		$this->sendParameters[$this->parametersMainKey]['address'] = array(
			'countries' 		=> array('country_id' => $countries),
			'radius' 			=> $radius,
			'zip_code'			=> $zipCode,
			'zip_country_id' 	=> $zipCountryId,
		);
	}
	
	/**
	 * Filter by min/max advertisement date
	 * @param string $beginFrom Begin from
	 */
	public function setAvailabilityBegin($beginFrom = null){
		$this->sendParameters[$this->parametersMainKey]['availability']['begin'] = array(
			'from' 	=> $beginFrom,
		);
	}
	
	/**
	 * Filter by min/max last change date
	 * @param string $lastChangeFrom Last change from
	 * @param string $lastChangeTo Last change to
	 */
	public function setAvailabilityLastChange($lastChangeFrom = null, $lastChangeTo = null){
		$this->sendParameters[$this->parametersMainKey]['availability']['last_change'] = array(
			'from' 	=> $lastChangeFrom,
			'to' 	=> $lastChangeTo,
		);
	}
	
	/**
	 * Filter by body color groups
	 * @param array $colourGroups Colour group IDs
	 * @throws TelekomException
	 */
	public function setBodyColorgroups($colourGroups){
		if (is_array($colourGroups)){
			$this->sendParameters[$this->parametersMainKey]['body_colorgroups']['body_colorgroup_id'] = $colourGroups;
		}
		else {
			throw new TelekomException('Wrong datatype: $colourGroups must be an array in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by body paintings
	 * @param array $paintings Paintings array
	 * @throws TelekomException
	 */
	public function setBodyPaintings($paintings){
		if (is_array($paintings)){
			$this->sendParameters[$this->parametersMainKey]['body_paintings']['body_painting_id'] = $paintings;
		}
		else {
			throw new TelekomException('Wrong datatype: $paintings must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by brands
	 * @param array $brands Brand IDs
	 * @throws TelekomException
	 */
	public function setBrands($brands){
		if (is_array($brands)){
			$this->sendParameters[$this->parametersMainKey]['brands']['brand_id'] = $brands;
		}
		else {
			throw new TelekomException('Wrong datatype: $brands must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by categories
	 * @param array $categories Categories
	 * @throws TelekomException
	 */
	public function setCategories($categories){
		if (is_array($categories)){
			$this->sendParameters[$this->parametersMainKey]['categories']['category_id'] = $categories;
		}
		else {
			throw new TelekomException('Wrong datatype: $categories must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by minimum and maxmimum doors
	 * @param string $doorsFrom Doors from
	 * @param string $doorsTo Doors to
	 */
	public function setDoors($doorsFrom = null, $doorsTo = null){
		$this->sendParameters[$this->parametersMainKey]['doors'] = array(
			'from' 	=> $doorsFrom,
			'to' 	=> $doorsTo,
		);
	}
	
	/**
	 * Filter by equipment
	 * @param array $equipments Equipments
	 * @throws TelekomException
	 */
	public function setEquipments($equipments){
		if (is_array($equipments)){
			$this->sendParameters[$this->parametersMainKey]['equipments']['equipment_id'] = $equipments;
		}
		else {
			throw new TelekomException('Wrong datatype: $equipments must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by fuel type
	 * @param array $fuelTypes Fuel types
	 * @throws TelekomException
	 */
	public function setFuelTypes($fuelTypes){
		if (is_array($fuelTypes)){
			$this->sendParameters[$this->parametersMainKey]['fuel_types']['fuel_type_id'] = $fuelTypes;
		}
		else {
			throw new TelekomException('Wrong datatype: $fuelTypes must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by gear type
	 * @param array $gearTypes Gear types
	 * @throws TelekomException
	 */
	public function setGearTypeIds($gearTypes){
		if (is_array($gearTypes)){
			$this->sendParameters[$this->parametersMainKey]['gear_type_ids']['gear_type_id'] = $gearTypes;
		}
		else {
			throw new TelekomException('Wrong datatype: $gearTypes must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by min/max power rating value
	 * @param string $kilowattFrom Kilowatt from
	 * @param string $kilowattTo Kilowatt to
	 */
	public function setKilowatt($kilowattFrom = null, $kilowattTo = null){
		$this->sendParameters[$this->parametersMainKey]['kilowatt'] = array(
			'from' 	=> $kilowattFrom,
			'to' 	=> $kilowattTo,
		);
	}
	
	/**
	 * Filter by min/max mileage
	 * @param string $mileageFrom Mileage from
	 * @param string $mileageTo Mileage to
	 */
	public function setMileage($mileageFrom = null, $mileageTo = null){
		$this->sendParameters[$this->parametersMainKey]['mileage'] = array(
			'from' 	=> $mileageFrom,
			'to' 	=> $mileageTo,
		);
	}
	
	/**
	 * Filter by models
	 * @param array $models array with the model IDs
	 * @throws TelekomException
	 */
	public function setModels($models){
		if (is_array($models)){
			$this->sendParameters[$this->parametersMainKey]['models']['model_id'] = $models;
		}
		else {
			throw new TelekomException('Wrong datatype: $models must be an array in ' . __METHOD__ . ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Filter by minimum and maximum price
	 * @param string $priceFrom Price from
	 * @param string $priceTo Price to
	 * @param string $vatTypeId VAT type ID
	 */
	public function setPricePublic($priceFrom = null, $priceTo = null, $vatTypeId = null){
		$this->sendParameters[$this->parametersMainKey]['price_public'] = array(
			'from' 			=> $priceFrom,
			'to' 			=> $priceTo,
			'vat_type_id' 	=> $vatTypeId,
		);
	}
	
	/**
	 * If parameter is true all usage states will be included into resultset, new and used vehicles will be found otherwise. If parameter is not present the filter is ignored.
	 * @param boolean $accidentFree accident free
	 */
	public function setAccidentFree($accidentFree){
		$this->sendParameters[$this->parametersMainKey]['accident_free'] = $accidentFree;
	}
	
	/**
	 * Filter by bodies
	 * @param string $bodyId body ID
	 */
	public function setBodies($bodyId){
		$this->sendParameters[$this->parametersMainKey]['bodies']['body_id'] = $bodyId;
	}
	
	/**
	 * Filter by dealer id
	 * @param string $dealerId dealer ID
	 */
	public function setDealerId($dealerId){
		$this->sendParameters[$this->parametersMainKey]['dealer_id'] = $dealerId;
	}
	
	/**
	 * Filter by emission classes and stickers
	 * @param string $classId class ID
	 * @param string $stickerId sticker ID
	 */
	public function setEmission($classId = null, $stickerId = null){
		$this->sendParameters[$this->parametersMainKey]['emission'] = array(
			'class_id' 		=> $classId,
			'sticker_id' 	=> $stickerId,
		);
	}
	
	/**
	 * Filter by min/max date of next general inspection
	 * @param string $from date from
	 * @param string $to date to
	 */
	public function setGeneralInspection($from = null, $to = null){
		$this->sendParameters[$this->parametersMainKey]['general_inspection'] = array(
			'from' 			=> $from,
			'to' 			=> $to,
		);
	}
	
	/**
	 * Filter by min/max initial registration date, date has to be formatted like this: '2011-12-15' or '2012-12-15'
	 * @param string $from date from
	 * @param string $to date to
	 */
	public function setInitialRegistration($from = null, $to = null){
		$this->sendParameters[$this->parametersMainKey]['initial_registration'] = array(
			'from' 			=> $from,
			'to' 			=> $to,
		);
	}
	
	/**
	 * Filter by model lines
	 * @param string $modelLineId model line ID
	 */
	public function setModelLines($modelLineId){
		$this->sendParameters[$this->parametersMainKey]['model_lines']['model_line_id'] = $modelLineId;
	}
	
	/**
	 * Filter by owners's offer key
	 * @param string $ownersOfferKey owners offer key
	 */
	public function setOwnersOfferKey($ownersOfferKey){
		$this->sendParameters[$this->parametersMainKey]['owners_offer_key'] = $ownersOfferKey;
	}
	
	/**
	 * Set Pagination parameters
	 * @param integer $currentPage current page
	 * @param integer $resultsPerPage results per page
	 */
	public function setPaging($currentPage = null, $resultsPerPage = null){
		$this->sendParameters[$this->parametersMainKey]['paging'] = array(
			'current_page' 			=> $currentPage,
			'results_per_page' 		=> $resultsPerPage,
		);
	}
	
	/**
	 * Filter by count of previous owners
	 * @param string $from count from
	 * @param string $to count to
	 */
	public function setPreviousOwnerCount($from = null, $to = null){
		$this->sendParameters[$this->parametersMainKey]['previous_owner_count'] = array(
			'from' 			=> $from,
			'to' 			=> $to,
		);
	}
	
	/**
	 * Set sorting parameters
	 * @param string $descending descending
	 * @param string $key key
	 */
	public function setSorting($descending = null, $key = null){
		$this->sendParameters[$this->parametersMainKey]['sorting'] = array(
			'descending' 	=> $descending,
			'key' 			=> $key,
		);
	}
	
	/**
	 * Set if to show only dealer vehicles
	 * @param boolean $showDealerVehicles show dealer vehicles
	 */
	public function setShowDealerVehicles($showDealerVehicles){
		$this->sendParameters[$this->parametersMainKey]['show_dealer_vehicles'] = $showDealerVehicles;
	}
	
	/**
	 * Set if to show only private vehicles
	 * @param boolean $showPrivateVehicles show private vehicles
	 */
	public function setShowPrivateVehicles($showPrivateVehicles){
		$this->sendParameters[$this->parametersMainKey]['show_private_vehicles'] = $showPrivateVehicles;
	}
	
	/**
	 * Set if to show only vehicles with images
	 * @param boolean $showWithImagesOnly show with images only
	 */
	public function setShowWithImagesOnly($showWithImagesOnly){
		$this->sendParameters[$this->parametersMainKey]['show_with_images_only'] = $showWithImagesOnly;
	}
	
	/**
	 * Filter by model version
	 * @param string $version version
	 */
	public function setVersion($version){
		$this->sendParameters[$this->parametersMainKey]['version'] = $version;
	}
}
