<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for root paths data.
 */
class RootPathsDataObject {
	
	/**
	 * Data array
	 * @var array data
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $data data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get autoscout24 URL root.
	 * @return string root url
	 */
	public function getAs24UrlRoot(){
		return $this->data['as24_url_root'];
	}
	
	/**
	 * Get big images URL.
	 * @return string URL
	 */
	public function getImagesBig(){
		return $this->data['images_big'];
	}
	
	/**
	 * Get main images URL.
	 * @return string URL
	 */
	public function getImagesMain(){
		return $this->data['images_main'];
	}
	
	/**
	 * Get small images URL.
	 * @return string URL
	 */
	public function getImagesSmall(){
		return $this->data['images_small'];
	}
	
	/**
	 * Get thumbnail images URL.
	 * @return string URL
	 */
	public function getImagesThumbnails(){
		return $this->data['images_thumbnails'];
	}
}
