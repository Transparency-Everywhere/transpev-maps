<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object to get a single brand and it's models.
 */
class BrandModelDataObject {
	
	/**
	 * Data array
	 * @var array data
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param string $brandName Name of the brand
	 * @param string $brandId ID of the brand
	 * @param string $brandText Text of the brand, which is the brands name, e.g. "BWM"
	 * @param array $models Models data array
	 */
	public function __construct($brandName, $brandId, $brandText, $models){
		
		$this->data['brandName'] = $brandName;
		$this->data['brandId'] = $brandId;
		$this->data['brandText'] = $brandText;
		$this->data['models'] = $models;
	}
	
	/**
	 * Brand name.
	 * @return string brand name
	 */
	public function getBrandName(){
		return $this->data['brandName'];
	}
	
	/**
	 * Brand ID.
	 * @return string brand ID
	 */
	public function getBrandId(){
		return $this->data['brandId'];
	}
	
	/**
	 * Brand text.
	 * @return string brand text
	 */
	public function getBrandText(){
		return $this->data['brandText'];
	}
	
	/**
	 * Models.
	 * @return array[ModelDataObject] models
	 */
	public function getModels(){
		return $this->data['models'];
	}
}
