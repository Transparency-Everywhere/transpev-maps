<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Factory to build all Autoscout24 data objects.
 */
class Autoscout24DataFactory extends TelekomDataFactory {
	
	/**
	 * Autoscout24 Configuration array to map values by key / method name.
	 * @var array $vehicleConfArr
	 */
	static private $vehicleConfArr = array(
				'availability_begin' => array('availability', 'begin'),
				'availability_last_change' => array('availability', 'last_change'),
				'emission_class_id' => array('emission', 'class_id'),
				'emission_sticker_id' => array('emission', 'sticker_id'),
				'previous_owner' => array('previous_owner', 'owner', 'owner'),
				'previous_owner_count' => array('previous_owner', 'count'),
				'properties' => array('properties', 'property'),
				'vehicle_id' => array('vehicle_id'),
				'version' => array('version'),
				'media_image_count' => array('media', 'x_code', 'image_count'),
				'media_images' => array('media', 'images', 'image'),
				'consumption_liquid_combined' => array('consumption', 'liquid', 'combined'),
				'consumption_liquid_extra_urban' => array('consumption', 'liquid', 'extra_urban'),
				'consumption_liquid_urban' => array('consumption', 'liquid', 'urban'),
				'accident_free' => array('accident_free'),
				'body_color' => array('body_color'),
				'body_colorgroup_id' => array('body_colorgroup_id'),
				'body_id' => array('body_id'),
				'body_painting_id' => array('body_painting_id'),
				'brand_id' => array('brand_id'),
				'category_id' => array('category_id'),
				'equipment_ids' => array('equipments', 'equipment_id'),
				'fuel_type_id' => array('fuel_type_id'),
				'gear_type_id' => array('gear_type_id'),
				'general_inspection' => array('general_inspection'),
				'initial_registration' => array('initial_registration'),
				'kilowatt' => array('kilowatt'),
				'mileage' => array('mileage'),
				'model_id' => array('model_id'),
				'owners_offer_key' => array('owners_offer_key'),
				'prices' => array('prices', 'price'),
				'seals' => array('seals'),
				'title' => array('title'),
				'detail_page_url' => array('x_code', 'as24_detail_page_url'),
			);
	
	/**
	 * Creates a Telekom data object for look up data from the response array.
	 * @param array $dataArray Data array
	 * @return LookUpDataObject Look up data Object
	 */
	public static function createLookUpDataObject($dataArray) {
		
		// create the Object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
			'responseStatus' => '',
			'requestId' => '',
			'lookUpData' => array(),
			'build' => '',
		);
	
		if (isset($dataArray['response'])){
			
			$objectsArray['responseStatus'] = $dataArray['response']['status'];
			$objectsArray['requestId'] = $dataArray['response']['request_id'];
			$objectsArray['lookUpData'] = self::createLookUpData($dataArray['response']['stx3_idpool']['elements']);
			$objectsArray['build'] = self::createBuild($dataArray['response']['build']);
		}
		
		return new LookUpDataObject($objectsArray);
	}

	/**
	 * Creates an array including all look up data objects.
	 * @param array $dataArray Data array
	 * @return array $lookUpData Array with object elements
	 */
	public static function createLookUpData($dataArray) {
		
		$lookUpData = array();
		if (array_key_exists('element', $dataArray)) {
			$lookUpArray = $dataArray['element'];
			
			foreach($lookUpArray as $lookUpSingle) {
				$singleLookUp = new ElementDataObject($lookUpSingle);
				$lookUpData[$singleLookUp->getId() . "_" . $singleLookUp->getName()] = $singleLookUp;
			}
		}
		
		return $lookUpData;
	}
	
	/**
	 * Creates a Telekom data object for model tree from the response array.
	 * @param array $dataArray Data array
	 * @return ModelTreeDataObject Model tree data Object
	 */
	public static function createModelTreeDataObject($dataArray) {
		
		// create the Object parts
		$objectsArray = array(
			'status' => self::createStatus($dataArray['status']),
			'responseStatus' => '',
			'requestId' => '',
			'modelTreeData' => array(),
			'build' => '',
		);
	
		if (isset($dataArray['response'])){
			$objectsArray['responseStatus'] = $dataArray['response']['status'];
			$objectsArray['requestId'] = $dataArray['response']['request_id'];
			if (isset($dataArray['response']['stx3_idpool']['nodes'])){
				$objectsArray['modelTreeData'] = self::createModelTreeData($dataArray['response']['stx3_idpool']['nodes']);
			}
			$objectsArray['build'] = self::createBuild($dataArray['response']['build']);
		}
	
		return new ModelTreeDataObject($objectsArray);
	}

	/**
	 * Creates an array including all model tree data objects.
	 * @param array $dataArray Data array
	 * @return array $brandModelTreeData Array with brand an model object elements
	 */
	public static function createModelTreeData($dataArray) {
		
		$brandModelTreeData = array();
		if (array_key_exists('node', $dataArray)) {
			$modelTreeArray = $dataArray['node'];

			foreach ($modelTreeArray as $brandModelArray) {
				
				// here are the models
				$models = array();
				if (isset($brandModelArray['nodes']['node']) && is_array($brandModelArray['nodes']['node'])){
					foreach ($brandModelArray['nodes']['node'] as $model){
						$models[] = new ModelDataObject($model);
					}
				}
				
				// building brand object
				$brandModelObj = new BrandModelDataObject($brandModelArray['name'], $brandModelArray['id'], $brandModelArray['text'], $models);
				
				// adding object to array
				$brandModelTreeData[$brandModelObj->getBrandId()] = $brandModelObj;
			}
		}
		
		return $brandModelTreeData;
	}
	
	/**
	 * Creates a Telekom data object for vehicles from the response array.
	 * @param array $dataArray Data array
	 * @return FindArticlesDataObject Find articles data object including all vehicles
	 */
	public static function createVehiclesDataObject($dataArray) {
		
		// create the Object parts
		$objectsArray = array(
			'status' => self::createStatus($dataArray['status']),
			'rootPaths' => array(),
			'usedVehicleSearchParameters' => array(),
			'vehicles' => array(),
			'vehiclesFound' => '',
			'responseStatus' => '',
			'requestId' => '',
			'build' => '',
		);
		
		if (isset($dataArray['response'])){
			$objectsArray['rootPaths'] = self::createRootPaths($dataArray['response']['root_paths']);
			$objectsArray['usedVehicleSearchParameters'] = self::createUsedVehicleSearchParameters($dataArray['response']['used_vehicle_search_parameters']);
			$objectsArray['vehicles'] = self::createVehicles($dataArray['response']['vehicles']);
			if(isset($dataArray['response']['vehicles_found'])) {
				$objectsArray['vehiclesFound'] = $dataArray['response']['vehicles_found'];
			} else {
				$objectsArray['vehiclesFound'] = 0;
			}
			$objectsArray['responseStatus'] = $dataArray['response']['status'];
			$objectsArray['requestId'] = $dataArray['response']['request_id'];
			$objectsArray['build'] = self::createBuild($dataArray['response']['build']);
		}
		return new FindArticlesDataObject($objectsArray);
	}

	/**
	 * Creates an array including all vehicle data objects.
	 * @param array $dataArray Data array
	 * @return array $vehicles Array with all vehicles
	 */
	public static function createVehicles($dataArray) {
		
		$vehicles = array();
		if (array_key_exists('vehicle', $dataArray)) {
			$vehicleArray = $dataArray['vehicle'];
			
			foreach($vehicleArray as $vehicleSingle) {
				$singleVehicle = self::createVehicle($vehicleSingle);
				$vehicles[$singleVehicle->getVehicleId()] = $singleVehicle;
			}
		}
		
		return $vehicles;
	}

	/**
	 * Creates a single vehicle data object.
	 * @param array $dataArray Vehicle Data array
	 * @return VehicleDataObject Single vehicle object
	 */
	public static function createVehicle($dataArray) {
		
		$vehicleData = array();
		foreach (self::$vehicleConfArr as $keyName => $confKeys){
			
			$data = TelekomDataFactoryHelper::getDataForKey($confKeys, $dataArray);
			switch ($keyName){
				case 'media_images':
					$vehicleData[$keyName] = self::createImages($data);
					break;
				case 'previous_owner':
					$vehicleData[$keyName] = self::createPrevieousOwner($data);
					break;
				case 'properties':
					$vehicleData[$keyName] = self::createProperties($data);
					break;
				case 'prices':
					$vehicleData[$keyName] = self::createPrice($data);
					break;
				default:
					$vehicleData[$keyName] = $data;
					break;
			}
		}
		
		return new VehicleDataObject($vehicleData);
	}

	/**
	 * Creates an array including all build data.
	 * @param array $dataArray Data array
	 * @return BuildDataObject standard data object
	 */
	public static function createBuild($dataArray) {
		$buildArray = array(
			'number' => $dataArray['number'],
			'runtime' => $dataArray['time_stamp']['runtime'],
			'service' => $dataArray['time_stamp']['service'],
		);
		return new BuildDataObject($buildArray);
	}

	/**
	 * Creates an array including all previous owner data.
	 * @param array $dataArray Data array
	 * @return array[VehiclePrevieousOwnerDataObject]
	 */
	public static function createPrevieousOwner($dataArray) {
		
		$previousOwner = array();
		foreach ($dataArray as $owner){
			$oArray = array(
				'customerTypeId' => $owner['x_code']['customer_type_id'],
				'country' => $owner['country'],
				'city' => $owner['city'],
				'zip' => $owner['zip'],
			);
			$previousOwner[] = new VehiclePrevieousOwnerDataObject($oArray);
		}
		
		return $previousOwner;
	}

	/**
	 * Creates an array including all previous owner data.
	 * @param array $dataArray Data array
	 * @return array
	 */
	public static function createProperties($dataArray) {
		
		$properties = array();
		foreach ($dataArray as $prop){
			$pArray = array(
				'text' => $prop['text'],
				'x_code' => $prop['x_code'],
			);
			$properties[] = new VehiclePropertiesDataObject($pArray);
		}
		
		return $properties;
	}

	/**
	 * Creates an array including all root paths.
	 * @param array $dataArray Data array
	 * @return RootPathsDataObject root paths object
	 */
	public static function createRootPaths($dataArray) {
		return new RootPathsDataObject($dataArray);
	}
	
	/**
	 * Creates the used vehicle search parameters data object.
	 * @param array $dataArray Data array
	 * @return UsedSerarchParametersDataObject used search parameters data object
	 */
	public static function createUsedVehicleSearchParameters($dataArray){
		return new UsedSerarchParametersDataObject($dataArray);
	}
	
	/**
	 * Creates an images data array including single image objects.
	 * @param array $dataArray Data array
	 * @return array[VehicleImageDataObject] $images Images array including single image objects
	 */
	public static function createImages($dataArray){
		
		$images = array();
		if (is_array($dataArray)){
			foreach ($dataArray as $image){
				$images[] = new VehicleImageDataObject($image);
			}
		}
		return $images;
	}
	
	/**
	 * Creates a price data object.
	 * @param array $dataArray Data array
	 * @return array[VehiclePriceDataObject] Prices
	 */
	public static function createPrice($dataArray){
		
		$priceArray = array();
		foreach ($dataArray as $priceArray){
			$priceArray[] = new VehiclePriceDataObject($priceArray);
		}
		return $priceArray;
	}
}
