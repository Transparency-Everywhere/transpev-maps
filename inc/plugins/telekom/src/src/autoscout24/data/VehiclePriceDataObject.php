<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for vehicle price.
 */
class VehiclePriceDataObject {
	
	/**
	 * Data array
	 * @var array data
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $data data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get price value.
	 * @return string value
	 */
	public function getValue(){
		return $this->data['value'];
	}
	
	/**
	 * Get price type.
	 * @return string type
	 */
	public function getType(){
		return $this->data['type'];
	}
	
	/**
	 * Get price currency.
	 * @return string currency
	 */
	public function getCurrencyId(){
		return $this->data['currency_id'];
	}
	
	/**
	 * Get price vat type ID.
	 * @return string vat type ID
	 */
	public function getVatTypeId(){
		return $this->data['vat_type_id'];
	}
}
