<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for a vehicle.
 */
class VehicleDataObject {
	
	/**
	 * Data array
	 * @var array data
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $data data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get availability begin.
	 * @return string availability begin
	 */
	public function getAvailabilityBegin(){
		return $this->data['availability_begin'];
	}
	
	/**
	 * Get availability last change.
	 * @return string availability last change
	 */
	public function getAvailabilityLastChange(){
		return $this->data['availability_last_change'];
	}
	
	/**
	 * Get emission class ID.
	 * @return string emission class ID
	 */
	public function getEmissionClassId(){
		return $this->data['emission_class_id'];
	}
	
	/**
	 * Get emission sticker ID.
	 * @return string emission sticker ID
	 */
	public function getEmissionStickerId(){
		return $this->data['emission_sticker_id'];
	}
	
	/**
	 * Get previous owner.
	 * @return VehiclePrevieousOwnerDataObject previous owner
	 */
	public function getPreviousOwner(){
		return $this->data['previous_owner'];
	}
	
	/**
	 * Get previous owner count.
	 * @return string previous owner count
	 */
	public function getPreviousOwnerCount(){
		return $this->data['previous_owner_count'];
	}
	
	/**
	 * Get properties.
	 * @return VehiclePropertiesDataObject properties
	 */
	public function getProperties(){
		return $this->data['properties'];
	}
	
	/**
	 * Get vehicle ID.
	 * @return string vehicle ID
	 */
	public function getVehicleId(){
		return $this->data['vehicle_id'];
	}
	
	/**
	 * Get version.
	 * @return string version
	 */
	public function getVersion(){
		return $this->data['version'];
	}
	
	/**
	 * Get media image count.
	 * @return string media image count
	 */
	public function getMediaImageCount(){
		return $this->data['media_image_count'];
	}
	
	/**
	 * Get media images.
	 * @return array[VehicleImageDataObject] media images
	 */
	public function getMediaImages(){
		return $this->data['media_images'];
	}
	
	/**
	 * Get consumption liquid combined.
	 * @return string consumption liquid combined
	 */
	public function getConsumptionLiquidCombined(){
		return $this->data['consumption_liquid_combined'];
	}
	
	/**
	 * Get consumption liquid extra urban.
	 * @return string consumption liquid extra urban
	 */
	public function getConsumptionLiquidExtraUrban(){
		return $this->data['consumption_liquid_extra_urban'];
	}
	
	/**
	 * Get consumption liquid urban.
	 * @return string consumption liquid urban
	 */
	public function getConsumptionLiquidUrban(){
		return $this->data['consumption_liquid_urban'];
	}
	
	/**
	 * Get accident free.
	 * @return string accident free
	 */
	public function getAccidentFree(){
		return $this->data['accident_free'];
	}
	
	/**
	 * Get body color.
	 * @return string body color 
	 */
	public function getBodyColor(){
		return $this->data['body_color'];
	}
	
	/**
	 * Get body colorgroup id.
	 * @return string body colorgroup id
	 */
	public function getBodyColorgroupId(){
		return $this->data['body_colorgroup_id'];
	}
	
	/**
	 * Get body id.
	 * @return string body id 
	 */
	public function getBodyId(){
		return $this->data['body_id'];
	}
	
	/**
	 * Get body painting id.
	 * @return string body painting id
	 */
	public function getBodyPaintingId(){
		return $this->data['body_painting_id'];
	}
	
	/**
	 * Get brand ID.
	 * @return string brand ID 
	 */
	public function getBrandId(){
		return $this->data['brand_id'];
	}
	
	/**
	 * Get category ID.
	 * @return string category ID 
	 */
	public function getCategoryId(){
		return $this->data['category_id'];
	}
	
	/**
	 * Get equipment IDs.
	 * @return array equipment IDs
	 */
	public function getEquipmentIds(){
		return $this->data['equipment_ids'];
	}
	
	/**
	 * Get fuel type ID.
	 * @return string fuel type ID
	 */
	public function getFuelTypeId(){
		return $this->data['fuel_type_id'];
	}
	
	/**
	 * Get gear type ID.
	 * @return string gear type ID
	 */
	public function getGearTypeId(){
		return $this->data['gear_type_id'];
	}
	
	/**
	 * Get general inspection.
	 * @return string general inspection
	 */
	public function getGeneralInspection(){
		return $this->data['general_inspection'];
	}
	
	/**
	 * Get initial registration.
	 * @return string initial registration
	 */
	public function getInitialRegistration(){
		return $this->data['initial_registration'];
	}
	
	/**
	 * Get kilowatt.
	 * @return string kilowatt
	 */
	public function getKilowatt(){
		return $this->data['kilowatt'];
	}
	
	/**
	 * Get mileage.
	 * @return string mileage
	 */
	public function getMileage(){
		return $this->data['mileage'];
	}
	
	/**
	 * Get model ID.
	 * @return string model ID
	 */
	public function getModelId(){
		return $this->data['model_id'];
	}
	
	/**
	 * Get owners offer key.
	 * @return string owners offer key
	 */
	public function getOwnersOfferKey(){
		return $this->data['owners_offer_key'];
	}
	
	/**
	 * Get prices.
	 * @return array[VehiclePriceDataObject] prices
	 */
	public function getPrices(){
		return $this->data['prices'];
	}
	
	/**
	 * Get seals.
	 * @return string seals
	 */
	public function getSeals(){
		return $this->data['seals'];
	}
	
	/**
	 * Get title.
	 * @return string title
	 */
	public function getTitle(){
		return $this->data['title'];
	}
	
	/**
	 * Get detail page url.
	 * @return string detail page url
	 */
	public function getDetailPageUrl(){
		return $this->data['detail_page_url'];
	}
}
