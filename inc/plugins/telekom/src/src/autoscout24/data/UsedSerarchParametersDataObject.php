<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object for used search parameters.
 */
class UsedSerarchParametersDataObject {
	
	/**
	 * Data array
	 * @var array data
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $data data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get address.
	 * @return array address
	 */
	public function getAddress(){
		return $this->data['address'];
	}
	
	/**
	 * Get kilowatt.
	 * @return array kilowatt
	 */
	public function getKilowatt(){
		return $this->data['kilowatt'];
	}
	
	/**
	 * Get brands.
	 * @return array brands
	 */
	public function getBrands(){
		return $this->data['brands'];
	}
	
	/**
	 * Get paging.
	 * @return array paging
	 */
	public function getPaging(){
		return $this->data['paging'];
	}
	
	/**
	 * Get price public.
	 * @return array price public
	 */
	public function getPricePublic(){
		return $this->data['price_public'];
	}
	
	/**
	 * Get show dealer vehicles.
	 * @return string show dealer vehicles
	 */
	public function getShowDealerVehicles(){
		return $this->data['show_dealer_vehicles'];
	}
	
	/**
	 * Get show private vehicles.
	 * @return string show private vehicles
	 */
	public function getShowPrivateVehicles(){
		return $this->data['show_private_vehicles'];
	}
	
	/**
	 * Get show with images only.
	 * @return string show with images only
	 */
	public function getShowWithImagesOnly(){
		return $this->data['show_with_images_only'];
	}
	
	/**
	 * Get sorting.
	 * @return array sorting
	 */
	public function getSorting(){
		return $this->data['sorting'];
	}
	
	/**
	 * Get the culture ID.
	 * @return string culture ID
	 */
	public function getCultureId(){
		return $this->sendParameters['culture_id'];
	}
	
	/**
	 * Get the availability begin from/to.
	 * @return array availability begin
	 */
	public function getAvailabilityBegin(){
		return $this->data['availability']['begin'];
	}
	
	/**
	 * Get the availability last change from/to.
	 * @return array availability last change
	 */
	public function getAvailabilityLastChange(){
		return $this->data['availability']['last_change'];
	}
	
	/**
	 * Gets the body colour groups.
	 * @return array Colour groups
	 */
	public function getBodyColorgroups(){
		return $this->data['body_colorgroups']['body_colorgroup_id'];
	}
	
	/**
	 * Gets the body paintings
	 * @return array paintings
	 */
	public function getBodyPaintings(){
		return $this->data['body_paintings']['body_painting_id'];
	}
	
	/**
	 * Get the categories.
	 * @return array categories
	 */
	public function getCategories(){
		return $this->data['categories']['category_id'];
	}
	
	/**
	 * Get the doors from/to.
	 * @return array doors
	 */
	public function getDoors(){
		return $this->data['doors'];
	}
	
	/**
	 * Get the equipments.
	 * @return array equipments
	 */
	public function getEquipments(){
		return $this->data['equipments']['equipments_id'];
	}
	
	/**
	 * Get the fuel types.
	 * @return array fuel types
	 */
	public function getFuelTypes(){
		return $this->data['fuel_types']['fuel_type_id'];
	}
	
	/**
	 * Get the gear type IDs.
	 * @return array $gearTypes Gear types
	 */
	public function getGearTypeIds(){
		return $this->data['gear_type_ids']['gear_type_id'];
	}
	
	/**
	 * Get the mileage from/to.
	 * @return array mileage
	 */
	public function getMileage(){
		return $this->data['mileage'];
	}
	
	/**
	 * Get the models.
	 * @return array models
	 */
	public function getModels(){
		return $this->data['models']['model_id'];
	}
	
	/**
	 * Get accident free.
	 * @return string accident free
	 */
	public function getAccidentFree(){
		return $this->data['accident_free'];
	}
	
	/**
	 * Get bodies.
	 * @return array bodies
	 */
	public function getBodies(){
		return $this->data['bodies'];
	}
	
	/**
	 * Get dealer ID.
	 * @return string dealer ID
	 */
	public function getDealerId(){
		return $this->data['dealer_id'];
	}
	
	/**
	 * Get emission.
	 * @return array emission
	 */
	public function getEmission(){
		$this->data['emission'];
	}
	
	/**
	 * Get general inspection.
	 * @return array general inspection
	 */
	public function getGeneralInspection(){
		return $this->data['general_inspection'];
	}
	
	/**
	 * Get initial registration.
	 * @return array initial registration
	 */
	public function getInitialRegistration(){
		return $this->data['initial_registration'];
	}
	
	/**
	 * Get model lines.
	 * @return string model lines
	 */
	public function getModelLines(){
		return $this->data['model_lines']['model_line_id'];
	}
	
	/**
	 * Get owners offer key.
	 * @return string owners offer key
	 */
	public function getOwnersOfferKey(){
		return $this->data['owners_offer_key'];
	}
	
	/**
	 * Get previous owner count.
	 * @return array previous owner count 
	 */
	public function getPreviousOwnerCount(){
		return $this->data['previous_owner_count'];
	}
	
	/**
	 * Get verion.
	 * @return string version
	 */
	public function getVersion(){
		return $this->data['version'];
	}
}
