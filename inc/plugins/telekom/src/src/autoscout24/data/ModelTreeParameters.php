<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for the model tree.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ModelTreeParameters extends TelekomSendParameters implements TelekomParameters {
	
	/**
	 * Initializing the send parameters data array.
	 * @param string $cultureId Culture ID e.g. "de-DE"
	 */
	public function __construct($cultureId){
	
		$this->sendParameters = array(
			'culture_id' => null,
		);
		$this->setCultureId($cultureId);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (isset($this->sendParameters['culture_id'])){
			return true;
		}
		return false;
	}
	
	/**
	 * Set the culture code (following RFC 1766, e.g. "de-DE")
	 * @param string $cultureId culture ID
	 */
	public function setCultureId($cultureId){
		$this->sendParameters['culture_id'] = $cultureId;
	}
}
