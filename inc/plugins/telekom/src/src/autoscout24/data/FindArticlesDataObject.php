<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the find articles results.
 */
class FindArticlesDataObject extends TelekomDataObject {
	
	protected $rootPaths;
	
	protected $usedVehicleSearchParameters;
	
	protected $vehiclesFound;
	
	protected $vehicles;
	
	protected $build;
	
	protected $responseStatus;
	
	protected $requestId;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
		
		$parts = array('rootPaths', 'usedVehicleSearchParameters', 'vehiclesFound', 'vehicles', 'build', 'responseStatus', 'requestId');
		foreach ($parts as $part){
			if (isset($objectsArray[$part])){
				$this->{$part} = $objectsArray[$part];
			}
		}
	}

	/**
	 * Root path for returned images
	 * @return RootPathsDataObject root paths data object
	 */
	public function getRootPaths(){
		return $this->rootPaths;
	}

	/**
	 * Number of vehicles found
	 * @return string
	 */
	public function getVehiclesFound(){
		return $this->vehiclesFound;
	}

	/**
	 * Search parameters used for this search
	 * @return UsedSerarchParametersDataObject
	 */
	public function getUsedVehicleSearchParameters(){
		return $this->usedVehicleSearchParameters;
	}

	/**
	 * Get the vehicles data array.
	 * @return array[VehicleDataObject] vehicles
	 */
	public function getVehicles(){
		return $this->vehicles;
	}

	/**
	 * Get a single vehicle object.
	 * @return VehicleDataObject Single vehicle object
	 */
	public function getVehiclesById($vehicleId){
		if (isset($this->vehicles[$vehicleId])){
			return $this->vehicles[$vehicleId];
		}
	}

	/**
	 * Get build data object.
	 * @return BuildDataObject
	 */
	public function getBuild(){
		return $this->build;
	}

	/**
	 * Response of AutoScout24 backend servers.
	 * Usually not relevant since errors are mapped to Telekom status codes.
	 * @return string
	 */
	public function getResponseStatus(){
		return $this->responseStatus;
	}

	/**
	 * Auto generated response ID.
	 * Used for support and troubleshooting.
	 * @return string
	 */
	public function getRequestId(){
		return $this->requestId;
	}
}
