<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the tree data results.
 */
class ModelTreeDataObject extends TelekomDataObject {
	
	protected $modelTreeData;
	
	protected $build;
	
	protected $responseStatus;
	
	protected $requestId;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
		
		// set the model tree data
		if (isset($objectsArray['modelTreeData'])){
			$this->modelTreeData = $objectsArray['modelTreeData'];
		}
		if (isset($objectsArray['build'])){
			$this->build = $objectsArray['build'];
		}
		if (isset($objectsArray['responseStatus'])){
			$this->responseStatus = $objectsArray['responseStatus'];
		}
		if (isset($objectsArray['requestId'])){
			$this->requestId = $objectsArray['requestId'];
		}
	}

	/**
	 * Get the model tree data array.
	 * @return array
	 */
	public function getModelTreeData(){
		return $this->modelTreeData;
	}

	/**
	 * Get the model single object by ID.
	 * @param string $modelTreeId Model ID
	 * @return StdDataArrayObject
	 */
	public function getModelById($modelTreeId){
		if (isset($this->modelTreeData[$modelTreeId])){
			return $this->modelTreeData[$modelTreeId];
		}
	}

	/**
	 * Information about the data build
	 * @return BuildDataObject
	 */
	public function getBuild(){
		return $this->build;
	}

	/**
	 * Response of AutoScout24 backend servers (over-the-wire format).
	 * Usually not relevant since errors are mapped to Telekom status codes.
	 * @return string
	 */
	public function getResponseStatus(){
		return $this->responseStatus;
	}

	/**
	 * Auto generated response ID.
	 * Used for support and troubleshooting
	 * @return string
	 */
	public function getRequestId(){
		return $this->requestId;
	}
}
