<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Autoscout24 Client to get articles, look up data and model tree.
 */
class AutoScout24Client extends TelekomClient {
	
	/**
	 * Constant of API Autoscout24 Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_autoscout_rest_url';
	
	/**
	 * Constant of articles response key, used to build the request URL.
	 */
	const RESPONSE_KEY_ARTICLES = 'articles'; 
	
	/**
	 * Constant of look up data response key, used to build the request URL.
	 */
	const RESPONSE_KEY_LOOKUP = 'lookUpData';
	
	/**
	 * Constant of model tree response key, used to build the request URL.
	 */
	const RESPONSE_KEY_MODELTREE = 'makeModelTree';
	
	/**
	 * Query the article database.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param VehicleSearchParameters $sendParameters Search parameters including all set search data.
	 * @return FindArticlesDataObject $dataObj The find articles data object.
	 */
	public function findArticles($secureToken, $sendParameters){
		
		// build url, get response and create data object
		$responseUrl = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_ARTICLES);
		$responseArray = $this->getResponseArray($responseUrl, $secureToken, $sendParameters, 'POST');
		
		$dataObj = Autoscout24DataFactory::createVehiclesDataObject($responseArray);
		return $dataObj;
	}
	
	/**
	 * Query the look up data.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param LookUpDataSearchParameters $sendParameters Search parameters including all set search data.
	 * @return LookUpDataObject $dataObj The look up data object.
	 */
	public function findLookUpData($secureToken, $sendParameters){
		
		// build url, get response and create data object
		$responseUrl = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_LOOKUP);
		$responseArray = $this->getResponseArray($responseUrl, $secureToken, $sendParameters, 'POST');

		$dataObj = Autoscout24DataFactory::createLookUpDataObject($responseArray);
		return $dataObj;
	}
	
	/**
	 * Query the find model tree data.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ModelTreeSearchParameters $sendParameters Search parameters including all set search data.
	 * @return ModelTreeDataObject $dataObj The model tree data object.
	 */
	public function findModelTreeData($secureToken, $sendParameters){
		
		// build url, get response and create data object
		$responseUrl = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_MODELTREE);
		$responseArray = $this->getResponseArray($responseUrl, $secureToken, $sendParameters, 'POST');
		
		$dataObj = Autoscout24DataFactory::createModelTreeDataObject($responseArray);
		return $dataObj;
	}
}
