<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client to send a MMS.
 */
class SendMmsClient extends TelekomClient {
	
	/**
	 * Constant of API SendMMS Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_sendmms_rest_url';
	
	/**
	 * Constant of MMS response key, used to build the request URL.
	 */
	const RESPONSE_KEY_SENDMMS = 'sendMMS'; 
	
	/**
	 * Send an MMS.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param SendMmsParameters $sendParameters send parameters including all set send data.
	 * @return SendMmsDataObject $dataObj The send MMS data object.
	 */
	public function sendMms($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// build url and get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_SENDMMS);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = SendMmsDataFactory::createSendMmsDataObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing send parameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
