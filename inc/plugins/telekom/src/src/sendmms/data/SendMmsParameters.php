<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for send MMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class SendMmsParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'number' 		=> null,
			'subject' 		=> null,
			'message' 		=> null,
			'attachment' 	=> null,
			'filename' 		=> null,
			'contentType' 	=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		
		foreach ($this->sendParameters as $key => $val){
			if (empty($this->sendParameters[$key])){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Phone Number(s) of SMS receiver(s), comma separated
	 * @param string $number Mobile phone number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}
	
	/**
	 * Sets the subject.
	 * @param string $subject MMS Text subject
	 */
	public function setSubject($subject){
		$this->sendParameters['subject'] = $subject;
	}
	
	/**
	 * Sets the message.
	 * @param string $message MMS Text message
	 */
	public function setMessage($message){
		$this->sendParameters['message'] = $message;
	}
	
	/**
	 * Sets the attachment data or file.
	 * File names start with @, otherwise the parameter is treated as the attachment data. 
	 * @param string $attachment MMS attachment
	 */
	public function setAttachment($attachment){
		if (substr($attachment, 0, 1) == "@") {
			$this->sendParameters['attachment'] = base64_encode(file_get_contents(substr($attachment, 1)));
		} else {
			$this->sendParameters['attachment'] = base64_encode($attachment);
		}
	}
	
	/**
	 * Sets the filename of the attachment.
	 * @param string $filename filename
	 */
	public function setFilename($filename){
		$this->sendParameters['filename'] = $filename;
	}
	
	/**
	 * Sets the content type of the attachment.
	 * @param string $contentType content type
	 */
	public function setContentType($contentType){
		$this->sendParameters['contentType'] = $contentType;
	}
	
	/**
	 * Sets the originator.
	 * @param string $originator Originator
	 */
	public function setOriginator($originator){
		$this->sendParameters['originator'] = $originator;
	}
	
	/**
	 * Account-ID of the sub account which should be billed for this service call
	 * @param string $account Account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}
}
