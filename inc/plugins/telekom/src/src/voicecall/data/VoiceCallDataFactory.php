<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the Voice call data object.
 */
class VoiceCallDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates a Telekom data object for voice call from the response array.
	 * @param array $dataArray Data array
	 * @return VoiceCallDataObject Data object
	 */
	public static function createVoiceCallObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		return new VoiceCallDataObject($objectsArray);
	}
	
	/**
	 * Creates a Telekom data object for created voice call from the response array.
	 * @param array $dataArray Data array
	 * @return VoiceCallCreatedDataObject Data object
	 */
	public static function createVoiceCallCreatedObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		// set response data
		if (isset($dataArray['sessionID'])){
			$objectsArray['sessionID'] = $dataArray['sessionID'];
		}
		
		return new VoiceCallCreatedDataObject($objectsArray);
	}
	
	/**
	 * Creates a Telekom data object for voice call status from the response array.
	 * @param array $dataArray Data array
	 * @return VoiceCallStatusDataObject Data object
	 */
	public static function createVoiceCallStatusObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		// set call status data
		$objectsArray['callStatus'] = $dataArray;
		
		return new VoiceCallStatusDataObject($objectsArray);
	}
}
