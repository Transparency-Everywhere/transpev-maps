<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object to get the call status data.
 */
class CallStatusDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		$this->data = array();
		if (is_array($objectsArray)){
			
			foreach ($objectsArray as $key => $val){
				
				// we need all other key-value-pairs
				if ($key == 'status') continue;
				
				$this->data[$key] = $val;
			}
		}
	}
	
	/**
	 * Get state of first client.
	 * `IDLE` The call has not yet been made.
	 * `CONNECTING` The participant is being called.
	 * `RINGING` The participant's telephone is ringing.
	 * `CONNECTED` The participant is connected.
	 * `DISCONNECTING` The connection to the participant is being ended.
	 * `DISCONNECTED` The connection to the participant has ended.
	 * @return string state A
	 */
	public function getStateA(){
		if (isset($this->data['stateA'])){
			return $this->data['stateA'];
		}
	}
	
	/**
	 * Get state of first client.
	 * `IDLE` The call has not yet been made.
	 * `CONNECTING` The participant is being called.
	 * `RINGING` The participant's telephone is ringing.
	 * `CONNECTED` The participant is connected.
	 * `DISCONNECTING` The connection to the participant is being ended.
	 * `DISCONNECTED` The connection to the participant has ended.
	 * @return string state B
	 */
	public function getStateB(){
		if (isset($this->data['stateB'])){
			return $this->data['stateB'];
		}
	}
	
	/**
	 * Duration of the call with the first participant in seconds.
	 * A value of -1 means that the call has not yet started.
	 * @return string connection time A
	 */
	public function getConnectionTimeA(){
		if (isset($this->data['connectionTimeA'])){
			return $this->data['connectionTimeA'];
		}
	}
	
	/**
	 * Duration of the call with the second participant in seconds.
	 * A value of -1 means that the call has not yet started.
	 * @return string connection time B
	 */
	public function getConnectionTimeB(){
		if (isset($this->data['connectionTimeB'])){
			return $this->data['connectionTimeB'];
		}
	}
	
	/**
	 * Reason for ending the connection to the first participant:
	 * 0 - Call not yet ended.
	 * 1 - The participant is engaged.
	 * 2 - The participant has rejected the call.
	 * 3 - The participant could not be reached or has not accepted the call.
	 * 4 - The participant could not be reached due to barriers.
	 * 99 - An internal error has occurred.
	 * @return string reason A
	 */
	public function getReasonA(){
		if (isset($this->data['reasonA'])){
			return $this->data['reasonA'];
		}
	}
	
	/**
	 * Reason for ending the connection to the second participant:
	 * 0 - Call not yet ended.
	 * 1 - The participant is engaged.
	 * 2 - The participant has rejected the call.
	 * 3 - The participant could not be reached or has not accepted the call.
	 * 4 - The participant could not be reached due to barriers.
	 * 99 - An internal error has occurred.
	 * @return string reason B
	 */
	public function getReasonB(){
		if (isset($this->data['reasonB'])){
			return $this->data['reasonB'];
		}
	}
	
	/**
	 * Description of reasonA
	 * @return string description A
	 */
	public function getDescriptionA(){
		if (isset($this->data['descriptionA'])){
			return $this->data['descriptionA'];
		}
	}
	
	/**
	 * Description of reasonB
	 * @return string description B
	 */
	public function getDescriptionB(){
		if (isset($this->data['descriptionB'])){
			return $this->data['descriptionB'];
		}
	}
	
	/**
	 * Get phone number of the second participant, that was called.
	 * @return string phone number of B
	 */
	public function getBe164(){		
		if (isset($this->data['be164'])){
			// Workaround because "be164": {"@nil":"true"} is returned
			// before the first numbers is called
			if (is_array($this->data['be164'])) {
				return NULL;
			}
			return $this->data['be164'];
		}
	}
	
	/**
	 * Index of the phone number of the second participant (B), who was called.
	 * The value 0 means the first B party phone number which was called,
	 * 1 means the second B party phone number which was called etc.
	 * @return string b index
	 */
	public function getBindex(){
		if (isset($this->data['bindex'])){
			return $this->data['bindex'];
		}
	}
}
