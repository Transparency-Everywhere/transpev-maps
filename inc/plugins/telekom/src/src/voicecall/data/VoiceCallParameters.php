<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for voice calls.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class VoiceCallParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'anumber' 	=> null,
			'bnumber' 	=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['anumber']) && !empty($this->sendParameters['bnumber'])){
			return true;
		}
		return false;
	}
	
	/**
	 * Set the number of first participant. See \ref devgarden_phonenumberformats.
	 * @param string $aNumber phone number
	 */
	public function setANumber($aNumber){
		$this->sendParameters['anumber'] = $aNumber;
	}
	
	/**
	 * Set the number of second participant. See \ref devgarden_phonenumberformats.
	 * @param string $bNumber phone number
	 */
	public function setBNumber($bNumber){
		$this->sendParameters['bnumber'] = $bNumber;
	}
	
	/**
	 * Set the expiration.
	 * @param string $expiration expiration
	 */
	public function setExpiration($expiration){
		$this->sendParameters['expiration'] = $expiration;
	}
	
	/**
	 * Duration in seconds after which the Voice Call service disconnects a call without receiving a
	 * `callStatus` request.
	 * A value of "0" means that there are no `callStatus` requests needed to keep the connection running. 
	 * @param string $maxduration max. duration
	 */
	public function setMaxduration($maxduration){
		$this->sendParameters['maxduration'] = $maxduration;
	}
	
	/**
	 * The time in seconds after which the next phone number in the list of b-numbers is called.
	 * @param string $maxwait max. wait
	 */
	public function setMaxwait($maxwait){
		$this->sendParameters['maxwait'] = $maxwait;
	}
	
	/**
	 * Deactivates the signaling of the Open Development System phone number on the first participant's terminal.
	 * @param string $aprivacy privacy
	 */
	public function setAprivacy($aprivacy){
		$this->sendParameters['aprivacy'] = $aprivacy;
	}
	
	/**
	 * Deactivates the signaling of the Open Development System phone number on the second participant's terminal.
	 * @param string $bprivacy privacy
	 */
	public function setBprivacy($bprivacy){
		$this->sendParameters['bprivacy'] = $bprivacy;
	}
	
	/**
	 * Account ID of the sub-account that should be billed for the service use.
	 * If the parameter is not specified, the main account is billed.
	 * @param string $account account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}
}
