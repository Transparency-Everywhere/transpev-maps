<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for voice calls.
 */
class VoiceCallClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_voicecall_rest_url';
	
	/**
	 * Constant of voicecall standard response key, used to build the request URL.
	 */
	const RESPONSE_KEY_VOICECALL = 'call'; 
	
	/**
	 * Create a new call.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param VoiceCallParameters $sendParameters send parameters including all set send data.
	 * @return VoiceCallCreatedDataObject $dataObj The send data object.
	 */
	public function newCall($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// build url and get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_VOICECALL);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = VoiceCallDataFactory::createVoiceCallCreatedObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Get call status.
	 * @param string $sessionId Session ID of the actual call
	 * @param string $secureToken Security token we got from authentication class
	 * @param boolean $keepAlive Hold the call, refresh the expiration time
	 * @return VoiceCallStatusDataObject $dataObj The send data object
	 */
	public function getCallStatus($sessionId, $secureToken, $keepAlive = false){
		
		if (!empty($sessionId)){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VOICECALL . '/' . urlencode($sessionId) . '?keepAlive=' . $keepAlive;
			
			// get response
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = VoiceCallDataFactory::createVoiceCallStatusObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sessionId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Tear down the call.
	 * @param string $sessionId Session ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @return VoiceCallDataObject $dataObj The send data object.
	 */
	public function tearDownCall($sessionId, $secureToken){
		
		if (!empty($sessionId)){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_VOICECALL . '/' . urlencode($sessionId);
			
			// get response
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = VoiceCallDataFactory::createVoiceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sessionId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
