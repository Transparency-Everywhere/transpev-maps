<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for IP Location.
 */
class IpLocationClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_iplocation_rest_url';
	
	/**
	 * Constant of response key, used to build the request URL.
	 */
	const RESPONSE_KEY_IPLOCATION = 'location?ipaddress='; 
	
	/**
	 * Locate IP
	 * @param string $secureToken Security token we got from authentication class.
	 * @param IpLocationParameters $sendParameters send parameters including all set send data.
	 * @return IpLocationDataObject $dataObj data object.
	 */
	public function locateIp($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// build url
			$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey(self::URL_KEY);
			$url = $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . self::RESPONSE_KEY_IPLOCATION . $sendParameters->getIpAddress();
			
			// get response, in this case this is a custom GET request
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = IpLocationDataFactory::createIpLocationObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing send parameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
