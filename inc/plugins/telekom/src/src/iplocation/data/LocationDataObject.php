<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data object for a single ip location.
 */
class LocationDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;
	
	/**
	 * Constructs the data array with the specified values.
	 * @param string $ipAddress IP Address
	 * @param array $isInRegion Array including the country code, region code and region name 
	 * @param string $radius radius
	 */
	public function __construct($ipAddress, $isInRegion, $radius){
		
		$this->data['ipAddress'] = $ipAddress;
		$this->data['countryCode'] = $isInRegion['countryCode'];
		$this->data['regionCode'] = $isInRegion['regionCode'];
		$this->data['regionName'] = $isInRegion['regionName'];
		$this->data['radius'] = $radius;
	}
	
	/**
	 * IP Address this location information refers to
	 * @return string IP address
	 */
	public function getIpAddress(){
		return $this->data['ipAddress'];
	}
	
	/**
	 * Code of the country the IP address belongs to
	 * @return string country code
	 */
	public function getCountryCode(){
		return $this->data['countryCode'];
	}
	
	/**
	 * Reserved for future use
	 * @return string region code
	 */
	public function getRegionCode(){
		return $this->data['regionCode'];
	}
	
	/**
	 * Name of the region (see documentation)
	 * @return string region name
	 */
	public function getRegionName(){
		return $this->data['regionName'];
	}
	
	/**
	 * Field not currently in use.
	 * @return string radius
	 */
	public function getRadius(){
		return $this->data['radius'];
	}
}
