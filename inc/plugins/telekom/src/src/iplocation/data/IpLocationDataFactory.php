<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the IP location data object.
 */
class IpLocationDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates a data object for IP Location from the response array.
	 * @param array $dataArray Data array
	 * @return IpLocationDataObject Data object
	 */
	public static function createIpLocationObject($dataArray) {
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		// set response data
		if (isset($dataArray['results'])){
			$objectsArray['ipLocations'] = self::createLocationData($dataArray['results']);
		}
		
		return new IpLocationDataObject($objectsArray);
	}

	/**
	 * Creates an array including all location data objects.
	 * @param array $dataArray Data array
	 * @return array $locations Array with location objects
	 */
	public static function createLocationData($dataArray) {
		
		$locations = array();
		if (!empty($dataArray) && is_array($dataArray)) {
			foreach ($dataArray as $location) {
				
				// building brand object
				$locationObj = new LocationDataObject($location['ipAddress'], $location['isInRegion'], $location['radius']);
				
				// adding object to array
				$locations[$location['ipAddress']] = $locationObj;
			}
		}
		
		return $locations;
	}
}
