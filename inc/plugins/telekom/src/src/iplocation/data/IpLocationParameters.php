<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for IP location.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class IpLocationParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'ipaddress' 	=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['ipaddress'])){
			return true;
		}
		return false;
	}
	
	/**
	 * Comma separated list of IP adresses, which are to be located.
	 * @param string $ipaddress one or multiple IP addresses
	 */
	public function setIpAddress($ipaddress){
		$this->sendParameters['ipaddress'] = $ipaddress;
	}
	
	/**
	 * Get the IP Address
	 * @return string IP Adress
	 */
	 public function getIpAddress() {
	 	return $this->sendParameters['ipaddress'];
	 }

}
