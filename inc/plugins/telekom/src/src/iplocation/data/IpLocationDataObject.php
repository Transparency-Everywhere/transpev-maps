<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the ip location results.
 */
class IpLocationDataObject extends TelekomDataObject {
	
	/**
	 * ip locations data as an array.
	 * @var array[LocationDataObject]
	 */
	private $ipLocations;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
		
		// set the ip locations data
		if (isset($objectsArray['ipLocations'])){
			$this->ipLocations = $objectsArray['ipLocations'];
		}
	}

	/**
	 * Get the ip locations data array.
	 * @return array[LocationDataObject] locations
	 */
	public function getIpLocations(){
		return $this->ipLocations;
	}
	
	/**
	 * Get an ip location object by IP
	 * @param string $ip Look up IP
	 * @return LocationDataObject location
	 */
	public function getIpLocationById($ip){
		if (isset($this->ipLocations[$ip])){
			return $this->ipLocations[$ip];
		}
	}
}
