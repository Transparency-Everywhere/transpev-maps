<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Send SMS Receive Client to receive sms sent to your app
 */
class SendSmsReceiveClient extends TelekomClient {
	
	/**
	 * Constant of API SendSMS Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_sendsms_base_url';
	
	/**
	 * Constant of SMS response key, used to build the request URL.
	 */
	const RESPONSE_KEY_RECEIVESMS = 'api_sendsms_receive_url'; 
	
	/**
	 * Send the SMS
	 * @param string $secureToken Security token we got from authentication class.
	 * @param SendSmsParameters $sendParameters send parameters including all set send data.
	 * @return SendSmsDataObject $dataObj The send SMS data object.
	 */
	public function sendSmsReceive($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// build url and get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_RECEIVESMS);
			$url = sprintf($url, urlencode($sendParameters->getRegistrationId()));

			$first = true;

			$paramsArray = $sendParameters->getParametersArray();

			if (isset($paramsArray['account'])) {
				$url .= '?account=' . $paramsArray['account'];
				$first = false;
			}
			if (isset($paramsArray['maxBatchSize'])) {
				if ($first) {
					$url .= '?maxBatchSize=' . $paramsArray['maxBatchSize'];
				} else {
					$url .= '&maxBatchSize=' . $paramsArray['maxBatchSize'];
				}
			}

			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = SendSmsDataFactory::createSendSmsDataObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Builds the response URL.
	 * @param $urlKey Config key to get the right URL
	 * @param $responseKey Config key to get the additional response
	 * @return string
	 * @Override TelekomClient::buildResponseUrl($urlKey, $responseKey)
	 */
	protected function buildResponseUrl($urlKey, $responseKey){
		$apiUrl = $this->service->config->getApiBaseUrl() . sprintf($this->service->config->getConfigByKey($urlKey), $this->service->config->getEnvironment());
		$apiUrl = $apiUrl . $this->service->config->getConfigByKey($responseKey);
		return $apiUrl;
	}
}
