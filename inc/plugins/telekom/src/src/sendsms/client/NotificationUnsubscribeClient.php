<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Query Report Client to query the delivery status of a SMS sent.
 */
class NotificationUnsubscribeClient extends TelekomClient {
	
	/**
	 * Constant of API Query Report Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_sendsms_base_url';

	/**
	 * Constant of SMS response key, used to build the request URL.
	 */
	const RESPONSE_KEY_SENDSMS = 'api_sendsms_unsubscribe_url'; 
	
	/**
	 * Send the SMS
	 * @param string $secureToken Security token we got from authentication class.
	 * @param QueryReportParameters $sendParameters send parameters including all set send data.
	 * @return QueryReportDataObject $dataObj The Query Report data object.
	 */
	public function unsubscribeNotifications($secureToken, $sendParameters){
		if ($sendParameters->hasRequiredFields()){
			
			// build url and get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_SENDSMS);
			$url = sprintf($url, urlencode($sendParameters->getSubScriptionId()));

			$response = $this->service->getResponseJSONData($url, $secureToken, null, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = NotificationUnsubscriptionDataFactory::createNotificationUnsubscriptionDataObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Builds the response URL.
	 * @param $urlKey Config key to get the right URL
	 * @param $responseKey Config key to get the additional response
	 * @return string
	 * @Override TelekomClient::buildResponseUrl($urlKey, $responseKey)
	 */
	protected function buildResponseUrl($urlKey, $responseKey){
		$apiUrl = $this->service->config->getApiBaseUrl() . sprintf($this->service->config->getConfigByKey($urlKey), $this->service->config->getEnvironment());
		$apiUrl = $apiUrl . $this->service->config->getConfigByKey($responseKey);
		return $apiUrl;
	}
}
