<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Include the required data objects if not done before.
 */
require_once(dirname(__FILE__).'/OutboundSMSType.php');
require_once(dirname(__FILE__).'/OutboundSMSEncoding.php');

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class SendSmsReceiveParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'account' 				=> null,
			'registrationId' 		=> null,
			'maxBatchSize'			=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['registrationId'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		$paramArray = array();

		if(isset($this->sendParameters['account'])) {
			$paramArray['account'] = $this->sendParameters['account'];
		}

		if(isset($this->sendParameters['maxBatchSize'])) {
			$paramArray['maxBatchSize'] = $this->sendParameters['maxBatchSize'];
		}

		return $paramArray;
	}
	
	/**
	 * Account-ID of the sub account which should be billed for this service call
	 * @param string $account Account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}

	/**
	 * maxBatchSize, number of messages to be be included in the response
	 * @param string $maxBatchSize MaxBatchSize
	 */
	public function setMaxBatchSize($maxBatchSize){
		$this->sendParameters['maxBatchSize'] = $maxBatchSize;
	}

	/**
	 * Is typically a short-code or ‘virtual’ MSISDN agreed with the mobile operator for receipt of SMS messages
	 * @param string $registrationId RegistrationId
	 */
	public function setRegistrationId($registrationId){
		$this->sendParameters['registrationId'] = $registrationId;
	}

	public function getRegistrationId() {
		if (isset($this->sendParameters['registrationId']))
			return $this->sendParameters['registrationId'];
		return null;
	}
}
