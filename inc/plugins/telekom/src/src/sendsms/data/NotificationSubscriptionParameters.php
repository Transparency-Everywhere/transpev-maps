<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class NotificationSubscriptionParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'callbackData' 			=> null,
			'clientCorrelator' 		=> null,
			'notifyURL'				=> null,
			'senderAddress'			=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['notifyURL'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		$paramsArray = array();
		$paramsArray['deliveryReceiptSubscription'] = array();
		if(isset($this->sendParameters['callbackData'])) {
			$paramsArray['deliveryReceiptSubscription']['callbackReference']['callbackData'] = $this->sendParameters['callbackData'];
		}
		if(isset($this->sendParameters['notifyURL'])) {
			$paramsArray['deliveryReceiptSubscription']['callbackReference']['notifyURL'] = $this->sendParameters['notifyURL'];
		}
		if(isset($this->sendParameters['clientCorrelator'])) {
			$paramsArray['deliveryReceiptSubscription']['callbackReference']['clientCorrelator'] = $this->sendParameters['clientCorrelator'];
		}
		return $paramsArray;
	}

	/**
	 * Returns the senderAddress to use for URL creation.
	 * @return string $this->sendParameters['senderAddress'] senderAddress
	 */
	public function getSenderAddress() {
		if(isset($this->sendParameters['senderAddress'])) {
			return $this->sendParameters['senderAddress'];
		}
	}
	
	/**
	 * The url the serivce sends subscription infos to
	 * @param string $notifyURL notifyURL
	 */
	public function setNotifyURL($notifyURL){
		$this->sendParameters['notifyURL'] = $notifyURL;
	}
	
	/**
	 * Uniquely idientifies this create subscription request.
	 * Avoids duplicate subscription on communication errors
	 * when sending the subscription request again with the 
	 * same ClientCorrelator.
	 * @param string $clientCorrelator clientCorrelator
	 */
	public function setClientCorrelator($clientCorrelator){
		$this->sendParameters['clientCorrelator'] = $clientCorrelator;
	}

	/**
	 * Holds a function name or other data that you would like
	 * included when your application is notified.
	 * @param string $callbackData callbackData
	 */
	public function setCallbackData($callbackData){
		$this->sendParameters['callbackData'] = $callbackData;
	}

	/**
	 * Sender, as shown at the receiver
	 * @param string $senderAddress senderAddress
	 */
	public function setSenderAddress($senderAddress){
		$this->sendParameters['senderAddress'] = $senderAddress;
	}
}
