<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the Send SMS data object.
 */
class NotificationUnsubscriptionDataFactory extends TelekomDataFactory {
	
	/**
	 * Creates a query report data object from the response array.
	 * @param array $dataArray Data array
	 * @return QueryReportDataObject Data object
	 */
	public static function createNotificationUnsubscriptionDataObject($dataArray) {
		return new NotificationUnsubscriptionDataObject($dataArray);
	}
}
