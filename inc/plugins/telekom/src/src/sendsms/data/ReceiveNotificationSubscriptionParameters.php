<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ReceiveNotificationSubscriptionParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'callbackData' 			=> null,
			'clientCorrelator' 		=> null,
			'notificationFormat'	=> null,
			'notifyURL'				=> null,
			'criteria'				=> null,
			'destinationAddress'	=> null,
			'account'				=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['notifyURL']) && !empty($this->sendParameters['callbackData'])
			&& !empty($this->sendParameters['destinationAddress']) && !empty($this->sendParameters['notifyURL'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		$paramsArray = array();
		$paramsArray['subscription'] = array();
		if (isset($this->sendParameters['callbackData'])) {
			$paramsArray['subscription']['callbackReference']['callbackData'] = $this->sendParameters['callbackData'];
		}
		if (isset($this->sendParameters['notifyURL'])) {
			$paramsArray['subscription']['callbackReference']['notifyURL'] = $this->sendParameters['notifyURL'];
		}
		if (isset($this->sendParameters['criteria'])) {
			$paramsArray['subscription']['criteria'] = $this->sendParameters['criteria'];
		}
		if (isset($this->sendParameters['destinationAddress'])) {
			$paramsArray['subscription']['destinationAddress'] = $this->sendParameters['destinationAddress'];
		}
		if (isset($this->sendParameters['notificationFormat'])) {
			$paramsArray['subscription']['notificationFormat'] = $this->sendParameters['notificationFormat'];
		}
		if (isset($this->sendParameters['clientCorrelator'])) {
			$paramsArray['subscription']['clientCorrelator'] = $this->sendParameters['clientCorrelator'];
		}
		if (isset($this->sendParameters['account'])) {
			$paramsArray['subscription']['account'] = $this->sendParameters['account'];
		}

		return $paramsArray;
	}
	
	/**
	 * The url the serivce sends subscription infos to
	 * @param string $notifyURL notifyURL
	 */
	public function setNotifyURL($notifyURL){
		$this->sendParameters['notifyURL'] = $notifyURL;
	}

	/**
	 * is the content type that notifications will be sent in
	 * the default format is JSON, values of XML or JSON can be specified
	 * @param string $notificationFormat notificationFormat
	 */
	public function setNotifitcationFormat($notificationFormat) {
		$this->sendParameters['notificationFormat'] = $notificationFormat;
	}

	/**
	 * is case-insensitve text to match against the first word of the message,
	 * ignoring any leading whitespace. This allows you to reuse a short code
	 * among various applications, each of which can register their own
	 * subscription with different criteria.
	 * @param string $criteria criteria
	 */
	public function setCriteria($criteria) {
		$this->sendParameters['criteria'] = $criteria;
	}

	/**
	 * is the MSISDN, or code agreed with the operator, to which people may 
	 * send an SMS to your application
	 * @param string $destinationAddress destinationAddress
	 */
	public function setDestinationAddress($destinationAddress) {
		$this->sendParameters['destinationAddress'] = $destinationAddress;
	}

	/**
	 * specifies with account to charge
	 * @param string $account account
	 */
	public function setAccount($account) {
		$this->sendParameters['account'] = $account;
	}
	
	/**
	 * Uniquely idientifies this create subscription request.
	 * Avoids duplicate subscription on communication errors
	 * when sending the subscription request again with the 
	 * same ClientCorrelator.
	 * @param string $clientCorrelator clientCorrelator
	 */
	public function setClientCorrelator($clientCorrelator){
		$this->sendParameters['clientCorrelator'] = $clientCorrelator;
	}

	/**
	 * Holds a function name or other data that you would like
	 * included when your application is notified.
	 * @param string $callbackData callbackData
	 */
	public function setCallbackData($callbackData){
		$this->sendParameters['callbackData'] = $callbackData;
	}
}
