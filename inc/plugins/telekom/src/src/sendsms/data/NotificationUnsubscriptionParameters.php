<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class NotificationUnsubscriptionParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'subscriptionId' 		=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['subscriptionId'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		return $this->sendParameters;
	}

	/**
	 * Returns the subscriptionId to use for URL creation.
	 * @return string $this->sendParameters['subscriptionId'] subscriptionId
	 */
	public function getSubscriptionId() {
		if(isset($this->sendParameters['subscriptionId'])) {
			return $this->sendParameters['subscriptionId'];
		}
	}

	/**
	 * Sets the subscriptionId to use for URL creation.
	 * @param string subscriptionId
	 */
	public function setSubscriptionId($subscriptionId) {
		$this->sendParameters['subscriptionId'] = $subscriptionId;
	}
}
