<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class QueryReportParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'requestId' 			=> null,
			'senderAddress'	 		=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['requestId']) && !empty($this->sendParameters['senderAddress'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		return $this->sendParameters;
	}
	
	/**
	 * Sender, as shown at the receiver
	 * @param string $senderAddress senderAddress
	 */
	public function setSenderAddress($senderAddress){
		$this->sendParameters['senderAddress'] = $senderAddress;
	}
	
	/**
	 * Request id of the sms that should be queried
	 * @param string $requestId requestId
	 */
	public function setRequestId($requestId){
		$this->sendParameters['requestId'] = $requestId;
	}
}
