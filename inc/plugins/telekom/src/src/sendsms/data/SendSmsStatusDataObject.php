<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Include the Telekom status data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/StatusDataObject.php');

/**
 * SendSms status data object
 */
class SendSmsStatusDataObject extends StatusDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	protected $data;
	
	/**
	 * Constructor sets the data
	 * @param string $data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get requestError.
	 * @return array requestError
	 */
	public function getRequestError(){
		if (isset($this->data['requestError'])){
			return $this->data['requestError'];
		}
	}

	/**
	 * Get policyException.
	 * @return array policyException (messageId, text, variables)
	 */
	public function getPolicyException() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['policyException'])) {
				return $this->data['requestError']['policyException'];
			}
		}
	}

	/**
	 * Get policyException messageId.
	 * @return string policyException messageId
	 */
	public function getPolicyExceptionMessageId() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['policyException'])) {
				if(isset($this->data['requestError']['policyException']['messageId'])) {
					return $this->data['requestError']['policyException']['messageId'];
				}
			}
		}
	}

	/**
	 * Get policyException text.
	 * @return string policyException text
	 */
	public function getPolicyExceptionText() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['policyException'])) {
				if(isset($this->data['requestError']['policyException']['text'])) {
					return $this->data['requestError']['policyException']['text'];
				}
			}
		}
	}

	/**
	 * Get policyException variables.
	 * @return array policyException text
	 */
	public function getPolicyExceptionVariables() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['policyException'])) {
				if(isset($this->data['requestError']['policyException']['variables'])) {
					return $this->data['requestError']['policyException']['variables'];
				}
			}
		}
	}

	/**
	 * Get compiled policyException.
	 * @return string policyException
	 */
	public function getCompiledPolicyException() {
		return "Error " . $this->getPolicyExceptionMessageId() . ": " . vsprintf(preg_replace('/%[0-9].*/', '%s', $this->getPolicyExceptionText()), $this->getPolicyExceptionVariables());
	}

	/**
	 * Get serviceException.
	 * @return array serviceException (messageId, text, variables)
	 */
	public function getServiceException() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['serviceException'])) {
				return $this->data['requestError']['serviceException'];
			}
		}
	}

	/**
	 * Get serviceException messageId.
	 * @return string serviceException messageId
	 */
	public function getServiceExceptionMessageId() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['serviceException'])) {
				if(isset($this->data['requestError']['serviceException']['messageId'])) {
					return $this->data['requestError']['serviceException']['messageId'];
				}
			}
		}
	}

	/**
	 * Get serviceException text.
	 * @return string serviceException text
	 */
	public function getServiceExceptionText() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['serviceException'])) {
				if(isset($this->data['requestError']['serviceException']['text'])) {
					return $this->data['requestError']['serviceException']['text'];
				}
			}
		}
	}

	/**
	 * Get serviceException variables.
	 * @return array serviceException text
	 */
	public function getServiceExceptionVariables() {
		if(isset($this->data['requestError'])) {
			if(isset($this->data['requestError']['serviceException'])) {
				if(isset($this->data['requestError']['serviceException']['variables'])) {
					return $this->data['requestError']['serviceException']['variables'];
				}
			}
		}
	}

	/**
	 * Get compiled serviceException.
	 * @return string serviceException
	 */
	public function getCompiledServiceException() {
		return "Error " . $this->getServiceExceptionMessageId() . ": " . vsprintf(preg_replace('/%[0-9].*/', '%s', $this->getServiceExceptionText()), $this->getServiceExceptionVariables());
	}

	/**
	 * Get compiled output.
	 * @return string Output
	 */
	public function getCompiledOutput() {
	
		if(isset($this->data['requestError']['policyException'])) {
			return $this->getCompiledPolicyException();
		} else if (isset($this->data['requestError']['serviceException'])) {
			return $this->getCompiledServiceException();
		}
	}

	
	/**
	 * Get deliveryInfoList.
	 * @return string deliveryInfoList
	 */
	public function getDeliveryInfoList(){
		if (isset($this->data['deliveryInfoList'])){
			return $this->data['deliveryInfoList'];
		}
	}

	/**
	 * Get deliveryReceiptSubscription.
	 * @return string deliveryReceiptSubscription
	 */
	public function getDeliveryReceiptSubscription() {
		if(isset($this->data['deliveryReceiptSubscription'])){
			return $this->data['deliveryReceiptSubscription'];
		}
	}

	public function getInboundSMSMessageList() {
		if(isset($this->data['inboundSMSMessageList'])) {
			return $this->data['inboundSMSMessageList'];
		}
	}

	public function getReceiveNotificationResponse() {
		if(isset($this->data['subscription'])) {
			return $this->data['subscription'];
		}
	}
}