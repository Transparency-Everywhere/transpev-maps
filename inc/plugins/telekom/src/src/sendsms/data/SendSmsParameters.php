<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Include the required data objects if not done before.
 */
require_once(dirname(__FILE__).'/OutboundSMSType.php');
require_once(dirname(__FILE__).'/OutboundSMSEncoding.php');

/**
 * Handles the needed send parameters data for send SMS.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class SendSmsParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'address' 				=> null,
			'clientCorrelator' 		=> null,
			'message'				=> null,
			'messagetype'			=> null,
			'notifyURL'		 		=> null,
			'callbackData'			=> null,
			'senderAddress'			=> null,
			'senderName'			=> null,
			'account'				=> null,
			'outboundEncoding'		=> null,
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['address']) && !empty($this->sendParameters['message']) && !empty($this->sendParameters['senderAddress'])){
			return true;
		}
		return false;
	}

	/**
	 * Get the send params array.
	 * @return array $sendParameters Send parameters
	 * @Override TelekomSendParameters::getParametersArray()
	 */
	public function getParametersArray() {
		$paramArray = array('outboundSMSMessageRequest' => array());

		if(isset($this->sendParameters['messagetype'])) {
			if ($this->sendParameters['messagetype'] == OutboundSMSType::flashMessage) {
				$paramArray['outboundSMSMessageRequest'][OutboundSMSType::flashMessage] = array('flashMessage' => $this->sendParameters['message']);
			} else if ($this->sendParameters['messagetype'] == OutboundSMSType::binaryMessage) {
				$paramArray['outboundSMSMessageRequest'][OutboundSMSType::binaryMessage] = array('message' => $this->sendParameters['message']);
			} else {
				$paramArray['outboundSMSMessageRequest'][OutboundSMSType::textMessage] = array('message' => $this->sendParameters['message']);
			}
		} else {
				$paramArray['outboundSMSMessageRequest'][OutboundSMSType::textMessage] = array('message' => $this->sendParameters['message']);
		}

		foreach ($this->sendParameters as $key => $value) {
			if ($key == 'address' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest'][$key] = $value;
			}
			if ($key == 'senderAddress' && ($value != null || $value != ''))  {
				$paramArray['outboundSMSMessageRequest'][$key] = $value;
			}
			if ($key == 'senderName' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest'][$key] = $value;	
			}
			if ($key == 'account' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest'][$key] = $value;
			}
			if ($key == 'notifyURL' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest']['receiptRequest'][$key] = $value;
			}
			if ($key == 'callbackData' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest']['receiptRequest'][$key] = $value;
			}
			if ($key == 'clientCorrelator' && ($value != null || $value != '')) {
				$paramArray['outboundSMSMessageRequest'][$key] = $value;	
			}
			if ($key == 'outboundEncoding' && ($value != null || $value != '')) {
				if ($value == OutboundSMSEncoding::UCS) {
					$paramArray['outboundSMSMessageRequest'][$key] = OutboundSMSEncoding::UCS;
				} else {
					$paramArray['outboundSMSMessageRequest'][$key] = OutboundSMSEncoding::GSM;
				}
			}
		}
		return $paramArray;
	}
	
	/**
	 * Phone addresss of the recipient(s), comma separated and each starting with 'tel:<number>'
	 * @param string $address Mobile phone address
	 */
	public function setAddress($address){
		$this->sendParameters['address'] = $address;
	}
	
	/**
	 * Set the message.
	 * @param string $message SMS Text message
	 */
	public function setMessage($message){
		$this->sendParameters['message'] = $message;
	}
	
	/**
	 * Sender, as shown at the receiver
	 * @param string $senderAddress senderAddress
	 */
	public function setSenderAddress($senderAddress){
		$this->sendParameters['senderAddress'] = $senderAddress;
	}
	
	/**
	 * Defines, if the SMS should be sent as text, flash or binary SMS
	 * @param OutboundSMSType messagetype
	 */
	public function setMessageType($messagetype){
		$this->sendParameters['messagetype'] = $messagetype;
	}
	
	/**
	 * Account-ID of the sub account which should be billed for this service call
	 * @param string $account Account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}

	/**
	 * Uniquely idientifies this create subscription request.
	 * Avoids duplicate subscription on communication errors
	 * when sending the subscription request again with the 
	 * same ClientCorrelator.
	 * @param string $clientCorrelator clientCorrelator
	 */
	public function setClientCorrelator($clientCorrelator){
		$this->sendParameters['clientCorrelator'] = $clientCorrelator;
	}

	/**
	 * Holds a function name or other data that you would like
	 * included when your application is notified.
	 * @param string $callbackData callbackData
	 */
	public function setCallbackData($callbackData){
		$this->sendParameters['callbackData'] = $callbackData;
	}
}
