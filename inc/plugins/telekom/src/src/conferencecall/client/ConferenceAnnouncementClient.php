<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for conference announcements.
 */
class ConferenceAnnouncementClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_conferencecall_rest_url';
	
	/**
	 * Constant of conferences announcement set key, used to build the request URL.
	 */
	const RESPONSE_KEY_ANNOUNCEMENT_SET = 'announcementset';
	
	/**
	 * List announcement sets
	 * @param string $secureToken Security token we got from authentication class.
	 * @param string $lang language
	 * @param AnnouncementSetParameters $sendParameters send parameters object
	 * @return ConferenceCallDataFactory $dataObj The send data object.
	 */
	public function getAnnouncementSets($secureToken, $lang, $sendParameters = null){
		
		// get response
		$url = $this->buildResponseUrl(self::URL_KEY, urlencode($lang) . '/' . self::RESPONSE_KEY_ANNOUNCEMENT_SET . '/');
		$sendParametersArray = (is_object($sendParameters)) ? $sendParameters->getParametersArray() : array();
		if (isset($sendParametersArray['clientid']))
			$url += "?clientid=".urlencode($sendParametersArray['clientid']);
		$response = $this->service->getResponseStandardData($url, $secureToken, $sendParametersArray, 'GET');
		$responseArray = $this->service->transformResponseDataToArray($response);
		
		// create data object
		$dataObj = ConferenceAnnouncementDataFactory::createAnnouncementSetsObject($responseArray);
		return $dataObj;
	}
	
	/**
	 * Upload an announcement WAVE file.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param UploadAnnouncementParameters $sendParameters send parameters object
	 * @param string $lang language
	 * @param string $announcementId annaouncement ID
	 * @return ConferenceCallDataFactory $dataObj The send data object.
	 */
	public function uploadAnnouncement($secureToken, $sendParameters, $lang, $announcementId = '', $clientId = ''){
		
		if ($sendParameters->hasRequiredFields()){
			
			// check whether client id is set otherwise call without
			$clientId = ($clientId != '') ? "?clientid=".urlencode($clientId) : "";
			
			// build url
			$url = $this->buildResponseUrl(self::URL_KEY, urlencode($lang) . '/' . self::RESPONSE_KEY_ANNOUNCEMENT_SET . '/' . urlencode($announcementId).$clientId);
			
			// get response
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceAnnouncementDataFactory::createAnnouncementObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Delete an announcement set
	 * @param string $secureToken Security token we got from authentication class.
	 * @param string $lang language
	 * @param string $announcementId annaouncement ID
	 * @param AnnouncementSetParameters $sendParameters send parameters object
	 * @return ConferenceCallDataFactory $dataObj The send data object.
	 */
	public function deleteAnnouncementSet($secureToken, $lang, $announcementId, $sendParameters = null){
		
		// get response
		$url = $this->buildResponseUrl(self::URL_KEY, urlencode($lang) . '/' . self::RESPONSE_KEY_ANNOUNCEMENT_SET . '/' . urlencode($announcementId));
		$sendParametersArray = (is_object($sendParameters)) ? $sendParameters->getParametersArray() : array();
		if (isset($sendParametersArray['clientid']))
			$url += "?clientid=".urlencode($sendParametersArray['clientid']);
		$response = $this->service->getResponseStandardData($url, $secureToken, $sendParametersArray, 'DELETE');
		$responseArray = $this->service->transformResponseDataToArray($response);
		
		// create data object
		$dataObj = ConferenceAnnouncementDataFactory::createAnnouncementObject($responseArray);
		return $dataObj;
	}
}
