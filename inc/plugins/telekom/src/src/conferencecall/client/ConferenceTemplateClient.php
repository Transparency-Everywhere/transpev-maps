<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for conference template.
 */
class ConferenceTemplateClient extends TelekomClient {
	
	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_conferencecall_rest_url';
	
	/**
	 * Constant of conference template standard response key, used to build the request URL.
	 */
	const RESPONSE_KEY_CONFERENCETEMPLATE = 'conferencetemplate';
	
	/**
	 * Constant of conference templates response key, used to build the request URL.
	 */
	const RESPONSE_KEY_CONFERENCETEMPLATES = 'conferencetemplates';
	
	/**
	 * Constant of conference template standard response key, used to build the request URL.
	 */
	const RESPONSE_KEY_TEMPLATE_PARTICIPANTS = 'participants';
	
	/**
	 * Create a new conference template.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceTemplateCreateParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceTemplateCreateDataObject $dataObj The send data object.
	 */
	public function createConferenceTemplate($secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceTemplateDataFactory::createConferenceTemplateCreateObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Update a conference template.
	 * @param string $templateId Template ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceTemplateUpdateParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function updateConferenceTemplate($templateId, $secureToken, $sendParameters){
		
		if ($sendParameters->hasRequiredFields()){
			
			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId));
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'PUT');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Delete a conference template.
	 * @param string $templateId Template ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function removeConferenceTemplate($templateId, $secureToken){
		
		if (!empty($templateId)){
			
			// build url
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Get details on a conference template.
	 * @param string $templateId Template ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceSingleTemplateDataObject $dataObj The send data object.
	 */
	function getConferenceTemplate($templateId, $secureToken) {
		
		if (!empty($templateId)){
			
			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceTemplateDataFactory::createSingleTemplateObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Get conference template list.
	 * @param string $ownerId Owner ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return TemplateListDataObject $dataObj The send data object.
	 */
	function getConferenceTemplateList($ownerId, $secureToken) {
		
		if (!empty($ownerId)){
			
			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATES . '/' . urlencode($ownerId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceTemplateDataFactory::createTemplateListObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $ownerID in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Adds a conference template participant.
	 * @param string $templateId Template ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceTemplateAddParticipantParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallAddParticipantDataObject $dataObj The send data object.
	 */
	public function addConferenceTemplateParticipant($templateId, $secureToken, $sendParameters){
		
		if (!empty($templateId) && $sendParameters->hasRequiredFields()){
			
			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId) . '/' . self::RESPONSE_KEY_TEMPLATE_PARTICIPANTS);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallAddParticipantObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId or $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Gets a conference template participant.
	 * @param string $templateId Template ID
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceTemplateParticipantDataObject $dataObj The send data object.
	 */
	public function getConferenceTemplateParticipant($templateId, $participantId, $secureToken){
		
		if (!empty($templateId) && !empty($participantId)){
			
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId) . '/' . self::RESPONSE_KEY_TEMPLATE_PARTICIPANTS . '/' . urlencode($participantId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceTemplateDataFactory::createConferenceTemplateParticipantObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId or $participantId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Update of conference template participant.
	 * @param string $templateId Template ID
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceTemplateUpdateParticipantParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function updateConferenceTemplateParticipant($templateId, $participantId, $secureToken, $sendParameters){
		
		if (!empty($templateId) && !empty($participantId)){
			
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId) . '/' . self::RESPONSE_KEY_TEMPLATE_PARTICIPANTS . '/' . urlencode($participantId));
			$response = $this->service->getResponseStandardData($url, $secureToken,$sendParameters->getParametersArray(), 'PUT');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId or $participantId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Delete a conference template participant.
	 * @param string $templateId Template ID
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function removeConferenceTemplateParticipant($templateId, $participantId, $secureToken){
		
		if (!empty($templateId) && !empty($participantId)){
			
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCETEMPLATE . '/' . urlencode($templateId) . '/' . self::RESPONSE_KEY_TEMPLATE_PARTICIPANTS . '/' . urlencode($participantId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);
			
			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $templateId or $participantId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
