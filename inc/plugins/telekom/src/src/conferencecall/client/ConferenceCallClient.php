<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client for conference call.
 */
class ConferenceCallClient extends TelekomClient {

	/**
	 * Constant of API Rest URL part, used to build the request URL.
	 */
	const URL_KEY = 'api_conferencecall_rest_url';

	/**
	 * Constant of conference call standard response key, used to build the request URL.
	 */
	const RESPONSE_KEY_CONFERENCECALL = 'conference';

	/**
	 * Constant of conferences response key, used to build the request URL.
	 */
	const RESPONSE_KEY_CONFERENCES = 'conferences';

	/**
	 * Constant of participants response key, used to build the request URL.
	 */
	const RESPONSE_KEY_PARTICIPANTS = 'participants';

	/**
	 * Constant of running status response key, used to build the request URL.
	 */
	const RESPONSE_KEY_RUNSTATUS = 'runstatus';

	/**
	 * Create a new conference call.
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceCallCreateParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function createConference($secureToken, $sendParameters){

		if ($sendParameters->hasRequiredFields()){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallCreateObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Commit (save/start) a conference.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallDataObject conference call data object
	 */
	public function commitConference($conferenceId, $secureToken){

		if (!empty($conferenceId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Get the conference status.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @param integer $what What kind of data is needed.
	 * 0 = Display all information, 1 = Display only detail, account and startTime,
	 * 2 = Display only participants, 3 = Display only schedule
	 * @return ConferenceCallStatusDataObject $dataObj The send data object.
	 */
	public function getConferenceStatus($conferenceId, $secureToken, $what = 0){

		if (!empty($conferenceId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '?what=' . ((int)$what));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createGetConferenceStatusObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Remove a conference call.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	public function removeConference($conferenceId, $secureToken){

		if (!empty($conferenceId)){

			// build url
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Add a participant to the conference call.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceCallAddParticipantParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallAddParticipantDataObject $dataObj The send data object.
	 */
	public function addParticipant($conferenceId, $secureToken, $sendParameters){

		if (!empty($conferenceId) && $sendParameters->hasRequiredFields()){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '/' . self::RESPONSE_KEY_PARTICIPANTS);
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'POST');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallAddParticipantObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId or required fields in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Get the participant status.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ParticipantStatusDataObject $dataObj The send data object.
	 */
	public function getParticipantStatus($conferenceId, $participantId, $secureToken){

		if (!empty($conferenceId) && !empty($participantId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '/participants/' . urlencode($participantId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createParticipantStatusObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId or $participantId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Update participant.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceCallParticipantParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallDataObject $dataObj data object
	 */
	function updateParticipant($conferenceId, $participantId, $secureToken, $sendParameters) {

		if (!empty($conferenceId) && !empty($participantId) && $sendParameters->hasRequiredFields()){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '/' . self::RESPONSE_KEY_PARTICIPANTS . '/' . urlencode($participantId));
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'PUT');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId or $participantID or required fields fields in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Remove participant.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $participantId Participant ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	function removeParticipant($conferenceId, $participantId, $secureToken) {

		if (!empty($conferenceId) && !empty($participantId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '/' . self::RESPONSE_KEY_PARTICIPANTS . '/' . urlencode($participantId));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'DELETE');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId or $participantID fields in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Get the conference list.
	 * @param string $ownerID Owner ID
	 * @param string $secureToken Security token we got from authentication class.
	 * @param integer $what What kind of data is needed:
	 * 0 = Display all conferences,
	 * 1 = Display ad-hoc conferences only,
	 * 2 = Display planned conferences only,
	 * 3 = Display failed or not commited conferences only,
	 * 4 = Display not commited conferences only
	 * @return ConferenceListDataObject $dataObj The send data object.
	 */
	function getConferenceList($ownerID, $secureToken, $what = null) {

		if (!empty($ownerID)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCES . '?ownerID=' . urlencode($ownerID) . '&what=' . urlencode($what));
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferencesListObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $ownerID in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Update conference.
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @param ConferenceCallConferenceParameters $sendParameters send parameters including all set send data.
	 * @return ConferenceCallDataObject $dataObj The send data object.
	 */
	function updateConference($conferenceId, $secureToken, $sendParameters) {

		if (!empty($conferenceId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId));
			$response = $this->service->getResponseStandardData($url, $secureToken, $sendParameters->getParametersArray(), 'PUT');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId fields in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Request the running instance of a planned conference
	 * @param string $conferenceId Conference ID of the actual call
	 * @param string $secureToken Security token we got from authentication class.
	 * @return ConferenceCallGetRunningObject $dataObj The send data object.
	 */
	function getRunningConference($conferenceId, $secureToken) {

		if (!empty($conferenceId)){

			// get response
			$url = $this->buildResponseUrl(self::URL_KEY, self::RESPONSE_KEY_CONFERENCECALL . '/' . urlencode($conferenceId) . '/' . self::RESPONSE_KEY_RUNSTATUS);
			$response = $this->service->getResponseCustomData($url, $secureToken, 'GET');
			$responseArray = $this->service->transformResponseDataToArray($response);

			// create data object
			$dataObj = ConferenceCallDataFactory::createConferenceCallGetRunningObject($responseArray);
			return $dataObj;
		}
		else {
			throw new TelekomException('Missing $conferenceId in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
}
