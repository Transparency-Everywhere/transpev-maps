<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Data container object to get the conference template details data.
 */
class TemplateParticipantDataObject {
	
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		$this->data = array();
		if (is_array($objectsArray)){
			foreach ($objectsArray as $key => $val){
				$this->data[$key] = $val;
			}
		}
	}
	
	/**
	 * Get participant ID.
	 * @return string participant ID 
	 */
	public function getParticipantId(){
		if (isset($this->data['participantID'])){
			return $this->data['participantID'];
		}
	}
	
	/**
	 * Get number.
	 * @return string number 
	 */
	public function getNumber(){
		if (isset($this->data['number'])){
			return $this->data['number'];
		}
	}
	
	/**
	 * Get first name.
	 * @return string first name 
	 */
	public function getFirstName(){
		if (isset($this->data['firstName'])){
			return $this->data['firstName'];
		}
	}
	
	/**
	 * Get last name.
	 * @return string last name 
	 */
	public function getLastName(){
		if (isset($this->data['lastName'])){
			return $this->data['lastName'];
		}
	}
	
	/**
	 * Get E-Mail.
	 * @return string E-Mail 
	 */
	public function getEmail(){
		if (isset($this->data['email'])){
			return $this->data['email'];
		}
	}
	
	/**
	 * Is the new participant the initiator of the conference?
	 * @return boolean true, if the participant is the initiator
	 */
	public function getIsInitiator(){
		if (isset($this->data['isInitiator']) && $this->data['isInitiator'] == 'true'){
			return true;
		}
		return false;
	}
	
	/**
	 * Specifies if a participant is called by the conference service.
	 * If true, participant is called, else participant has to dial-in.
	 * @return boolean dial out
	 */
	public function getDialOut(){
		if (isset($this->data['dialOut']) && $this->data['dialOut'] == 'true'){
			return true;
		}
		return false;
	}
}
