<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for conference call create request.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceCallCreateParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the  parameters data array.
	 */
	public function __construct(){

		$this->sendParameters = array(
			'ownerID'		=> null,
			'name'			=> null,
			'description'	=> null,
		);
	}

	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['ownerID']) && !empty($this->sendParameters['name']) && !empty($this->sendParameters['description'])){
			return true;
		}
		return false;
	}

	/**
	 * Set the owner ID.
	 * @param string $ownerID owner ID
	 */
	public function setOwnerId($ownerID){
		$this->sendParameters['ownerID'] = $ownerID;
	}

	/**
	 * Set the conference name.
	 * @param string $name conference name
	 */
	public function setName($name){
		$this->sendParameters['name'] = $name;
	}

	/**
	 * Set the conference description.
	 * @param string $description conference description
	 */
	public function setDescription($description){
		$this->sendParameters['description'] = $description;
	}

	/**
	 * Parameter not currently in use.
	 * @param string $notify not used
	 */
	public function setNotify($notify){
		$this->sendParameters['notify'] = $notify;
	}

	/**
	 * If this parameter is not specified, the conference is handled as an ad-hoc conference and
	 * is initiated immediately after it is saved in the Conference Call Server using `commitConference`.
	 * However, if the parameter is specified, it is a planned conference. It is initiated at the specified time.
	 * @param integer $timestamp unix timestamp of the planned conference start
	 */
	public function setTimestamp($timestamp){
		if (!is_integer($timestamp)){
			throw new TelekomException('Wrong datatype - timestamp must be an integer in ' . __METHOD__. ' - line ' . __LINE__);
		}
		if (date_default_timezone_set('UTC')){
			$this->sendParameters['timestamp'] = date('Y-m-d', $timestamp) . 'T' . date('H:i:s', $timestamp) . 'Z';
		}
		else {
			throw new TelekomException('Could not set default timezone to UTC in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Maximum duration of the conference in seconds.
	 * A conference begins as soon as the initiator pushes the # key.
	 * @param string $duration duration
	 */
	public function setDuration($duration){
		$this->sendParameters['duration'] = $duration;
	}

	/**
	 * Series type:
	 * 0 - No repeat, 1 - Repeated hourly, 2 - Repeated daily, 3 - Repeated weekly, 4 - Repeated monthly
	 * @param string $recurring recurring mode and intervall
	 */
	public function setRecurring($recurring){
		$this->sendParameters['recurring'] = $recurring;
	}

	/**
	 * Is the confirmation of the participant to join the conference required?
	 * If "true", the participant must confirm to join the conference by pressing the # key.
	 * @param string $joinConfirm value of the flag
	 */
	public function setJoinConfirm($joinConfirm){
		$this->sendParameters['joinConfirm'] = $joinConfirm;
	}

	/**
	 * PIN for the dial-in of the conferences initiator. If omitted, the service assigns a value.
	 * @param string $initiatorAccessPin pin
	 */
	public function setInitiatorAccessPin($initiatorAccessPin){
		$this->sendParameters['initiatorAccessPin'] = $initiatorAccessPin;
	}

	/**
	 * PIN for the dial-in of a conference participant. If omitted, the service assigns a value.
	 * @param string $participantAccessPin pin
	 */
	public function setParticipantAccessPin($participantAccessPin){
		$this->sendParameters['participantAccessPin'] = $participantAccessPin;
	}

	/**
	 * ID for choosing an individual announcement that has been uploaded before.
	 * @param string $announcementSet announcement set
	 */
	public function setAnnouncementSet($announcementSet){
		$this->sendParameters['announcementSet'] = $announcementSet;
	}

	/**
	 * Language of the announcements
	 * @param string $language language
	 */
	public function setLanguage($language){
		$this->sendParameters['language'] = $language;
	}

	/**
	 * Defines which Caller ID will be shown to the participants,
	 * possible values: "moderator", "conference".
	 * @param string $serviceCid service cid
	 */
	public function setServiceCid($serviceCid){
		$this->sendParameters['serviceCid'] = $serviceCid;
	}

	/**
	 * Specifies if dial-in into the conference is prohibited. "true" to disallow dial-in.
	 * @param string $dialInLocked dial in locked
	 */
	public function setDialInLocked($dialInLocked){
		$this->sendParameters['dialInLocked'] = $dialInLocked;
	}

	/**
	 * Account ID of the sub-account that should be billed for the service use.
	 * If the parameter is not specified, the main account is billed.
	 * @param string $account account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}
}
