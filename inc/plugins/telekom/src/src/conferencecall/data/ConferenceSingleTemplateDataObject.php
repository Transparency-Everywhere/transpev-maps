<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get a single template result.
 */
class ConferenceSingleTemplateDataObject extends TelekomDataObject {
	
	/**
	 * template details data.
	 * @var TemplateDetailsDataObject
	 */
	protected $templateDetails = null;
	
	/**
	 * template participants
	 * @var array
	 */
	protected $templateParticipants = array();
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		
		// set the status
		$this->initResponseStatus($objectsArray);
		
		// set the conference ID
		if (isset($objectsArray['conference'])){
			
			if (!empty($objectsArray['conference']['participants'])){
				foreach ($objectsArray['conference']['participants'] as $participant){
					$this->templateParticipants[] = new TemplateParticipantDataObject($participant);
				}
				unset($objectsArray['conference']['participants']);
			}
			$this->templateDetails = new TemplateDetailsDataObject($objectsArray['conference']);
		}
	}
	
	/**
	 * Get the template details.
	 * @return TelekomStdDataObject
	 */
	public function getTemplateDetails(){
		return $this->templateDetails;
	}
	
	/**
	 * Get the template participants.
	 * @return array
	 */
	public function getTemplateParticipants(){
		return $this->templateParticipants;
	}
}
