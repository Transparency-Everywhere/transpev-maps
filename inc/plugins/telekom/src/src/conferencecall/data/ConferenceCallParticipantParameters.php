<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters to update a participant.
 * If one of the properties firstname, lastname, number, email, isInitiator, dialOut is set,
 * all properties have to be set or the missing ones will be replaced with default values.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceCallParticipantParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){

		$this->sendParameters = array();
	}

	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		return true;
	}

	/**
	 * Sets the first name
	 * @param string $firstName first name
	 */
	public function setFirstName($firstName){
		$this->sendParameters['firstName'] = $firstName;
	}

	/**
	 * Sets the last name
	 * @param string $lastName last name
	 */
	public function setLastName($lastName){
		$this->sendParameters['lastName'] = $lastName;
	}

	/**
	 * Sets the telephone number
	 * @param string $number telephone number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}

	/**
	 * Sets the E-Mail
	 * @param string $email E-Mail
	 */
	public function setEmail($email){
		$this->sendParameters['email'] = $email;
	}

	/**
	 * Sets if the participant is the initiator
	 * @param string $isInitiator is initiator
	 */
	public function setIsInitiator($isInitiator){
		$this->sendParameters['isInitiator'] = $isInitiator;
	}

	/**
	 * Sets the dial out.
	 * @param string $dialOut dial out
	 */
	public function setDialOut($dialOut){
		$this->sendParameters['dialOut'] = $dialOut;
	}

	/**
	 * Sets the action.
	 * @param string $action action
	 */
	public function setAction($action){
		$this->sendParameters['action'] = $action;
	}
	
	/**
	 * @param int $joinConfirmDialIn Specifies if a participant has to press
	 *        *-key to enter (=1) a Dial-In Conference or not (=0)
	 */
	public function setJoinConfirmDialIn($joinConfirmDialIn) {
		return $this->sendParameters['joinConfirmDialIn'] = $joinConfirmDialIn;
	}
	
	/**
	 * @param int $joinConfirmDialOut Specifies if a participant has to press
	 *        #-key to enter (=1) a Dial-Out Conference or not (=0)
	 */
	public function setJoinConfirmDialOut() {
		return $this->sendParameters['joinConfirmDialOut'] = $joinConfirmDialOut;
	}
}
