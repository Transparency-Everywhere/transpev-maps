<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the conference template data objects.
 */
class ConferenceTemplateDataFactory extends TelekomDataFactory {
		
	/**
	 * Creates a Telekom data object containing the ID of the new template
	 * @param array $dataArray Data array
	 * @return TemplateListDataObject Data object
	 */	
	public static function createConferenceTemplateCreateObject($dataArray) {
		// create the object parts
		$objectsArray = array(
				'status' => parent::createStatus($dataArray['status']),
		);
		
		// set response data
		if (isset($dataArray['templateId'])){
			$objectsArray['templateId'] = $dataArray['templateId'];
		}
		
		return new ConferenceTemplateCreateDataObject($objectsArray);
	}
	
	/**
	 * Creates a Telekom data object containing a list of templates
	 * @param array $dataArray Data array
	 * @return TemplateListDataObject Data object
	 */
	public static function createTemplateListObject($dataArray){
	
		// create the object parts
		$objectsArray = array(
				'status' => parent::createStatus($dataArray['status']),
		);
	
		// set response data
		if (isset($dataArray['conferenceTemplateIds'])){
			$objectsArray['conferenceTemplateIds'] = $dataArray['conferenceTemplateIds'];
		}
	
		return new TemplateListDataObject($objectsArray);
	}	
	
	/**
	 * Creates a Telekom data object for conference templates from the response array.
	 * @param array $dataArray Data array
	 * @return ConferenceTemplateDataObject Data object
	 */
	public static function createConferenceTemplateParticipantObject($dataArray){
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
	
		// set response data
		if (isset($dataArray['participant'])){
			$objectsArray['participant'] = $dataArray['participant'];
		}
	
		return new ConferenceTemplateParticipantDataObject($objectsArray);
	}
	
	/**
	 * Creates a Telekom data object for a single template from the response array.
	 * @param array $dataArray Data array
	 * @return ConferenceSingleTemplateDataObject Data object
	 */
	public static function createSingleTemplateObject($dataArray){
		
		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);
		
		// set response data
		if (isset($dataArray['conference'])){
			$objectsArray['conference'] = $dataArray['conference'];
		}
		
		return new ConferenceSingleTemplateDataObject($objectsArray);
	}
}
