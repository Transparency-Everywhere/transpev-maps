<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for the update template request.
 * All properties have to be provided or they will be replaced with default values.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceTemplateUpdateParameters extends TelekomSendParameters implements TelekomParameters {
	
	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		// in this case all parameters are optional
		return true;
	}
	
	/**
	 * Sets the initiator of the conference template.
	 * @param string $initiatorID initiator ID
	 */
	public function setInitiatorId($initiatorID){
		$this->sendParameters['initiatorId'] = $initiatorID;
	}
	
	/**
	 * Sets the conference name
	 * @param string $name conference name
	 */
	public function setName($name){
		$this->sendParameters['name'] = $name;
	}
	
	/**
	 * Sets the conference description
	 * @param string $description conference description
	 */
	public function setDescription($description){
		$this->sendParameters['description'] = $description;
	}
	
	/**
	 * Maximum duration of the conference in seconds.
	 * A conference begins as soon as the initiator pushes the # key.
	 * @param string $duration duration
	 */
	public function setDuration($duration){
		$this->sendParameters['duration'] = $duration;
	}
	
	/**
	 * Is the confirmation of the participant to join the conference required?
	 * "true" if participant has to press the # key to join the conference.
	 * @param string $joinConfirm join confirm
	 */
	public function setJoinConfirm($joinConfirm){
		$this->sendParameters['joinConfirm'] = $joinConfirm;
	}
	
	/**
	 * PIN for the dial-in of the conferences initiator. If omitted, the service assigns a value.
	 * @param string $initiatorAccessPin pin
	 */
	public function setInitiatorAccessPin($initiatorAccessPin){
		$this->sendParameters['initiatorAccessPin'] = $initiatorAccessPin;
	}
	
	/**
	 * PIN for the dial-in of a conference participant. If omitted, the service assigns a value.
	 * @param string $participantAccessPin pin
	 */
	public function setParticipantAccessPin($participantAccessPin){
		$this->sendParameters['participantAccessPin'] = $participantAccessPin;
	}
	
	/**
	 * ID for choosing an individual announcement that has been uploaded before.
	 * @param string $announcementSet announcement set
	 */
	public function setAnnouncementSet($announcementSet){
		$this->sendParameters['announcementSet'] = $announcementSet;
	}
	
	/**
	 * Language of the announcements
	 * @param string $language language
	 */
	public function setLanguage($language){
		$this->sendParameters['language'] = $language;
	}
	
	/**
	 * Defines which Caller ID will be shown to the participants.
	 * Possible options are: "moderator" and "conference".
	 * @param string $serviceCid service cid
	 */
	public function setServiceCid($serviceCid){
		$this->sendParameters['serviceCid'] = $serviceCid;
	}
	
	/**
	 * Specifies if dial-in into the conference is prohibited. "true" to disallow dial-in.
	 * @param string $dialInLocked dial in locked
	 */
	public function setDialInLocked($dialInLocked){
		$this->sendParameters['dialInLocked'] = $dialInLocked;
	}
}
