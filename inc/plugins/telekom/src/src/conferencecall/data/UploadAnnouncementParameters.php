<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for uploading an announcement.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class UploadAnnouncementParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'type' 			=> '',
			'audioFile' 	=> '',
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		
		foreach ($this->sendParameters as $key => $val){
			if (empty($this->sendParameters[$key])){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Sets the filename of the audio file in WAVE format.
	 * @param string $audioFile audio file
	 */
	public function setAudioFile($audioFile){
		$this->sendParameters['audioFile'] = '@' . $audioFile;
	}
	
	/**
	 * Sets the announcement type. Please refer to documentation for options.
	 * @param string $type announcement type
	 */
	public function setType($type){
		$this->sendParameters['type'] = $type;
	}
}
