<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Creates a data container object to get the conference detail results.
 */
class ConferenceDetailDataObject {

	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;

	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){

		$this->data = $objectsArray;
	}

	/**
	 * Short name of the conference
	 * @return string name
	 */
	public function getName(){
		if (isset($this->data['name'])){
			return $this->data['name'];
		}
	}

	/**
	 * Description of the conference
	 * @return string description
	 */
	public function getDescription(){
		if (isset($this->data['description'])){
			return $this->data['description'];
		}
	}

	/**
	 * Maximum duration of the conference in seconds.
	 * @return string duration
	 */
	public function getDuration(){
		if (isset($this->data['duration'])){
			return $this->data['duration'];
		}
	}

	/**
	 * Get the initiator access pin.
	 * @return string Pin
	 */
	public function getInitiatorAccessPin(){
		if (isset($this->data['initiatorAccessPin'])){
			return $this->data['initiatorAccessPin'];
		}
	}

	/**
	 * Get the participant access pin.
	 * @return string Pin
	 */
	public function getParticipantAccessPin(){
		if (isset($this->data['participantAccessPin'])){
			return $this->data['participantAccessPin'];
		}
	}

	/**
	 * ID of the chosen announcement set
	 * @return announcement set ID
	 */
	public function getAnnouncementSet() {
		if (isset($this->data['announcementSet'])) {
			return $this->data['announcementSet'];
		}
	}
	
	/**
	 * Language of the announcements
	 * @return string language
	 */
	public function getLanguage(){
		if (isset($this->data['language'])){
			return $this->data['language'];
		}
	}	
	
	/**
	 * Defines which caller ID will be shown to the participants.
	 * "moderator" or "conference"
	 * @return string service Cid
	 */
	public function getServiceCid(){
		if (isset($this->data['serviceCid'])){
			return $this->data['serviceCid'];
		}
	}

	/**
	 * Do the participants have to confirm joining the conference?
	 * @return boolean join confirm
	 */
	public function getJoinConfirm(){
		if (isset($this->data['joinConfirm'])){
			return $this->data['joinConfirm'];
		}
	}

	/**
	 * Specifies if dial-in into the conference is prohibited.
	 * "true" if dial-in is disallowed.
	 * @return boolean dialin locked
	 */
	public function getDialInLocked(){
		if (isset($this->data['dialInLocked'])){
			return $this->data['dialInLocked'];
		}
	}
	
	/**
	 * @return Specifies if a participant has to press *-key to
	 * 		   enter (=1) a Dial-In Conference or not (=0)
	 */
	public function getJoinConfirmDialIn() {
		if(isset($this->data['joinConfirmDialIn'])) {
			return $this->data['joinConfirmDialIn'];
		}
	}
	
	/**
	 * @return Specifies if a participant has to press #-key to
	 * 		   enter (=1) a Dial-Out Conference or not (=0)
	 */
	public function getJoinConfirmDialOut() {
		if(isset($this->data['joinConfirmDialOut'])) {
			return $this->data['joinConfirmDialOut'];
		}
	}
	
}
