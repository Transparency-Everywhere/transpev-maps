<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters to add a participant to a template
 * Fields that are omitted, are set to default values, e.g. empty strings.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceTemplateAddParticipantParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = array(
			'number'			=> '',
			'isInitiator'		=> '',
		);
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		if (!empty($this->sendParameters['number']) && !empty($this->sendParameters['isInitiator'])){
			return true;
		}
		return false;
	}
	
	/**
	 * Set the number of the participant.
	 * A service number is displayed as caller ID to the initiator.
	 * All other participants see the number of the initiator as caller ID.
	 * @param string $number number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}
	
	/**
	 * Set wheather the participant is initiator.
	 * @param boolean $isInitiator initiator
	 */
	public function setIsInitiator($isInitiator){
		$this->sendParameters['isInitiator'] = $isInitiator;
	}
	
	/**
	 * Set the first name of the participant.
	 * @param string $firstName name
	 */
	public function setFirstName($firstName){
		$this->sendParameters['firstName'] = $firstName;
	}
	
	/**
	 * Set the last name of the participant.
	 * @param string $lastName name
	 */
	public function setLastName($lastName){
		$this->sendParameters['lastName'] = $lastName;
	}
	
	/**
	 * E-mail address of the participant.
	 * @param string $email email
	 */
	public function setEmail($email){
		$this->sendParameters['email'] = $email;
	}
	
	/**
	 * Defines if the participant is called by the service.
	 * @param string $dialOut dial out
	 */
	public function setDialOut($dialOut){
		$this->sendParameters['dialOut'] = $dialOut;
	}
}
