<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the Telekom data object if not done before.
 */
require_once(dirname(__FILE__).'/../../common/data/TelekomDataObject.php');


/**
 * Creates a data container object to get the conference call status results.
 */
class ConferenceCallStatusDataObject extends TelekomDataObject {

	protected $account;

	protected $detail;

	protected $participants;

	protected $startTime;

	protected $schedule;

	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){

		// set the status
		$this->initResponseStatus($objectsArray);

		// set account
		if (isset($objectsArray['account'])){
			$this->account = $objectsArray['account'];
		}

		if ($objectsArray['detail'] instanceof ConferenceDetailDataObject){
			$this->detail = $objectsArray['detail'];
		}

		if (!empty($objectsArray['participants'])){
			$this->participants = $objectsArray['participants'];
		}

		if (!empty($objectsArray['startTime'])){
			$this->startTime = $objectsArray['startTime'];
		}

		if (!empty($objectsArray['schedule'])){
			$this->schedule = $objectsArray['schedule'];
		}
	}

	/**
	 * Sub-Account ID. Null if main account.
	 * @return string
	 */
	public function getAccount(){
		return $this->account;
	}

	/**
	 * Details about the conference
	 * @return ConferenceDetailDataObject details
	 */
	public function getDetail(){
		return $this->detail;
	}

	/**
	 * Get the participants array.
	 * @return array[ParticipantDataObject] participants
	 */
	public function getParticipants(){
		return $this->participants;
	}

	/**
	 * Starting time of a conference in UTC time.
	 * Null if the conference is scheduled.
	 * @return string sart time
	 */
	public function getStartTime(){
		return $this->startTime;
	}

	/**
	 * Scheduling information. Null if not a scheduled conference.
	 * @return ScheduleDataObject schedule
	 */
	public function getSchedule(){
		return $this->schedule;
	}
}
