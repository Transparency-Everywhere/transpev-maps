<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Creates a data container object for a participant.
 */
class ParticipantDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		$this->data = $objectsArray;
	}
	
	/**
	 * Status information on the participant
	 * @return string status
	 */
	public function getParticipantStatus(){
		if (isset($this->data['participantStatus'])){
			return $this->data['participantStatus'];
		}
	}
	
	/**
	 * Get the participant ID.
	 * @return string participant ID
	 */
	public function getParticipantId(){
		if (isset($this->data['participantID'])){
			return $this->data['participantID'];
		}
	}
	
	/**
	 * Get the number.
	 * @return string number
	 */
	public function getNumber(){
		if (isset($this->data['number'])){
			return $this->data['number'];
		}
	}
	
	/**
	 * Get the first name.
	 * @return string first name
	 */
	public function getFirstName(){
		if (isset($this->data['firstName'])){
			return $this->data['firstName'];
		}
	}
	
	/**
	 * Get the last name.
	 * @return string last name
	 */
	public function getLastName(){
		if (isset($this->data['lastName'])){
			return $this->data['lastName'];
		}
	}
	
	/**
	 * Get the E-Mail.
	 * @return string E-Mail
	 */
	public function getEmail(){
		if (isset($this->data['email'])){
			return $this->data['email'];
		}
	}
	
	/**
	 * "true", if this participant is the initiator of the conference.
	 * @return boolean is initiator
	 */
	public function getIsInitiator(){
		if (isset($this->data['isInitiator'])){
			return $this->data['isInitiator'];
		}
	}
	
	/**
	 * Specifies if a participant is called by the conference service.
	 * If "true", participant is called, else participant has to dial-in.
	 * @return boolean dial out
	 */
	public function getDialOut(){
		if (isset($this->data['dialOut'])){
			return $this->data['dialOut'];
		}
	}
}
