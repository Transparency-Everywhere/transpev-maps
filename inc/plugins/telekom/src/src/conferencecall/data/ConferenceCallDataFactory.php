<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Factory to build the conference call data object.
 */
class ConferenceCallDataFactory extends TelekomDataFactory {

	/**
	 * Creates a Telekom data object for conference call from the response array.
	 * @param array $dataArray Data array
	 * @return ConferenceCallDataObject Data object
	 */
	public static function createConferenceCallObject($dataArray){

		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);

		return new ConferenceCallDataObject($objectsArray);
	}
	
	/**
	 * Creates a data object containing the response after creating a conference
	 * @param array $dataArray Data array
	 * @return ConferenceCallDataObject Data object
	 */
	public static function createConferenceCallCreateObject($dataArray){
	
		// create the object parts
		$objectsArray = array(
				'status' => parent::createStatus($dataArray['status']),
		);
	
		// set response data
		if (isset($dataArray['conferenceId'])){
			$objectsArray['conferenceId'] = $dataArray['conferenceId'];
		}
	
		return new ConferenceCallCreateDataObject($objectsArray);
	}
	
	/**
	 * Creates a data object containing the response of querying the running instance
	 * @param array $dataArray Data array
	 * @return ConferenceCallGetRunningObject Data object
	 */
	public static function createConferenceCallGetRunningObject($dataArray){
	
		// create the object parts
		$objectsArray = array(
				'status' => parent::createStatus($dataArray['status']),
		);
	
		// set response data
		if (isset($dataArray['conferenceId'])){
			$objectsArray['conferenceId'] = $dataArray['conferenceId'];
		}
	
		return new ConferenceCallGetRunningDataObject($objectsArray);
	}	
	
	/**
	 * Creates a Telekom data object for the response of adding a participant
	 * @param array $dataArray Data array
	 * @return ConferenceCallAddParticipantDataObject Data object
	 */
	public static function createConferenceCallAddParticipantObject($dataArray){
	
		// create the object parts
		$objectsArray = array(
				'status' => parent::createStatus($dataArray['status']),
		);

		// set response data
		if (isset($dataArray['participantID'])){
			$objectsArray['participantID'] = $dataArray['participantID'];
		}
	
		return new ConferenceCallAddParticipantDataObject($objectsArray);
	}	

	/**
	 * Creates a Telekom data object for participant status from the response array.
	 * @param array $dataArray Data array
	 * @return ParticipantStatusDataObject Data object
	 */
	public static function createParticipantStatusObject($dataArray){

		// create the object parts
		$objectsArray = array(
			'status' => parent::createStatus($dataArray['status']),
		);

		// set response data
		if (isset($dataArray['participantStatus'])){
			$objectsArray['participantStatus'] = new ParticipantSingleStatusDataObject($dataArray['participantStatus']);
		}

		return new ParticipantStatusDataObject($objectsArray);
	}

	/**
	 * Creates a Telekom data object from the response array with the conference status data.
	 * @param array $dataArray Data array
	 * @return ConferenceCallDataObject Data object
	 */
	public static function createGetConferenceStatusObject($dataArray){

		// create the object parts
		$objectsArray = array(
			'status' 		=> parent::createStatus($dataArray['status']),
			'account' 		=> null,
			'detail' 		=> null,
			'participants' 	=> null,
			'startTime' 	=> null,
			'schedule'		=> null,
		);

		// set response data
		if (isset($dataArray['conference']['account'])){
			$objectsArray['account'] = $dataArray['conference']['account'];
		}
		if (isset($dataArray['conference']['detail'])){
			$objectsArray['detail'] = new ConferenceDetailDataObject($dataArray['conference']['detail']);
		}
		if (isset($dataArray['conference']['participants'])){
			$objectsArray['participants'] = self::createParticipants($dataArray['conference']['participants']);
		}
		if (isset($dataArray['conference']['startTime'])){
			$objectsArray['startTime'] = $dataArray['conference']['startTime'];
		}
		if (isset($dataArray['conference']['schedule'])){
			$objectsArray['schedule'] = new ScheduleDataObject($dataArray['conference']['schedule']);
		}

		return new ConferenceCallStatusDataObject($objectsArray);
	}

	/**
	 * Creates a Telekom data object from the response array with the conferences list data.
	 * @param array $dataArray Data array
	 * @return ConferenceListDataObject Data object
	 */
	public static function createConferencesListObject($dataArray){

		// create the object parts
		$objectsArray = array(
			'status' 		=> parent::createStatus($dataArray['status']),
			'conferences' 	=> null,
		);

		// set response data
		if (isset($dataArray['conferences'])){
			foreach ($dataArray['conferences'] as $confArr){
				$confIdObj = new SingleConferenceDataObject($confArr['conferenceID']);
				$objectsArray['conferences'][$confIdObj->getConferenceID()] = $confIdObj;
			}
		}

		return new ConferenceListDataObject($objectsArray);
	}

	/**
	 * Creates a array with all participants including singe data objects
	 * @param array $participantsArray participants
	 * @return array $participantsData participants
	 */
	protected static function createParticipants($participantsArray){

		$participantsData = array();
		foreach($participantsArray as $participant) {

			// prepare Array
			$singlePart = $participant;
			$participantStatus = (isset($participant['status'])) ? $participant['status'] : '';
			$singlePart['participantStatus'] = new ParticipantSingleStatusDataObject($participantStatus);
			$participantsData[] = new ParticipantDataObject($singlePart);
		}
		return $participantsData;
	}
}
