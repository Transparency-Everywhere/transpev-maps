<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for conference template create request.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceTemplateCreateParameters extends TelekomSendParameters implements TelekomParameters {
	
	/**
	 * Required fields.
	 * @var array
	 */
	protected $requiredFields = array(
		'ownerID'		=> '',
		'name'			=> '',
		'description'	=> '',
		'firstName'		=> '',
		'lastName'		=> '',
		'email'			=> '',
		'number'		=> '',
	);
	
	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){
		
		$this->sendParameters = $this->requiredFields;
	}
	
	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		foreach ($this->requiredFields as $key => $val){
			if (empty($this->sendParameters[$key])){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * A unique ID of the owner of the conference template (e.g. "max.mustermann").
	 * The length of the ID must be between 3 and 39 characters.
	 * @param string $ownerID owner ID
	 */
	public function setOwnerId($ownerID){
		$this->sendParameters['ownerID'] = $ownerID;
	}
	
	/**
	 * Sets the conference name
	 * @param string $name conference name
	 */
	public function setName($name){
		$this->sendParameters['name'] = $name;
	}
	
	/**
	 * Sets the conference description
	 * @param string $description conference description
	 */
	public function setDescription($description){
		$this->sendParameters['description'] = $description;
	}
	
	/**
	 * Set the first name of the initiator.
	 * @param string $firstName name
	 */
	public function setFirstName($firstName){
		$this->sendParameters['firstName'] = $firstName;
	}
	
	/**
	 * Set the last name of the initiator.
	 * @param string $lastName name
	 */
	public function setLastName($lastName){
		$this->sendParameters['lastName'] = $lastName;
	}
	
	/**
	 * Set the email of the initiator.
	 * @param string $email email
	 */
	public function setEmail($email){
		$this->sendParameters['email'] = $email;
	}
	
	/**
	 * Set the number of the initiator.
	 * A service number is displayed as caller ID to the initiator. All other participants see the number of the initiator as caller ID.
	 * @param string $number number
	 */
	public function setNumber($number){
		$this->sendParameters['number'] = $number;
	}
		
	/**
	 * Specifies whether the initiator has to dial-in on his/her own or gets called by the system.
	 * @param string $dialOut dial out
	 */
	public function setDialOut($dialOut){
		$this->sendParameters['dialOut'] = $dialOut;
	}	
	
	/**
	 * Maximum duration of the conference in seconds. A conference begins as soon as the initiator pushes the # key.
	 * @param string $duration duration
	 */
	public function setDuration($duration){
		$this->sendParameters['duration'] = $duration;
	}
	
	/**
	 * Sets if a confirmation of the participant to join the conference is required.
	 * @param string $joinConfirm join confirm
	 */
	public function setJoinConfirm($joinConfirm){
		$this->sendParameters['joinConfirm'] = $joinConfirm;
	}
	
	/**
	 * PIN for the dial-in of the conferences initiator. If omitted, the service assigns a value.
	 * @param string $initiatorAccessPin pin
	 */
	public function setInitiatorAccessPin($initiatorAccessPin){
		$this->sendParameters['initiatorAccessPin'] = $initiatorAccessPin;
	}
	
	/**
	 * PIN for the dial-in of a conference participant. If omitted, the service assigns a value.
	 * @param string $participantAccessPin pin
	 */
	public function setParticipantAccessPin($participantAccessPin){
		$this->sendParameters['participantAccessPin'] = $participantAccessPin;
	}
	
	/**
	 * Sets ID for choosing an individual announcement that has been uploaded before.
	 * @param string $announcementSet announcement set
	 */
	public function setAnnouncementSet($announcementSet){
		$this->sendParameters['announcementSet'] = $announcementSet;
	}
	
	/**
	 * Sets the language of the announcements
	 * @param string $language language
	 */
	public function setLanguage($language){
		$this->sendParameters['language'] = $language;
	}
	
	/**
	 * Defines which Caller ID will be shown to the participants.
	 * Possible options are: "moderator", "conference".
	 * @param string $serviceCid service cid
	 */
	public function setServiceCid($serviceCid){
		$this->sendParameters['serviceCid'] = $serviceCid;
	}
	
	/**
	 * Specifies if dial-in into the conference is prohibited. "true" to disallow dial-in.
	 * @param string $dialInLocked dial in locked
	 */
	public function setDialInLocked($dialInLocked){
		$this->sendParameters['dialInLocked'] = $dialInLocked;
	}

}
