<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Creates a data container object for a participant status.
 */
class ParticipantSingleStatusDataObject {

	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;

	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		$this->data = $objectsArray;
	}

	/**
	 * Get the number.
	 * @return string number
	 */
	public function getNumber(){
		if (isset($this->data['number'])){
			return $this->data['number'];
		}
	}

	/**
	 * Current status of the participant
	 * @return string status
	 */
	public function getStatus(){
		if (isset($this->data['status'])){
			return $this->data['status'];
		}
	}

	/**
	 * Reason if the connection has failed.
	 * @return string last reason
	 */
	public function getLastReason(){
		if (isset($this->data['last_reason'])){
			return $this->data['last_reason'];
		}
	}

	/**
	 * Is the participant currently muted?
	 * @return boolean muted
	 */
	public function getMuted(){
		if (isset($this->data['muted'])){
			return $this->data['muted'];
		}
	}

	/**
	 * Time of the last status update
	 * @return string last access time
	 */
	public function getLastAccessTime(){
		if (isset($this->data['last_access_time'])){
			return $this->data['last_access_time'];
		}
	}

	/**
	 * State before the current state.
	 * @return string previous status
	 */
	public function getPrevStatus(){
		if (isset($this->data['prev_status'])){
			return $this->data['prev_status'];
		}
	}

	/**
	 * Did the initiator mute all participants?
	 * @return boolean initiator muted
	 */
	public function getInitiatorMuted(){
		if (isset($this->data['initiator_muted'])){
			return $this->data['initiator_muted'];
		}
	}
}
