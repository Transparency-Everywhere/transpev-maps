<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Handles the needed send parameters data for conference call update request.
 * Implements the TelekomParameters interface, because the method hasRequiredFields() is required.
 */
class ConferenceCallUpdateParameters extends TelekomSendParameters implements TelekomParameters {

	/**
	 * Initializing the send parameters data array.
	 */
	public function __construct(){

		$this->sendParameters = array();
	}

	/**
	 * Check all required parameters.
	 * @return bool
	 */
	public function hasRequiredFields(){
		return true;
	}

	/**
	 * Sets the conference name
	 * @param string $name conference name
	 */
	public function setName($name){
		$this->sendParameters['name'] = $name;
	}
	
	/**
	 * Sets the ownerID
	 * @param string $ownerID ownerID
	 */
	public function setOwnerID($ownerID){
		$this->sendParameters['ownerID'] = $ownerID;
	}

	/**
	 * Sets the conference description
	 * @param string $description conference description
	 */
	public function setDescription($description){
		$this->sendParameters['description'] = $description;
	}

	/**
	 * Maximum duration of the conference in seconds.
	 * A conference begins as soon as the initiator pushes the # key.
	 * @param string $duration duration
	 */
	public function setDuration($duration){
		$this->sendParameters['duration'] = $duration;
	}

	/**
	 * If this parameter is not specified, the conference is handled as an ad-hoc conference
	 * and is initiated immediately after it is saved in the Conference Call Server using
	 * `commitConference`.
	 * However, if the parameter is specified, it is a planned conference and is initiated at the specified time.
	 * @param string $timestamp unit timestamp of the conference start
	 */
	public function setTimestamp($timestamp){
		if (!is_integer($timestamp)){
			throw new TelekomException('Wrong datatype - timestamp must be an integer in ' . __METHOD__. ' - line ' . __LINE__);
		}
		if (date_default_timezone_set('UTC')){
			$this->sendParameters['timestamp'] = date('Y-m-d', $timestamp) . 'T' . date('H:i:s', $timestamp) . 'Z';
		}
		else {
			throw new TelekomException('Could not set default timezone to UTC in ' . __METHOD__. ' - line ' . __LINE__);
		}		
	}
	
	/**
	 * Parameter not currently in use.
	 * @param string $notify unused
	 */
	public function setNotify($notify){
		$this->sendParameters['notify'] = $notify;
	}	

	/**
	 * Series type: 0- No repeat, 1- Repeated hourly,
	 * 2- Repeated daily, 3- Repeated weekly, 4- Repeated monthly
	 * @param string $recurring recurring
	 */
	public function setRecurring($recurring){
		$this->sendParameters['recurring'] = $recurring;
	}
	
	/**
	 * Sets the ID of the initiator of the conference.
	 * The initiator can only be modified before the conference is saved using `commitConference`.
	 * @param string $initiatorId initiator ID
	 */
	public function setInitiatorId($initiatorId){
		$this->sendParameters['initiatorId'] = $initiatorId;
	}
	
	/**
	 * Sets if a confirmationto join the conference is required.
	 * "true" to require participants to press the # key to enter the conference.
	 * @param string $joinConfirm join confirm
	 */
	public function setJoinConfirm($joinConfirm){
		$this->sendParameters['joinConfirm'] = $joinConfirm;
	}

	/**
	 * PIN for the dial-in of the conferences initiator. If omitted, the service assigns a value.
	 * @param string $initiatorAccessPin pin
	 */
	public function setInitiatorAccessPin($initiatorAccessPin){
		$this->sendParameters['initiatorAccessPin'] = $initiatorAccessPin;
	}

	/**
	 * PIN for the dial-in of a conference participant. If omitted, the service assigns a value.
	 * @param string $participantAccessPin pin
	 */
	public function setParticipantAccessPin($participantAccessPin){
		$this->sendParameters['participantAccessPin'] = $participantAccessPin;
	}

	/**
	 * Specifies if dial-in into the conference is prohibited. "true" to disallow dial-in.
	 * @param string $dialInLocked dial in locked
	 */
	public function setDialInLocked($dialInLocked){
		$this->sendParameters['dialInLocked'] = $dialInLocked;
	}	
	
	/**
	 * Defines which Caller ID will be shown to the participants.
	 * Possible options are:
	 * `moderator` - Show number of moderator
	 * `conference` - Show conference number
	 * @param string $serviceCid service cid
	 */
	public function setServiceCid($serviceCid){
		$this->sendParameters['serviceCid'] = $serviceCid;
	}	
	
	/**
	 * ID for choosing an individual announcement that has been uploaded before.
	 * @param string $announcementSet announcement set
	 */
	public function setAnnouncementSet($announcementSet){
		$this->sendParameters['announcementSet'] = $announcementSet;
	}

	/**
	 * Language of the announcements
	 * @param string $language language
	 */
	public function setLanguage($language){
		$this->sendParameters['language'] = $language;
	}

	/**
	 * Account ID of the sub-account that should be billed for the service use. If the parameter is not specified, the main account is billed.
	 * @param string $account account
	 */
	public function setAccount($account){
		$this->sendParameters['account'] = $account;
	}
}
