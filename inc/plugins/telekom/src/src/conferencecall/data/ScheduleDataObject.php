<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Creates a data container object to get the conference schedule data.
 */
class ScheduleDataObject {

	private $data;

	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		$this->data = $objectsArray;
	}

	/**
	 * Planned start of the conference in ISO8601 format, e.g. 2012-12-31T12:30:00.000Z
	 * @return string timestamp
	 */
	public function getTimestamp(){
		if (isset($this->data['timestamp'])){
			return $this->data['timestamp'];
		}
	}

	/**
	 * Information about if and how often the conference is repeated.
	 * 0 = Not recurring, 1 = Hourly, 2 = Daily, 3 = Wekkly, 4 = Monthly
	 * @return string recurring
	 */
	public function getRecurring(){
		if (isset($this->data['recurring'])){
			return $this->data['recurring'];
		}
	}

	/**
	 * Parameter not in use
	 * @return string notify
	 */
	public function getNotify(){
		if (isset($this->data['notify'])){
			return $this->data['notify'];
		}
	}
}
