<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Creates a data container object for a template ID.
 */
class TemplateIdDataObject {
	
	private $data;
	
	/**
	 * Constructs the data object with the specified values.
	 * @param array $objectsArray Data array
	 */
	public function __construct($objectsArray){
		$this->data = $objectsArray;
	}
	
	/**
	 * Get template ID.
	 * @return string template ID
	 */
	public function getTemplateId(){
		if (isset($this->data['templateId'])){
			return $this->data['templateId'];
		}
	}
}
