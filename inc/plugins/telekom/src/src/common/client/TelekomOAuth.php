<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Class to handle the Telekom API OAUth 2.0 authentification.
 */
class TelekomOAuth extends TelekomAuth {


	
	/**
	 * Refresh token, needed in TelekomOAuth.
	 * @var string
	 */
	protected $refreshToken = null;
	
	/**
	 * Perform the OAuth authentification and get the access token.
	 */
	public function requestAccessToken($scope = ''){
		$url = $this->service->config->getApiOauthUrl() . $this->service->config->getApiOauthUrlTokens();
		$params = array(
			'grant_type' 		=> urlencode($this->service->config->getApiOauthGrantTypeAuth()),
			'scope'				=> urlencode($this->service->config->getOauthScope()),
		);
		
		// get response data
		$fieldsString = TelekomClient::addParameterToUrl('', $params, false);
		$fieldsString = trim($fieldsString, '&');
		$responseArray = $this->obGetResponseOAuthData($url, count($params), $fieldsString);
		
		// set token data
		if (empty($responseArray['error'])){
			$this->setTokenData($responseArray);
		}
		else {
			throw new TelekomException('OAuth Authentication failed in ' . __METHOD__. ' - line ' . __LINE__ . '; Error Message: ' . $responseArray['error']);
		}
	}
	
	/**
	 * Uses the refresh token to get a new access token
	 * @param $refreshToken refresh token
	 */
	public function requestRefreshToken($refreshToken){
		
		$url = $this->service->config->getApiOauthUrl() . $this->service->config->getApiOauthUrlTokens();
		$params = array(
			'client_id' 		=> $this->service->config->getOauthClientId(),
			'client_secret'		=> $this->service->config->getOauthClientSecret(),
			'grant_type' 		=> $this->service->config->getApiOauthGrantTypeRefresh(),
			'refresh_token' 	=> $refreshToken,
		);
		
		// get response data
		$fieldsString = TelekomClient::addParameterToUrl('', $params, false);
		$responseArray = $this->obGetResponseOAuthData($url, count($params), $fieldsString);
		
		// set token data
		if (empty($responseArray['error'])){
			$this->setTokenData($responseArray);
		}
		else {
			throw new TelekomException('OAuth refresh token failed in ' . __METHOD__. ' - line ' . __LINE__ . '; Error Message: ' . $responseArray['error']);
		}
	}
	
	/**
	 * Revoke OAuth refresh token.
	 * @param $token refresh token
	 * @return boolean
	 */
	public function requestRevoke($token){
		
		$url = $this->service->config->getApiOauthUrl() . $this->service->config->getApiOauthUrlRevoke();
		$params = array(
			'client_id' 		=> $this->service->config->getOauthClientId(),
			'client_secret'		=> $this->service->config->getOauthClientSecret(),
			'token'			 	=> $token,
			'token_type' 		=> $this->service->config->getApiOauthGrantTypeRefresh(),
		);
		
		// get response data
		$fieldsString = TelekomClient::addParameterToUrl('', $params, false);
		$responseArray = $this->obGetResponseOAuthData($url, count($params), $fieldsString);
		
		// set token data
		if (empty($responseArray['error'])){
			return true;
		}
		else {
			throw new TelekomException('OAuth revoke failed in ' . __METHOD__. ' - line ' . __LINE__ . '; Error Message: ' . $responseArray['error']);
		}
	}
	
	/**
	 * get response OAuth data with output buffering 
	 * @param string $url
	 * @param array $params
	 */
	private function obGetResponseOAuthData($url, $count, $fieldsString){		
		// get content
		$result = $this->service->getResponseOAuthData($url, $count, $fieldsString);
		
		// get response object
		return $this->service->transformResponseDataToArray($result);
	}
	
	/**
	 * Set the accessToken, refreshToken (optional) and the accessTokenValidUntil variables
	 * @param string $tokenData
	 */
	private function setTokenData($responseArray){
		
		if (isset($responseArray['access_token'])){
			$this->setAccessToken($responseArray['access_token']);
		}
		if (isset($responseArray['refresh_token'])){
			$this->setRefreshToken($responseArray['refresh_token']);
		}
		if (isset($responseArray['expires_in'])){
			$this->setAccessTokenValidUntil(time() + $responseArray['expires_in']);
		}
	}
	
	/**
	 * Sets the refresh token.
	 * @param string $refreshToken Token
	 */
	public function setRefreshToken($refreshToken){
		$this->refreshToken = $refreshToken;
	}
	
	/**
	 * Get the refresh token.
	 * @return string the refresh token
	 */
	public function getRefreshToken() {
		return $this->refreshToken;
	}
	
	/**
	 * Check if token is still valid.
	 * @return bool
	 */
	public function hasValidToken(){
		if ($this->getAccessTokenValidUntil() > time()){
			return true;
		}
		else {
			$refreshToken = $this->getRefreshToken();
			if (!empty($refreshToken)){
				$this->requestRefreshToken($refreshToken);
				if ($this->getAccessTokenValidUntil() > time()){
					return true;
				}
			}
		}
		return false;
	}
}
