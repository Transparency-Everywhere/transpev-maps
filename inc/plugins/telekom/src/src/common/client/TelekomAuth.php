<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Class with the Telekom API basic authentification methods.
 */
class TelekomAuth {
	
	/**
	 * Access token.
	 * @var string
	 */
	protected $accessToken = null;
	
	/**
	 * Access token valid until.
	 * @var string
	 */
	protected $accessTokenValidUntil = null;
	
	/**
	 * Service object that handles the API requests (e.g. JSON).
	 * The service object could be replaced by another service class e.h. SOAP.
	 * @var object
	 */
	protected $service = null;
	
	/**
	 * The construction requires the service object.
	 * @param object $service
	 */
	public function __construct($service){
		$this->service = $service;
	}
	
	/**
	 * Sets the access token.
	 * @param string $accessToken Token
	 */
	public function setAccessToken($accessToken){
		$this->accessToken = $accessToken;
	}
	
	/**
	 * Sets the validity of access token.
	 * @param string $validUntil Valid until
	 */
	public function setAccessTokenValidUntil($validUntil){
		$this->accessTokenValidUntil = $validUntil;
	}
	
	/**
	 * Get the access token.
	 * @return string Token
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}
	
	/**
	 * Get the validity of access token.
	 * @return string
	 */
	public function getAccessTokenValidUntil() {
		return $this->accessTokenValidUntil;
	}
	
	/**
	 * Check if token is still valid.
	 * @return bool
	 */
	public function hasValidToken(){
		if ($this->getAccessTokenValidUntil() > time()){
			return true;
		}
		return false;
	}
}

