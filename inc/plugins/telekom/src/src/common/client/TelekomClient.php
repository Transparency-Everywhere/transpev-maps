<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Client class with some basic functions.
 */
class TelekomClient {

	/**
	 * Service Object
	 * @param TelekomJSONService $service
	 */
	protected $service = null;

	/**
	 * Initialize and set the service object
	 * @param object $service
	 */
	public function __construct($service){
		$this->service = $service;
	}

	/**
	 * Builds the response URL.
	 * @param $urlKey Config key to get the right URL
	 * @param $responseKey Config key to get the additional response
	 * @return string
	 */
	protected function buildResponseUrl($urlKey, $responseKey){
		$apiUrl = $this->service->config->getApiBaseUrl() . $this->service->config->getConfigByKey($urlKey);
		return $apiUrl . '/' . $this->service->config->getEnvironment() . '/' . $responseKey;
	}

	/**
	 * Get response and transrorm to an array.
	 * @param $responseUrl URL to send the query to
	 * @param $secureToken access token
	 * @param $sendParameters parameters for the request
	 * @param $customRequest HTTP Method, defaults to 'POST'
	 * @return array Response data array
	 * @throws TelekomException
	 */
	protected function getResponseArray($responseUrl, $secureToken, $sendParameters, $customRequest = 'POST'){

		if ($sendParameters->hasRequiredFields()){

			// get response
			$requestString = $this->service->transformArrayToResponseData($sendParameters->getParametersArray());
			$response = $this->service->getResponseJSONData($responseUrl, $secureToken, $requestString, $customRequest);
			return $this->service->transformResponseDataToArray($response);
		}
		else {
			throw new TelekomException('Missing $sendParameters in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}

	/**
	 * Adds key value pairs to an url.
	 * @param string $url URL
	 * @param array $parametersArray data array
	 * @param bool $useFirstSeparator if to start parameter string with a question mark
	 */
	public static function addParameterToUrl($url, $parametersArray, $useFirstSeparator = true){

		$first = ($useFirstSeparator) ? '?' : '';
		if (!empty($parametersArray) && is_array($parametersArray)){
			foreach ($parametersArray as $key => $val){

				if (!empty($val)){
					$url .= (!empty($first) && !strstr($url, $first)) ? $first : '&';
					$url .= $key . '=' . urlencode($val);
				}
			}
		}
		return $url;
	}
}
