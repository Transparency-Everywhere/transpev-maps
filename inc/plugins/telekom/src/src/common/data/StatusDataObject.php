<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Status data object
 */
class StatusDataObject {
	
	/**
	 * Data array with key value pairs.
	 * @var array
	 */
	private $data;
	
	/**
	 * Constructor sets the data
	 * @param string $data
	 */
	public function __construct($data){
		$this->data = $data;
	}
	
	/**
	 * Get status code.
	 * @return string status code
	 */
	public function getStatusCode(){
		if (isset($this->data['statusCode'])){
			return $this->data['statusCode'];
		}
	}
	
	/**
	 * Get status message.
	 * @return string status message
	 */
	public function getStatusMessage(){
		if (isset($this->data['statusMessage'])){
			return $this->data['statusMessage'];
		}
	}
}
