<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Class to handle the configuration values.
 */
class TelekomConfig {
	
	/**
	 * Constant for version of this SDK
	 * @var string
	 */
	const SDK_VERSION 		= 'Telekom PHP SDK/5.0';
	
	/**
	 * Constant for user agent in auth request
	 * @var string
	 */
	const SDK_AUTH 	= 'OAuth realm="developergarden.com"';
	
	/**
	 * Constant for accept in auth request
	 * @var string
	 */
	const SDK_ACCEPT 		= 'application/json';
	
	/**
	 * Constant for Content-type in auth request
	 * @var string
	 */
	const SDK_CONTENT_TYPE 	= 'application/json';
	
	/**
	 * Configuration Data Array
	 * @var array
	 */
	private static $confData = array();

	/**
	 * Constructor sets the configuration array.
	 * @param array $confArray
	 */
	public function __construct($confArray){
		$this->setConfigData($confArray);
	}
	
	/**
	 * Sets the configuration data
	 * @param array $confArray
	 */
	public function setConfigData($confArray){
		if (empty($confArray)){
			throw new TelekomException('Missing $confArray in ' . __METHOD__. ' - line ' . __LINE__);
		}
		self::$confData = $confArray;
	}
	
	/**
	 * Get the config value by key.
	 * @param string $key
	 * @return string
	 */
	public function getConfigByKey($key){
		if (isset(self::$confData[$key])){
			return self::$confData[$key];
		}
	}
	
	/**
	 * Get the version of this SDK.
	 * @return string
	 */
	public function getConstant($constName){
		$refObj = new ReflectionObject($this);
		return $refObj->getConstant($constName);
	}

	/**
	 * Dynamic magic method to get a value from the config array.
	 * The Method Name begins with a "get".
	 * The Name is camelCase e.g. "getEnvironment".
	 * @param string $methodName
	 * @param array $arguments
	 * @return string
	 */
	public function __call($methodName, $arguments){

		if (substr($methodName, 0, 3) == 'get'){
			preg_match_all('/(?:^|\p{Lu})\P{Lu}*/iu', $methodName, $mArray);
			if (is_array($mArray[0])){
				$key = substr(strtolower(implode('_', $mArray[0])), 4);
				if (isset(self::$confData[$key])){
					return $this->getConfigByKey($key);
				}
			}
		}
		else {
			throw new TelekomException('function '.$methodName.' could not be executed, the name has to start with "get".');
		}
	}
}
