<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Include the Telekom Basic Service class if not done before.
 */
require_once(dirname(__FILE__).'/../data/TelekomService.php');


/**
 * Service Class to handle the data via JSON.
 */
class TelekomJSONService implements TelekomService {
	
	/**
	 * Telekom Config object.
	 * @var TelekomConfig
	 */
	public $config;
	
	/**
	 * Additional curl options as an array.
	 * @var array
	 */
	public $additionalOptions;
	
	/**
	 * Constructor gets and sets the Telekom Config object.
	 * @param TelekomConfig $config
	 * @param array $additionalOptions cURL options to set for each request
	 */
	public function __construct($config, $additionalOptions = array()){
		$this->config = $config;
		$this->additionalOptions = $additionalOptions;
	}
	
	/**
	 * Get response standard request data.
	 * @param string $url Response URL
	 * @param string $secureToken Secure token
	 * @param array $requestArray Request array data
	 * @param string $customRequest HTTP method
	 * @return string Response string
	 */
	public function getResponseStandardData($url, $secureToken, $requestArray, $customRequest = 'POST'){
		
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$curlOptions = array(
			CURLOPT_CUSTOMREQUEST => $customRequest,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => array(
				'Authorization: ' . $this->config->getConstant('SDK_AUTH') . ',oauth_token="'.$secureToken.'"',
				'User-Agent: ' . $this->config->getConstant('SDK_VERSION'),
				'Accept: ' . $this->config->getConstant('SDK_ACCEPT'),
			),
			CURLOPT_POSTFIELDS => $requestArray
		);
		
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}

	/**
	 * Get response request data with additional headers
	 * @param string $url Response URL
	 * @param string $secureToken Secure token
	 * @param array $requestArray Request array data
	 * @param string $customRequest HTTP method
	 * @return string Response string
	 */
	public function getResponseAdditionalHeaderData($url, $secureToken, $requestArray, $customRequest = 'POST', $additionalHeaders = null){
		
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$headers = array(
			'Authorization: ' . $this->config->getConstant('SDK_AUTH') . ',oauth_token="'.$secureToken.'"',
			'User-Agent: ' . $this->config->getConstant('SDK_VERSION'),
			'Accept: ' . $this->config->getConstant('SDK_ACCEPT'),
		);

		if ($additionalHeaders != null) {
			foreach ($additionalHeaders as $key => $value) {
				array_push($headers, $value);
			}
		}

		$curlOptions = array(
			CURLOPT_CUSTOMREQUEST => $customRequest,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POSTFIELDS => $requestArray
		);
		
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}
	
	/**
	 * Get post response data.
	 * Will be used e.g. in autoscout24.
	 * @param string $url Response URL
	 * @param string $secureToken Secure token
	 * @param string $requestString JSON Request string
	 * @param string $customRequest HTTP method
	 * @return string Response string
	 */
	public function getResponseJSONData($url, $secureToken, $requestString, $customRequest = 'POST'){
		
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$curlOptions = array(
			CURLOPT_CUSTOMREQUEST => $customRequest,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => array(
				'Authorization: ' . $this->config->getConstant('SDK_AUTH') . ', oauth_token="'.$secureToken.'"',
				'User-Agent: ' . $this->config->getConstant('SDK_VERSION'),
				'Accept: ' . $this->config->getConstant('SDK_ACCEPT'),
				'Content-Type: ' . $this->config->getConstant('SDK_CONTENT_TYPE'),
			),
			CURLOPT_POSTFIELDS => $requestString
		);
		
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}
	
	/**
	 * Get response data custom method.
	 * Will be used e.g. in autoscout24.
	 * @param string $url Response URL
	 * @param string $secureToken Secure token
	 * @param string $customRequest Request string
	 * @return string Response string
	 */
	public function getResponseCustomData($url, $secureToken, $customRequest){
		
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$curlOptions = array(
			CURLOPT_CUSTOMREQUEST => $customRequest,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => array(
				'Authorization: ' . $this->config->getConstant('SDK_AUTH') . ', oauth_token="'.$secureToken.'"',
				'User-Agent: ' . $this->config->getConstant('SDK_VERSION'),
				'Accept: ' . $this->config->getConstant('SDK_ACCEPT'),
			),
		);
		
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}
	
	/**
	 * Get the response token data from API.
	 * @var string $url Request URL
	 * @return string Response string
	 */
	public function getResponseTokenData($url){
		
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$curlOptions = array(
			CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
			CURLOPT_USERPWD => $this->config->getUsername().':'.$this->config->getPassword(),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HEADER => true,
			CURLOPT_FAILONERROR => true,
		);
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}
	
	/**
	 * Get the response token data from API.
	 * @var string $url Request URL
	 * @return string Response string
	 */
	public function getResponseOAuthData($url, $count, $fieldsString){
		// init the session
		$curlSession = $this->curlInit($url);
		
		// set the options
		$curlOptions = array(
			CURLOPT_USERPWD => $this->config->getOauthClientId() . ':' . $this->config->getOauthClientSecret(),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => array("Content-Type: application/x-www-form-urlencoded"),
			CURLOPT_POST => $count,
			CURLOPT_POSTFIELDS => $fieldsString
		);
		$this->curlSetOptions($curlSession, $curlOptions);
		
		// return the response
		return $this->curlExecute($curlSession);
	}
	
	/**
	 * Curl Initialisation by curl_init.
	 * @param string $url
	 * @return Resource $curlSession Curl Session Ressource
	 */
	protected function curlInit($url){
		
		if (!empty($url)){
			$curlSession = curl_init($url);
			if ($curlSession){
				return $curlSession;
			}
			else {
				throw new TelekomException('curl_init failed in ' . __METHOD__. ' - line ' . __LINE__);
			}
		}
		else {
			throw new TelekomException('empty curl url in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Sets the curl options.
	 * @param Resource $curlSession Curl session resource
	 * @param array $curlOptions Curl Options
	 */
	protected function curlSetOptions($curlSession, $curlOptions){
		
		if (!empty($this->additionalOptions) && is_array($this->additionalOptions)) {
			foreach ($this->additionalOptions as $key => $val){
				$curlOptions[$key] = $val;
			}
		}
		curl_setopt_array($curlSession, $curlOptions);
	}
	
	/**
	 * Execute the request and get the response.
	 * @param Resource $curlSession Curl session resoure
	 * @return string
	 */
	protected function curlExecute($curlSession){
		
		$curlResponse = curl_exec($curlSession);
		$curlHttpInfo = curl_getinfo($curlSession, CURLINFO_HTTP_CODE);
		$errMessage = curl_error($curlSession);
		
		curl_close($curlSession);
		
		if ($curlResponse !== false) {
			return $curlResponse;
		}
		else {
			if ($curlHttpInfo == 401) {
				throw new TelekomException('curl 401 error: Unauthorized - Authentification failed in ' . __METHOD__. ' - line ' . __LINE__);
			} else {
				throw new TelekomException('curl response error in ' . __METHOD__. ' - line ' . __LINE__ . '; Message: ' . $errMessage);
			}
		}
	}

	/**
	 * Transforms the string to an array.
	 * @param string $string
	 * @return array
	 */
	public function transformResponseDataToArray($string){
		return json_decode($string, true);
	}

	/**
	 * Transforms an array to a string.
	 * @param  array $dataArray
	 * @return string
	 */
	public function transformArrayToResponseData($dataArray){

		$string = '';
		if (is_array($dataArray)) {
			$string = json_encode($dataArray);
		}
		return $string;
	}
}
