<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * The Telekom Data factory basic class.
 */
class TelekomDataFactory {
	
	/**
	 * Creates a status data object to get the requested status data.
	 * @param array $dataArray Data array
	 * @return StatusDataObject status data object
	 */
	public static function createStatus($dataArray) {
		return new StatusDataObject($dataArray);
	}
}
