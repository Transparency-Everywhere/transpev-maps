<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/voicecall/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to connect two clients for a call.
 */
class SimpleVoiceCallSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Voice call Client Object
	 * @var VoiceCallClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new VoiceCallClient($this->service);
//! [init]
	}
	
	/**
	 * This main function to connect two phone numbers for a call and prints the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleVoiceCall(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Voicecall PHP SDK - SimpleVoiceCallSample';
		
		// set the two numbers
		$callANumber = ''; // e.g. '+49 30 1234567'
		$callBNumber = ''; // e.g. '+49 160 2345678'
		
		try {
			
			// we need here the session to buffer the token and session call ID
			session_start();
			
			$action = (isset($_GET['action'])) ? $_GET['action'] : '';
			switch ($action){
				
				case 'startCall':
					// show the call page
					$voicecallContent = $this->renderCallPageContent($callANumber, $callBNumber);
					break;
				
				case 'callStatus':
					// show the call page
					$voicecallContent = $this->renderCallStatusContent($callANumber, $callBNumber);
					break;
				
				case 'tearDown':
					// tear down the call
					$voicecallContent = $this->renderTearDownPageContent($callANumber, $callBNumber);
					break;
				
				default:
					// show start page
					$voicecallContent = $this->renderStartPageContent($callANumber, $callBNumber);
			}
		}
		catch (TelekomException $e){
			$voicecallContent = $e->getMessage();
		}
		
		// output content
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $voicecallContent), $mainTemplate);
		echo $mainContent;
	}
	
	/**
	 * Render the start page content.
	 * @param string $callANumber
	 * @param string $callBNumber
	 * @return string $voicecallContent Rendered main content
	 */
	private function renderStartPageContent($callANumber, $callBNumber){
		
		// at first reset the session data
		$_SESSION['__devgarden__'] = array();
		
		$voicecallContent = file_get_contents('../tpl/voicecall/voicecall_start_template.html');
		$voicecallContent = str_replace(
			array('{NUMBER_A}', '{NUMBER_B}'),
			array($callANumber, $callBNumber), $voicecallContent);
		return $voicecallContent;
	}
	
	/**
	 * Render the call page content.
	 * @param string $callANumber
	 * @param string $callBNumber
	 * @return string $voicecallContent Rendered call page content
	 */
	private function renderCallPageContent($callANumber, $callBNumber){
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
			// The container for the send parameters
//! [create]
$sendParameters = new VoiceCallParameters();

// create a call
$sendParameters->setANumber($callANumber); // e.g. '+49 1234 567890'
$sendParameters->setBNumber($callBNumber); // e.g. '+40 160 12345678'

// start the call
$requestObj = $this->client->newCall($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		$sessionId = $requestObj->getSessionId();		
		break;
		
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
	break;
}
//! [create]
			
			// we need the access token and the Session ID, so we buffer it here
			$_SESSION['__devgarden__'] = array(
					'sessionId' 				=> $sessionId,
					'accessToken' 				=> $this->auth->getAccessToken(),
					'accessTokenValidUntil'		=> $this->auth->getAccessTokenValidUntil()
			);			
			
			// return the call content template
			$voicecallContent = file_get_contents('../tpl/voicecall/voicecall_call_template.html');
			$voicecallContent = str_replace(
					array('{SESSION_ID}'),
					array($sessionId), $voicecallContent);			
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $voicecallContent;
	}
	
	/**
	 * Render the call status page.
	 * @param string $callANumber
	 * @param string $callBNumber
	 * @return string $voicecallContent Rendered status page content
	 */
	private function renderCallStatusContent($callANumber, $callBNumber){
		
		$voicecallContent = '';
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$sessionId = $_SESSION['__devgarden__']['sessionId'];
			$accessToken = $this->auth->getAccessToken();
			
			// call status
//! [status]
$requestObj = $this->client->getCallStatus($sessionId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':					
		$stateA = $requestObj->getCallStatus()->getStateA();
		$stateB = $requestObj->getCallStatus()->getStateB();		
		$actualNumberB = $requestObj->getCallStatus()->getBe164();	
		break;
		
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
	break;
}
//! [status]
			
			// the last content template
			$voicecallContent = file_get_contents('../tpl/voicecall/voicecall_status_template.html');
			$voicecallContent = str_replace(
					array('{NUMBNER_A}', '{NUMBNER_B}', '{CALL_STATUS_A}', '{CALL_STATUS_B}', '{ACTUAL_NUMBER_B}'),
					array($callANumber, $callBNumber, $stateA, $stateB, $actualNumberB), $voicecallContent);			
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $voicecallContent;
	}
	
	/**
	 * Render the last page content.
	 * @param string $callANumber
	 * @param string $callBNumber
	 * @return string $voicecallContent Rendered last page content
	 */
	private function renderTearDownPageContent($callANumber, $callBNumber){
		
		$voicecallContent = '';
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$sessionId = $_SESSION['__devgarden__']['sessionId'];
			$accessToken = $this->auth->getAccessToken();
			
			// tear down the call
//! [teardown]
$requestObj = $this->client->tearDownCall($sessionId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':

		break;
		
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: ' .
			$requestObj->getStatus()->getStatusCode() . ', Message: ' .
			$requestObj->getStatus()->getStatusMessage());
	break;
}
//! [teardown]

			// the last content template
			$voicecallContent = file_get_contents('../tpl/voicecall/voicecall_last_template.html');
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		// reset the session data
		$_SESSION['__devgarden__'] = array();
		
		return $voicecallContent;
	}
	
	/**
	 * Sets the token data from buffered session.
	 */
	private function setTokenData(){
		if (isset($_SESSION['__devgarden__']['accessToken']) && isset($_SESSION['__devgarden__']['accessTokenValidUntil'])){
			$this->auth->setAccessToken($_SESSION['__devgarden__']['accessToken']);
			$this->auth->setAccessTokenValidUntil($_SESSION['__devgarden__']['accessTokenValidUntil']);
		}
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleVoiceCallSample();
$sample->simpleVoiceCall();

?>