<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/iplocation/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to locate IP addresses.
 */
class SimpleIpLocationSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * IP Location Client Object
	 * @var IpLocationClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new IpLocationClient($this->service);
//! [init]
	}
	
	/**
	 * This main function to locate IP addresses and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleIpLocation(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'IP Location PHP SDK - SimpleIpLocationSample';
		
		try {
			
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
				
				// The container for the send parameters
//! [locate]
$sendParameters = new IpLocationParameters();

// required settings
$sendParameters->setIpAddress('87.172.26.1'); // e.g. '87.172.26.1,91.61.31.1'

// locate IP
$requestObj = $this->client->locateIp($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':						
		$ipLocations = $requestObj->getIpLocations();						
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [locate]

				// print out the region results
				$mainIpLocationTemplate = file_get_contents('../tpl/iplocation/iplocation_main_template.html');
				$ipLocationTemplate = file_get_contents('../tpl/iplocation/iplocation_template.html');
				
				// get the data content
				$ipLocationContent = $this->renderIpLocations($ipLocations, $ipLocationTemplate);
				$mainLocationContent = str_replace('{LOCATION_DATA}', $ipLocationContent, $mainIpLocationTemplate);
				
				$mainContent = str_replace(
						array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
						array($topHead, $mainLocationContent), $mainTemplate);				
				
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}
	
	/**
	 * Render the ip locations data content part.
	 * @param array[LocationDataObject] $ipLocations IP locations
	 * @param string $templateCode HTML Template code
	 * @return string $content Rendered HTML content
	 */
	public function renderIpLocations($ipLocations, $templateCode){
//! [render]
$nr = 1;
$content = '';
if (is_array($ipLocations)){
	foreach ($ipLocations as $location){
		$content .= str_replace(
			array('{NR}',
				'{IP}',
				'{LOCATION}'),
			array($nr,
				$location->getIpAddress(),
				htmlspecialchars($location->getRegionName())),
			$templateCode);
		$nr++;
	}
}
//! [render]
		return $content;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleIpLocationSample();
$sample->simpleIpLocation();

?>