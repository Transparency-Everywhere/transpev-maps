<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}


/**
 * Example class to send sms.
 */
class SimpleOAuthSample {

	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;

	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;

	/**
	 * OAuth Object handles the authentification.
	 * @var TelekomOAuth
	 */
	private $auth = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){

		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data.
		 */
		require_once(dirname(__FILE__) . '/config.inc.php');

		// get and set the configuration
		$this->config = new TelekomConfig($telekomConfig);

		// init the service class
        $additionalProxyOptions = array(
			CURLOPT_SSL_VERIFYPEER => false
		);
		$this->service = new TelekomJSONService($this->config, $additionalProxyOptions);
		

		// init the Authentification class
		$this->auth = new TelekomOAuth($this->service);
	}

	/**
	 * This main function shows you the process to use OAuth.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleOAuth(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'SimpleOAuthSample';
		
		// we need to buffer the token data
		session_start();
		
		// we need a redirect URL to this script
		$redirectUrl = self::getRedirectUrl();
		
		$action = (isset($_GET['action'])) ? $_GET['action'] : '';
		if (empty($action)){
			
			// show start page
			$loginUrl = self::getRedirectUrl() . '?action=login'; 
			$mainContent = file_get_contents('../tpl/oauth/start_template.html');
			$mainContent = str_replace('{OAUTH_LOGIN_URL}', $loginUrl, $mainContent);
		} else {
			
			try {
				
				if ($action == 'refresh'){
					if (isset($_SESSION['__devgarden__']['refreshToken'])){
						$this->auth->requestRefreshToken($_SESSION['__devgarden__']['refreshToken']);
					}
					else {
						throw new TelekomException('Refresh token is not stored.');
					}
				}
				else if ($action == 'delete'){
					if (isset($_SESSION['__devgarden__']['refreshToken'])){
						$this->auth->requestRevoke($_SESSION['__devgarden__']['refreshToken'], $redirectUrl);
					}
					else {
						throw new TelekomException('Refresh token is not stored.');
					}
				}
				else {
					$this->auth->requestAccessToken();
				}
				
				// store data in session
				// so the has token can refresh automatically, if access token is not valid any more
				$_SESSION['__devgarden__'] = array(
					'accessToken' 				=> $this->auth->getAccessToken(),
					'accessTokenValidUntil' 	=> $this->auth->getAccessTokenValidUntil(),
					'refreshToken' 				=> $this->auth->getRefreshToken(),
				);
				
				if ($this->auth->hasValidToken()){
					$message = 'You are logged in and the access token is valid until ' . date('Y-m-d H:i:s', $this->auth->getAccessTokenValidUntil());
				}
				else {
					$message = 'Token received is not valid or no token received.';
				}
			}
			catch (TelekomException $e){
				$message = $e->getMessage();
			}
			
			$sendTemplate = file_get_contents('../tpl/oauth/success_template.html');
			$mainContent = str_replace('###MSG###', $message, $sendTemplate);
		}
		
		// output content
		$outputContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $mainContent), $mainTemplate);
		echo $outputContent;
	}

	/**
	 * Build the redirect URL for this script.
	 * @return string $redirectUrl
	 */
	public static function getRedirectUrl(){
		
		$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'On') ? 'https://' : 'http://';
		$redirectUrl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
		return $redirectUrl;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleOAuthSample();
$sample->simpleOAuth();

?>