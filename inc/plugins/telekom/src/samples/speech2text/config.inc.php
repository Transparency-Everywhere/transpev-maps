<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Configuration Array including all relevant configuration data.
 * @var array $telekomConfig configuration array
 */
$telekomConfig = array(

	// environment settings
	'environment' 						=> 'mock', // the preferred environment (budget / premium / sandbox / mock)

	// OAuth basic configuration - change it, if you use OAuth authentication!
	'oauth_client_id'					=> '',
	'oauth_client_secret'				=> '',
	'oauth_scope'						=> 'DC0QX4UK',
	
	// OAuth URLs and keys - do not change!
	'api_oauth_url'						=> 'https://global.telekom.com/gcp-web-api',
	'api_oauth_url_authorize'			=> '/oauth',
	'api_oauth_url_tokens'				=> '/oauth',
	'api_oauth_url_revoke'				=> '/oauth',
	'api_oauth_grant_type_auth'			=> 'client_credentials',
	'api_oauth_grant_type_refresh'		=> 'refresh_token',
	
	// required API URLs - do not change!
	'api_base_url' 						=> 'https://gateway.developer.telekom.com',
	'api_speech2text_base_url' 			=> '/plone/speech2text/rest/v1',
	'api_speech2text_discovery_url'		=> '/discovery',
	'api_speech2text_transcription_url'	=>	'/%s/transcriptions',
);
