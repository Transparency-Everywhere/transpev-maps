<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the SendSMS specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/speech2text/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to send SMS.
 */
class SimpleSpeech2TextSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Send SMS Client Object
	 * @var SendSmsClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'clientId' 		=> 'myClientId',
		 * 'clientSecret' 	=> 'myClientSecret',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$additionalProxyOptions = array(
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_SSLVERSION => 3,
//	CURLOPT_PROXY => '<yourproxy>',
	CURLOPT_PROXY => 'browser:3128',
);
$this->service = new TelekomJSONService($this->config, $additionalProxyOptions);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new Speech2TextClient($this->service);
//! [init]
	}
	
	/**
	 * This main function send a SMS and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleSpeech2Text(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Speech2Text PHP SDK - SimpleSpeech2TextSample';
		
		try {

			$this->auth->requestAccessToken();

			if ($this->auth->hasValidToken()) {		
				$accessToken = $this->auth->getAccessToken();
	
				// The container for the speech2text parameters
//! [api_call_discover]
// Make discovery call
$requestObj = $this->client->discovery($accessToken);
//! [api_call_discover]

				// echo "<pre>";
				// print_r($requestObj);
				// echo "</pre>";

				switch ($requestObj->getStatus()->getStatusCode()){
	
					// success
					case '0000':
						$message = '<b>speech2text discovery was successful.</b>';
						$message .= '<br /><br />Links: <br />';
						foreach ($requestObj->getLink() as $key => $value) {
							$message .= $value . '<br />';	
						}
						$message .= '<br /><br /><br />';
						break;
					
					// no permissions
					case '0093':
						throw new TelekomException('API Service is not activated. Status Code: '
							. $requestObj->getStatus()->getStatusCode() . ', Message: '
							. $requestObj->getStatus()->getStatusMessage());
						break;
					
					// all others
					default:
						throw new TelekomException('API Request failed. Status Code: ' . $requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage());
				}

//! [prepare_transcription]
$audioFileData = dirname(__FILE__) . '/tiptop-short.raw';

$sendParameters = new Speech2TextSendParameters();

$sendParameters->setAcceptTopic("Dictation");
$sendParameters->setAudioFileContentType("audio/x-wav;codec=pcm;bit=16;rate=16000");
$sendParameters->setFile($audioFileData);
$sendParameters->setLanguage("en_US");
//! [prepare_transcription]

//! [api_call_transcription]
$requestObj = $this->client->transcription($accessToken, $sendParameters);
//! [api_call_transcription]

				// echo "<pre>";
				// print_r($requestObj);
				// echo "</pre>";

				switch ($requestObj->getStatus()->getStatusCode()){
	
					// success
					case '0000':
						$message .= '<b>speech2text transcription was successful.</b>';
						$message .= '<br /><br />Text: <br />';
						$message .= $requestObj->getTranscription();
						break;
					
					// no permissions
					case '0093':
						throw new TelekomException('API Service is not activated. Status Code: '
							. $requestObj->getStatus()->getStatusCode() . ', Message: '
							. $requestObj->getStatus()->getStatusMessage());
						break;
					
					// all others
					default:
						throw new TelekomException('API Request failed. Status Code: ' . $requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage());
				}

				
				$mainContent = str_replace(
						array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
						array($topHead, $message), $mainTemplate);				
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleSpeech2TextSample();
$sample->simpleSpeech2Text();

?>