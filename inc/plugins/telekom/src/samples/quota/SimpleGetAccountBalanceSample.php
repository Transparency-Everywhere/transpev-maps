<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the quota specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/quota/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to get the account balance.
 */
class SimpleGetAccountBalanceSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Quota Client Object
	 * @var QuotaClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the basic configuration data!
		 * That's what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */		
		
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');
		
// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new QuotaClient($this->service);
//! [init]
	}
	
	/**
	 * This main function will get the account balance and print the results.
	 * The integrated view helper is to show you the result a little nicer.
	 * You can either modify it or implement your own view here.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleGetAccountBalance(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Quota PHP SDK - SimpleGetAccountBalanceSample';
		
		try {
			
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
				
//! [getaccountbalance]
// The container for the parameters
$sendParameters = new QuotaAccountBalanceParameters();

// set the DeveloperCenter Account ID
$accountId = '';
$sendParameters->setAccountId($accountId);

$requestObj = $this->client->getAccountBalance($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		// The response can actually contain information about multiple (sub-)accounts,
		// this sample show only the one idenitfied by $accountId
		$account = $requestObj->getAccountById($accountId);
		
		$credits = $account->getCredits();
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [getaccountbalance]

			}
			else {
				throw new TelekomException('Token is not valid!');
			}
						
			$quotaContent = file_get_contents('../tpl/quota/quota_account_template.html');
			$quotaContent = str_replace(
					array('{CREDITS}'),
					array($credits),
					$quotaContent);			
		}
		catch (TelekomException $e){
			$quotaContent = $e->getMessage();
		}
		
		// output content
		$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $quotaContent), $mainTemplate);
		echo $mainContent;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleGetAccountBalanceSample();
$sample->simpleGetAccountBalance();

?>