<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the quota specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/quota/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}


/**
 * Example class to set max. quota.
 */
class SimpleSetQuotaSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Quota Client Object
	 * @var QuotaClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
		require_once(dirname(__FILE__).'/config.inc.php');
		
		// get and set the configuration
		$this->config = new TelekomConfig($telekomConfig);

		// init the service class
		$this->service = new TelekomJSONService($this->config);
		
		// init the Authentification class
		$this->auth = new TelekomOAuth($this->service);

		// Init the Client class
		$this->client = new QuotaClient($this->service);
	}
	
	/**
	 * This main function will set the max. quota and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleSetQuota(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Quota PHP SDK - SimpleSetQuotaSample';
		
		try {
			
			$this->auth->requestAccessToken();
			
			if ($this->auth->hasValidToken()) {			
				$accessToken = $this->auth->getAccessToken();
				
				/* set quota Information
				    "SmsProduction": Send SMS, production environment
				    "SmsSandbox": Send SMS, sandbox environment
				    "MmsProduction": Send MMS, production environmen
				    "MmsSandbox": Send MMS, sandbox environment
				    "VoiceButlerProduction": Voice Call, production environmen
				    "VoiceButlerSandbox": Voice Call, sandbox environment
				    "CCSProduction": Conference Call, production environmen
				    "CCSSandbox": Conference Call, sandbox environment
				    "IPLocationProduction": IP Location, production environmen
				    "IPLocationSandbox": IP Location, sandbox environment
				 */
//! [setquota]
$moduleId = 'SmsProduction';
$maxUserQuota = 500;

$sendParameters = new QuotaSetParameters();
$sendParameters->setValue($maxUserQuota);

$requestObj = $this->client->setQuota($moduleId, $accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		// new value has been applied
		
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [setquota]
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
			
			$quotaContent = file_get_contents('../tpl/quota/quota_set_template.html');
		}
		catch (TelekomException $e){
			$quotaContent = $e->getMessage();
		}
		
		// output content
		$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $quotaContent), $mainTemplate);
		echo $mainContent;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleSetQuotaSample();
$sample->simpleSetQuota();

?>