<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/conferencecall/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to create and update conference announcements.
 */
class ConferenceAnnouncementSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Conference call Client Object
	 * @var ConferenceAnnouncementClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new ConferenceAnnouncementClient($this->service);
//! [init]
	}
	
	/**
	 * This main function is to create and update conference templates.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function simpleConferenceAnnouncement(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'ConferenceCall PHP SDK - ConferenceAnnouncementSample';
		
		// we need here the session to buffer the token and session template ID
		session_start();
		
		// we need a language for the announcement functions
		$lang = 'de';
		
		$message = '';
		
		$announcementList = '';
		
		try {
			
			$action = (isset($_GET['action'])) ? $_GET['action'] : '';
			switch ($action){
				case 'uploadAnnouncement':
					
					// show the create conference call page
					$message = $this->renderUploadAnnouncement($lang);
					break;
				
				case 'deleteAnnouncementSet':
					$announcementId = $_GET['announcementId'];
					$message = $this->renderDeleteAnnouncementSet($lang, $announcementId);
					break;
			}
			
			// on start get announcements
			$announcementList = $this->renderGetAnnouncementSets($lang);
			
		}
		catch (TelekomException $e){
			$message = $e->getMessage();
		}
		
		// render main content
		$announcementTemplate = file_get_contents('../tpl/conferencecall/conference_announcement_all_template.html');
		$templateContent = str_replace(
			array('{MESSAGE}', '{ANNOUNCEMENT_LIST}'),
			array($message, $announcementList), $announcementTemplate);
		
		// output page content
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $templateContent), $mainTemplate);
		echo $mainContent;
	}
	
	/**
	 * render the announcement sets
	 * @return string
	 */
	private function renderGetAnnouncementSets($lang){
		
		$this->auth->requestAccessToken();
		if ($this->auth->hasValidToken()){
			
			$accessToken = $this->auth->getAccessToken();
			
//! [list]
$sendParameters = new AnnouncementSetParameters();

$requestObj = $this->client->getAnnouncementSets($accessToken, $lang, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$announcementRow = file_get_contents('../tpl/conferencecall/conference_announcement_row.html');
		$msg = '';
		$sets = $requestObj->getAnnouncementSets();
		if (is_array($sets)){
			foreach ($sets as $id){				
				$msg .= str_replace('{A_ID}', $id, $announcementRow);
			}
		}
		else {
			$msg = 'No announcement sets found.';
		}
		return $msg;
		
		break;
		
		// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [list]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}
	
	/**
	 * render the announcement upload
	 * @return string
	 */
	private function renderUploadAnnouncement($lang){
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()){			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
//! [createset]
$sendParameters = new UploadAnnouncementParameters();

// required settings
$sendParameters->setType('welcome_dialout_confirm'); // type of announcement
$sendParameters->setAudioFile(dirname(__FILE__) . '/num_1.wav'); // audio file path to .WAV file

$requestObj = $this->client->uploadAnnouncement($accessToken, $sendParameters, $lang);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		return 'Upload successful.';
		break;
		
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [createset]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}
	
	/**
	 * render delete announcement
	 * @param string $announcementId announcement ID
	 * @return string
	 */
	private function renderDeleteAnnouncementSet($lang, $announcementId){
		
		$this->auth->requestAccessToken();
		if ($this->auth->hasValidToken()){
			
			$accessToken = $this->auth->getAccessToken();			
			
//! [delete]
$sendParameters = new AnnouncementSetParameters();

$requestObj = $this->client->deleteAnnouncementSet($accessToken, $lang,
		$announcementId, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		return 'Announcement ' . $announcementId . ' successfully deleted.';
		break;
		
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [delete]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}
}

/**
 * Start the sample.
 */
$sample = new ConferenceAnnouncementSample();
$sample->simpleConferenceAnnouncement();

?>