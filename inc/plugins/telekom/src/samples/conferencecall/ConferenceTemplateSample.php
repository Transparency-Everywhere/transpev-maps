<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/conferencecall/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to create and update conference templates.
 */
class ConferenceTemplateSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Conference call Client Object
	 * @var ConferenceTemplateClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new ConferenceTemplateClient($this->service);
//! [init]
	}
	
	/**
	 * This main function is to create and update conference templates.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function simpleConferenceTemplate(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'ConferenceCall PHP SDK - ConferenceTemplateSample';
		$message = '';
		$statusContent = '';
		
//! [createData]
$conferenceData = array(
	'ownerId' 		=> 'PG-12345',
	'name'			=> 'Peters Testconference',
	'description' 	=> 'Peters conference test via Telekom PHP SDK',
	'firstName'		=> 'Peter',
	'lastName'		=> 'Test',
	'email'			=> 'peter@test.com',
	'number'		=> '0049691234567',
	'duration' 		=> 60,
	'joinConfirm'	=> 'false',
);
//! [createData]
//! [updateData]
$conferenceUpdateData = array(
	'name'			=> 'Toms Testconference',
	'description' 	=> 'Toms conference test via Telekom PHP SDK',
	'duration' 		=> 120,
);
//! [updateData]
//! [addData]
$participantAddData = array(
	'number'		=> '0049401122334',
	'isInitiator'	=> 'false',
);
//! [addData]
//! [updateParticipantData]
$participantUpdateData = array(
	'firstName'		=> 'Tom',
	'lastName'		=> 'Tester',
	'number'		=> '0049697654321',
	'email'			=> 'peter@test.com',
	'joinConfirm'	=> 'false',
	'isInitiator'	=> 'false',
	'dialOut'		=> 'true'
);
//! [updateParticipantData]
		
		try {
			
			// we need here the session to buffer the token and session template ID
			session_start();
			
			$lastParticipantId = (isset($_SESSION['__devgarden__']['lastParticipantId'])) ? $_SESSION['__devgarden__']['lastParticipantId'] : '';
			
			$action = (isset($_GET['action'])) ? $_GET['action'] : '';
			switch ($action){
				case 'createTemplate':
					
					// on create reset session data
					$_SESSION['__devgarden__'] = array();
					
					$message = $this->createConferenceTemplate($conferenceData);
					break;
				
				case 'getTemplate':
					$message = $this->getConferenceTemplate();
					break;
				
				case 'updateTemplate':
					$message = $this->getConferenceUpdateTemplate($conferenceUpdateData);
					break;
				
				case 'addParticipant':
					$message = $this->getConferenceAddTemplateParticipant($participantAddData);
					break;
				
				case 'getParticipant':
					$message = $this->getConferenceTemplateParticipant($lastParticipantId);
					break;
				
				case 'updateParticipant':
					$message = $this->getConferenceUpdateTemplateParticipant($lastParticipantId, $participantUpdateData);
					break;
				
				case 'deleteParticipant':
					$message = $this->getConferenceRemoveTemplateParticipant($lastParticipantId);
					break;
				
				case 'getTemplateList':
					$message = $this->getConferenceTemplateList($conferenceData['ownerId']);
					break;
				
				case 'deleteTemplate':
					$message = $this->getRemoveConferenceTemplate();
					break;
				
				default:
					// on start do nothing
			}
		}
		catch (TelekomException $e){
			$message = $e->getMessage();
		}
		
		// building content
		$templateContent = file_get_contents('../tpl/conferencecall/conference_template_all_template.html');
		$templateId = (!empty($_SESSION['__devgarden__']['templateId'])) ? $_SESSION['__devgarden__']['templateId'] : 'no active Template ID found.';
		$templateContent = $templateContent = str_replace(
			array(
				'{TEMPLATE_ID}',
				'{MESSAGE}',
				'{STATUS_CONTENT}',
			),
			array(
				$templateId,
				$message,
				$statusContent,
			),
			$templateContent);
		
		// output content
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $templateContent), $mainTemplate);
		echo $mainContent;
	}
	
	/**
	 * Render the create template page content.
	 * @param array $confData
	 * @return string $templateContent Rendered call page content
	 */
	private function createConferenceTemplate($conferenceData){
		
		$message = '';
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
//! [create]
// The container for the send parameters
$sendParameters = new ConferenceTemplateCreateParameters();

// configure call
$sendParameters->setOwnerId($conferenceData['ownerId']);
$sendParameters->setName($conferenceData['name']);
$sendParameters->setDescription($conferenceData['description']);
// configure initiator
$sendParameters->setFirstName($conferenceData['firstName']);
$sendParameters->setLastName($conferenceData['lastName']);
$sendParameters->setEmail($conferenceData['email']);
$sendParameters->setNumber($conferenceData['number']);

// start the call
$requestObj = $this->client->createConferenceTemplate($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':			
		$message = 'The conference template was created successfully.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [create]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		// we need the access token and the Session ID, so we buffer it here
		$_SESSION['__devgarden__'] = array(
				'templateId' 				=> $requestObj->getTemplateId(),
				'accessToken' 				=> $this->auth->getAccessToken(),
				'accessTokenValidUntil'		=> $this->auth->getAccessTokenValidUntil()
		);		
		
		return $message;
	}
	
	/**
	 * Render the get template list page content.
	 * @param string $templateId template ID
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceTemplate(){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
			// start the call
//! [get]
$requestObj = $this->client->getConferenceTemplate($templateId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message .= $requestObj->getTemplateDetails()->getName() . '<br />';
		$message .= $requestObj->getTemplateDetails()->getDescription() . '<br />';
		$message .= $requestObj->getTemplateDetails()->getDuration() . '<br />';
		foreach ($requestObj->getTemplateParticipants() as $participant){
			$message .= 'Participant ID: ' . $participant->getParticipantId() . '<br />';
			// set the last participant into the session data
			$_SESSION['__devgarden__']['lastParticipantId'] = $participant->getParticipantId();
		}
		return $message;
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [get]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the get template update page content.
	 * @param array $conferenceUpdateData update data
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceUpdateTemplate($conferenceUpdateData){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
//! [update]
// The container for the update parameters
$sendParameters = new ConferenceTemplateUpdateParameters();

// create a call
$sendParameters->setName($conferenceUpdateData['name']);
$sendParameters->setDescription($conferenceUpdateData['description']);
$sendParameters->setDuration($conferenceUpdateData['duration']);

// start the call
$requestObj = $this->client->updateConferenceTemplate($templateId,
		$accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':					
		$message = 'Template successfully updated.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [update]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the get template list page content.
	 * @param string $ownerId owner ID
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceTemplateList($ownerId){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$accessToken = $this->auth->getAccessToken();
			
			// start the call
//! [list]
$requestObj = $this->client->getConferenceTemplateList($ownerId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$templateIds = $requestObj->getTemplateIds();
		return count($templateIds) . ' active template(s) found.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [list]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the delete conference template page content.
	 * @return string $templateContent Rendered last page content
	 */
	private function getRemoveConferenceTemplate(){
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
			// delete a conference template
//! [remove]
$requestObj = $this->client->removeConferenceTemplate($templateId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		$message = 'Template was deleted successfully.';
		break;
		
		// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [remove]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		// reset the session data
		$_SESSION['__devgarden__'] = array();
		
		return $message;
	}
	
	/**
	 * Render the add template participant page content.
	 * @param array $participantAddData participant data
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceAddTemplateParticipant($participantAddData){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
			// The container for the create parameters
//! [add]
$sendParameters = new ConferenceTemplateAddParticipantParameters();

// create a call
$sendParameters->setNumber($participantAddData['number']);
$sendParameters->setIsInitiator($participantAddData['isInitiator']);

// start the call
$requestObj = $this->client->addConferenceTemplateParticipant($templateId,
		$accessToken , $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message = 'Participant added successfully with ID '.$requestObj->getParticipantId().".";
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [add]

			$_SESSION['__devgarden__']['lastParticipantId'] = $requestObj->getParticipantId();
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the update template participant page content.
	 * @param string $participantId participant ID
	 * @param array $participantUpdateData participant data
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceUpdateTemplateParticipant($participantId, $participantUpdateData){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
			// The container for the create parameters
//! [updateParticipant]
$sendParameters = new ConferenceTemplateUpdateParticipantParameters();

// all properties have to be provided, even if they did not change
$sendParameters->setFirstName($participantUpdateData['firstName']);
$sendParameters->setLastName($participantUpdateData['lastName']);
$sendParameters->setNumber($participantUpdateData['number']);
$sendParameters->setEmail($participantUpdateData['email']);
$sendParameters->setIsInitiator($participantUpdateData['isInitiator']);
$sendParameters->setDialOut($participantUpdateData['dialOut']);

// start the call
$requestObj = $this->client->updateConferenceTemplateParticipant($templateId,
		$participantId, $accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message = 'Participant updated successfully.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [updateParticipant]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the get template participant page content.
	 * @param string $participantId participant ID
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceTemplateParticipant($participantId){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
			// start the call
//! [getParticipant]
$requestObj = $this->client->getConferenceTemplateParticipant($templateId, $participantId,
		$accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message = 'Participant number: ' . $requestObj->getParticipant()->getNumber();
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: ' 
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [getParticipant]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Render the delete template participant page content.
	 * @param string $participantId participant ID
	 * @return string $templateContent Rendered call page content
	 */
	private function getConferenceRemoveTemplateParticipant($participantId){
		
		$message = '';
		
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$templateId = $_SESSION['__devgarden__']['templateId'];
			$accessToken = $this->auth->getAccessToken();
			
//! [removeParticipant]
$requestObj = $this->client->removeConferenceTemplateParticipant($templateId, $participantId,
		$accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message = 'Participant successfully deleted.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [removeParticipant]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $message;
	}
	
	/**
	 * Sets the token data from buffered session.
	 */
	private function setTokenData(){
		if (isset($_SESSION['__devgarden__']['accessToken']) && isset($_SESSION['__devgarden__']['accessTokenValidUntil'])){
			$this->auth->setAccessToken($_SESSION['__devgarden__']['accessToken']);
			$this->auth->setAccessTokenValidUntil($_SESSION['__devgarden__']['accessTokenValidUntil']);
		}
	}
}

/**
 * Start the sample.
 */
$sample = new ConferenceTemplateSample();
$sample->simpleConferenceTemplate();

?>