<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/conferencecall/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]


/**
 * Example class to connect three clients for a conference call.
 */
class ConferenceCallSample {

	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;

	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;

	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Conference call Client Object
	 * @var ConferenceCallClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){

		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 *
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new ConferenceCallClient($this->service);
//! [init]
	}

	/**
	 * This main function to connect three clients for a conference call.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function simpleConferenceCall(){

		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'ConferenceCall PHP SDK - ConferenceCallSample';
		$message = '';
		$statusContent = '';

		// set the basic conference data
//! [create_param]
$conferenceConfig = array(
	'ownerId' 		=> 'PG-12345',
	'name'			=> 'Peters Testconference',
	'description' 	=> 'Peters conference test via PHP SDK',
	'duration' 		=> 60,
	'joinConfirm'	=> 'false',
);
//! [create_param]

		// set the conference numbers
//! [add_param]
$conferenceParticipants = array(
	0 => array(
		'number' 		=> '', // e.g. '+49 30 1234567'
		'firstName' 	=> 'Peter',
		'lastName' 		=> 'Tester',
		'email' 		=> 'p@example.com',
		'isInitiator'	=> 'true',
	),
//! [add_param]
	1 => array(
		'number' 		=> '', // e.g. '+49 30 1234567'
		'firstName' 	=> 'Peter',
		'lastName' 		=> 'Test',
		'email' 		=> 'test@example.com',
		'isInitiator'	=> 'false',
	),
	2 => array(
		'number' 		=> '', // e.g. '+49 30 1234567'
		'firstName' 	=> 'Thomas',
		'lastName' 		=> 'Test',
		'email' 		=> 'thomas@example.com',
		'isInitiator'	=> 'false',
	),
);

$updateArr = array(
	'number' 		=> '', // e.g. '+49 30 1234567'
	'firstName' 	=> 'Thomas',
	'lastName' 		=> 'Test',
	'email' 		=> 'test@example.com',
	'isInitiator'	=> 'false',
	'dialOut'		=> 'true'
);

		try {

			// we need here the session to buffer the token and session call ID
			session_start();

			$action = (isset($_GET['action'])) ? $_GET['action'] : '';
			switch ($action){
				case 'createConference':

					// reset session data
					$_SESSION['__devgarden__'] = array();

					// show the create conference call page
					$message = $this->createConferenceCall($conferenceConfig);
					break;

				case 'addParticipant':
					// show the add participant page
					$actualNo = (isset($_GET['nr'])) ? (int)$_GET['nr'] : 0;
					$message = $this->addParticipant($conferenceParticipants, $actualNo);
					break;

				case 'commitConference':
					// commit the actual conference
					$message = $this->commitConference();
					break;

				case 'updateConference':
					// commit the actual conference
					$message = $this->updateConference($conferenceConfig);
					break;

				case 'getRunningConference':
					// commit the actual conference
					$message = $this->getRunningConference();
					break;

				case 'getConferenceList':
					// commit the actual conference
					$message = $this->getConferenceList($conferenceConfig['ownerId']);
					break;

				case 'tearDown':
					// tear down the conference call
					$message = $this->removeConference();
					break;

				default:
					// on start do nothing
			}

			// do we have a participant action?
			if (!empty($_SESSION['__devgarden__']['conferenceId'])){
				$action2 = (isset($_GET['action2'])) ? $_GET['action2'] : '';
				switch ($action2){
					case 'getParticipantStatus':
						$message = $this->renderGetParticipantStatus($_GET['participantId']);
						break;

					case 'updateParticipant':
						$message = $this->renderUpdateParticipant($_GET['participantId'], $updateArr);
						break;

					case 'removeParticipant':
						$message = $this->renderRemoveParticipant($_GET['participantId']);
						break;
				}
			}
		}
		catch (TelekomException $e){
			$message = $e->getMessage();
		}

		// always get the actual conference status
		$statusContent = $this->renderGetConferenceStatus();

		// building content
		$conferenceContent = file_get_contents('../tpl/conferencecall/conferencecall_all_template.html');
		$conferenceId = (!empty($_SESSION['__devgarden__']['conferenceId'])) ? $_SESSION['__devgarden__']['conferenceId'] : 'no active Session ID found.';
		$conferenceContent = $conferenceContent = str_replace(
			array(
				'{CONFERENCE_ID}',
				'{MESSAGE}',
				'{NUMBER1}',
				'{NUMBER2}',
				'{NUMBER3}',
				'{STATUS_CONTENT}',
			),
			array(
				$conferenceId,
				$message,
				$conferenceParticipants[0]['number'],
				$conferenceParticipants[1]['number'],
				$conferenceParticipants[2]['number'],
				$statusContent,
			),
			$conferenceContent);

		// output content
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $conferenceContent), $mainTemplate);
		echo $mainContent;
	}

	/**
	 * Render the create call page content.
	 * @param array $confData Conference data
	 * @return string $message Message
	 */
	private function createConferenceCall($conferenceConfig){

		$message = '';

//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()){			
	$accessToken = $this->auth->getAccessToken();
//! [token]

//! [create]
// The container for the send parameters
$sendParameters = new ConferenceCallCreateParameters();

// create a call
$sendParameters->setOwnerId($conferenceConfig['ownerId']);
$sendParameters->setName($conferenceConfig['name']);
$sendParameters->setDescription($conferenceConfig['description']);
$sendParameters->setDuration($conferenceConfig['duration']);
$sendParameters->setJoinConfirm($conferenceConfig['joinConfirm']);

// set the timestamp, if you would like to start the conference in future 
//$timestamp = time() + 120;
//$sendParameters->setTimestamp($timestamp); // optional, unix timestamp

// start the call
$requestObj = $this->client->createConference($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		$message = 'Conference call was created successfully.';
		break;

	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode() .
			', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [create]

			// we need the access token and the Session ID, so we buffer it here
			$_SESSION['__devgarden__'] = array(
					'conferenceId' 				=> $requestObj->getConferenceId(),
					'accessToken' 				=> $this->auth->getAccessToken(),
					'accessTokenValidUntil'		=> $this->auth->getAccessTokenValidUntil()
			);

		}
		else {
			throw new TelekomException('Token is not valid!');
		}

		return $message;
	}

	/**
	 * Render the add participant page content.
	 * @param array $conferenceParticipants conference participants data array
	 * @param integer $actualNo actual conference number key
	 * @return string $conferenceContent Rendered call page content
	 */
	private function addParticipant($conferenceParticipants, $actualNo){

		if (count($conferenceParticipants) < 2){
			throw new TelekomException('We need two or more numbers for a conference call!');
		}

		$conferenceContent = '';

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			if (!empty($conferenceParticipants[$actualNo])){

				$accessToken = $this->auth->getAccessToken();

				$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
//! [add]
// The container for the send parameters
$sendParameters = new ConferenceCallAddParticipantParameters();

// set the required fields
$sendParameters->setNumber($conferenceParticipants[$actualNo]['number']);
$sendParameters->setFirstName($conferenceParticipants[$actualNo]['firstName']);
$sendParameters->setLastName($conferenceParticipants[$actualNo]['lastName']);
$sendParameters->setEmail($conferenceParticipants[$actualNo]['email']);
$sendParameters->setIsInitiator($conferenceParticipants[$actualNo]['isInitiator']);

// now add participant
$requestObj = $this->client->addParticipant($conferenceId, $accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':

		$conferenceContent = 'Participant Nr. '.($actualNo+1).' was added successfully.';
		$conferenceContent .= ' Participant ID is '.$requestObj->getParticipantId().".";
		break;

		// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [add]
			}
			else {
				$conferenceContent = 'Phone Number No. ' . $actualNo . ' is not set!';
			}
		}
		else {
			throw new TelekomException('Token is not valid!');
		}

		return $conferenceContent;
	}

	/**
	 * Render the commit conference content.
	 * @return string Rendered status page content
	 */
	private function commitConference(){

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

			// commit the conference (in this case, start it)
//! [commit]
$requestObj = $this->client->commitConference($conferenceId, $accessToken);

switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		return 'Conference was started successfully.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [commit]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}

	/**
	 * Render the get running conference content.
	 * @return string Content part
	 */
	private function getRunningConference(){

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

//! [getrunning]
$requestObj = $this->client->getRunningConference($conferenceId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':

		return 'Conference is running.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [getrunning]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}

	/**
	 * Render the get conference list content.
	 * @param string $ownerId Owner ID
	 * @return string Content part
	 */
	private function getConferenceList($ownerId){

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$accessToken = $this->auth->getAccessToken();

			// get a list of conferences
//! [getlist]
$requestObj = $this->client->getConferenceList($ownerId, $accessToken, 0);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':

		$conferences = $requestObj->getConferences();
		return count($conferences) . ' active conference(s) found.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [getlist]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}

	/**
	 * Render the update conference content.
	 * @param array $confData Conference data
	 * @return string $conferenceContent Rendered content
	 */
	private function updateConference($confData){

		$conferenceContent = '';

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

//! [update]
// The container for the send parameters
$sendParameters = new ConferenceCallUpdateParameters();

// set updated parameters
$sendParameters->setName('Changedconferencename');
$sendParameters->setOwnerId($confData['ownerId']);
$sendParameters->setDescription('ChangedDescription');
$sendParameters->setDuration($confData['duration']);
// Please note: All omitted fields are replaced by default values

// update the conference
$requestObj = $this->client->updateConference($conferenceId,
		$accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()) {

	// success
	case '0000':
		$conferenceContent = 'Conference was updated successfully.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [update]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}

		return $conferenceContent;
	}

	/**
	 * Render the remove participant content.
	 * @param string $participantId Participant ID
	 * @return string content part
	 */
	private function renderRemoveParticipant($participantId){

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

//! [remove]
$requestObj = $this->client->removeParticipant($conferenceId, $participantId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		return 'Participant was removed successfully.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [remove]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}

	/**
	 * Render the get participant status content.
	 * @param string $participantId Participant ID
	 * @return string content part
	 */
	private function renderGetParticipantStatus($participantId){

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

//! [participantStatus]
$requestObj = $this->client->getParticipantStatus($conferenceId, $participantId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		return 'Last access time of user ' . $requestObj->getParticipantStatus()->getNumber() . ': '
			. date('d.m.Y H:i:s', $requestObj->getParticipantStatus()->getLastAccessTime());
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [participantStatus]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}

	/**
	 * Render the update participant page content.
	 * @param string $participantId Participant ID
	 * @param array $updateArr Array including update data
	 * @return string $conferenceContent Rendered content part
	 */
	private function renderUpdateParticipant($participantId, $updateArr){

		$conferenceContent = '';

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

//! [updateParticipant]
// The container for the send parameters
$sendParameters = new ConferenceCallParticipantParameters();

// Even if only one property should be updated...
$sendParameters->setFirstName($updateArr['firstName']);

// .. all properties have to be provided
$sendParameters->setLastName($updateArr['lastName']);
$sendParameters->setNumber($updateArr['number']);
$sendParameters->setEmail($updateArr['email']);
$sendParameters->setIsInitiator($updateAttr['isInitiator']);
$sendParameters->setDialOut($updateAttr['dialOut']);

$requestObj = $this->client->updateParticipant($conferenceId, $participantId,
		$accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		$conferenceContent = 'Participant was updated successfully.';
		break;

	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [updateParticipant]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}

		return $conferenceContent;
	}

	/**
	 * Render the conference status page content.
	 * @return string $conferenceContent Rendered status page content
	 */
	private function renderGetConferenceStatus(){

		$conferenceContent = '';

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			if (!empty($_SESSION['__devgarden__']['conferenceId'])){

				$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
				$accessToken = $this->auth->getAccessToken();

//! [status]
$requestObj = $this->client->getConferenceStatus($conferenceId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':

		$participants = $requestObj->getParticipants();
		$participantsContent = $this->renderParticipantsContent($participants);
		$conferenceContent = file_get_contents('../tpl/conferencecall/conferencecall_status_template.html');
		$conferenceContent = str_replace(
			array('{DESCRIPTION}', '{DURATION}', '{NAME}', '{PARTICIPANTS}'),
			array($requestObj->getDetail()->getDescription(), $requestObj->getDetail()->getDuration(), $requestObj->getDetail()->getName(), $participantsContent), $conferenceContent);
		break;

	// all others
	default:
		// if you like to throw an exception
		// you can do this here
		$conferenceContent = 'Get conference status error: ' . $requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage();
}
//! [status]
			}
		}

		return $conferenceContent;
	}

	/**
	 * Render participants content.
	 * @param array[ParticipantDataObject] $participantsArray participants
	 * @return string $conferenceContent Rendered content part
	 */
	private function renderParticipantsContent($participantsArray){

		$partTemplate = file_get_contents('../tpl/conferencecall/conferencecall_status_participant_template.html');
		$participantContent = '';
		if (!empty($participantsArray) && is_array($participantsArray)){
			foreach ($participantsArray as $part){
				$participantContent .= str_replace(
					array(
						'{PARTICIPANT_ID}',
						'{NUMBER}',
						'{FIRST_NAME}',
						'{LAST_NAME}',
						'{STATUS}',
					),
					array(
						$part->getParticipantID(),
						$part->getNumber(),
						$part->getFirstName(),
						$part->getLastName(),
						$part->getParticipantStatus()->getStatus()
					),
					$partTemplate
				);
			}
		}
		else {
			$participantContent = 'no participants found.';
		}
		return $participantContent;
	}

	/**
	 * Render the tear down content.
	 * @return string $message Message
	 */
	private function removeConference(){

		$message = '';

		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$conferenceId = $_SESSION['__devgarden__']['conferenceId'];
			$accessToken = $this->auth->getAccessToken();

			// tear down the conference call
//! [teardown]
$requestObj = $this->client->removeConference($conferenceId, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){

	// success
	case '0000':
		$message = 'Conference was stopped successfully.';
		break;

		// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [teardown]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}

		// reset the session data
		$_SESSION['__devgarden__'] = array();

		return $message;
	}

	/**
	 * Sets the token data from buffered session.
	 */
	private function setTokenData(){
		if (isset($_SESSION['__devgarden__']['accessToken']) && isset($_SESSION['__devgarden__']['accessTokenValidUntil'])){
			$this->auth->setAccessToken($_SESSION['__devgarden__']['accessToken']);
			$this->auth->setAccessTokenValidUntil($_SESSION['__devgarden__']['accessTokenValidUntil']);
		}
	}
}

/**
 * Start the sample.
 */
$sample = new ConferenceCallSample();
$sample->simpleConferenceCall();

?>