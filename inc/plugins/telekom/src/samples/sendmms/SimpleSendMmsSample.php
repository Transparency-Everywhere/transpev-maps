<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the SendMms specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/sendmms/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to send MMS.
 */
class SimpleSendMmsSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Send Mms Client Object
	 * @var SendMmsClient
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new SendMmsClient($this->service);
//! [init]
	}
	
	/**
	 * This main function send a MMS and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleSendMms(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Send MMS PHP SDK - SimpleSendMmsSample';
		
		try {
			
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
				
				// The container for the send parameters
//! [send]
$sendParameters = new SendMmsParameters();

// required settings
$sendParameters->setNumber('+49yourNumberHere'); // e.g. '0049160123456789'
$sendParameters->setSubject('Developer Garden Logo');
$sendParameters->setMessage('Here is the Developer Garden logo.');
// attachment path to picture or bease64 encoded string. The max. file size is 240 KB!
// the @ sign tells the SDK to load the data from the following filename
$sendParameters->setAttachment('@' . dirname(__FILE__) . '/../tpl/logo.jpg');
// filename as shown on mobile device
$sendParameters->setFilename('logo.jpg');
// attachment's content type
$sendParameters->setContentType('image/jpeg');

// Send Mms
$requestObj = $this->client->sendMms($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()) {
	
	// success
	case '0000':
		$message = 'MMS was send successfully.';
		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: ' . $requestObj->getStatus()->getStatusCode() . ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [send]

				$mainContent = str_replace(
						array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
						array($topHead, $message), $mainTemplate);
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleSendMmsSample();
$sample->simpleSendMms();

?>