<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/smsvalidation/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to validate numbers via SMS.
 */
class SimpleValidateSmsSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;
	
	/**
	 * Send Validation Keyword Client Object
	 * @var SmsValidationClient
	 */
	private $client = null;
	
	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new SmsValidationClient($this->service);
//! [init]
	}
	
	/**
	 * This main function validates a phone number with a keyword and prints the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleValidateSms(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'SMS Validation PHP SDK - SimpleValidateSmsSample';
		
		// which mobile phone number to validate?
		$phoneNumber = ''; // e.g. '+49 160 1234567'
		
		try {
			
			// we need here the session to buffer the token and session call ID
			session_start();
			
			$action = (isset($_GET['action'])) ? $_GET['action'] : '';
			switch ($action){
				
				case 'getKeyword':
					// show the send keyword page
					$validateContent = $this->renderSendNumberForValidation($phoneNumber);
					break;
				
				case 'validate':
					// show the validate page
					$validateContent = $this->renderValidateNumberWithKeyword($phoneNumber,  $_GET['confirm_key']);
					break;
				
				case 'getNumbers':
					// show the get validated numbers page
					$validateContent = $this->renderGetValidatedNumbers();
					break;
				
				case 'invalidate':
					// stop the validation
					$validateContent = $this->renderInvalidate($phoneNumber);
					break;
				
				default:
					// show start page
					$validateContent = $this->renderStartPageContent($phoneNumber);
			}
		}
		catch (TelekomException $e){
			$validateContent = $e->getMessage();
		}
		
		// output content
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $validateContent), $mainTemplate);
		echo $mainContent;
	}
	
	/**
	 * Render the start page.
	 * @param string $phoneNumber phone number
	 * @return string $validateContent Complete output HTML data
	 */
	public function renderStartPageContent($phoneNumber){
		
		// at first reset the session data
		$_SESSION['__devgarden__'] = array();
		
		$validateContent = file_get_contents('../tpl/smsvalidation/smsvalidation_start_template.html');
		$validateContent = str_replace(
			array('{NUMBER}'),
			array($phoneNumber), $validateContent);
		
		return $validateContent;
	}
	
	/**
	 * Render the page where a SMS will be send to get a validation key.
	 * @param string $phoneNumber phone number
	 * @return string $validateContent Complete output HTML data
	 */
	public function renderSendNumberForValidation($phoneNumber){
		
		$validateContent = '';
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
			// we need the access token and the Session ID, so we buffer it here
			$_SESSION['__devgarden__'] = array(
				'accessToken' 				=> $this->auth->getAccessToken(),
				'accessTokenValidUntil'		=> $this->auth->getAccessTokenValidUntil()
			);
			
			// The container for the send parameters
//! [send]
$sendParameters = new SendNumberForValidationParameters();

// required settings
$sendParameters->setNumber($phoneNumber); // e.g. '+49 160 12345678'
$sendParameters->setOriginator('Application');

// Send validation keyword
$requestObj = $this->client->sendNumberForValidation($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		break;
				
	// all others
	default:
		throw new TelekomException($requestObj->getStatus()->getStatusCode()
			. ', Message: ' . $requestObj->getStatus()->getStatusMessage());
}
//! [send]

			$validateContent = file_get_contents('../tpl/smsvalidation/smsvalidation_sendnumber_template.html');
			$validateContent = str_replace(
					array('{NUMBER}'),
					array($phoneNumber), $validateContent);

		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $validateContent;
	}
	
	/**
	 * Render the page where a SMS will be validated with requested the key.
	 * @param string $phoneNumber phone number
	 * @param string $confirmKey confirmation key
	 * @return string $validateContent Complete output HTML data
	 */
	public function renderValidateNumberWithKeyword($phoneNumber, $confirmKey){
		
		// we need a valid alphanumeric key with a string length of 6
		if (!preg_match("/^[a-zA-Z0-9]+$/", $confirmKey)){
			throw new TelekomException('Your confirm key is not valid!');
		}
		
		$validateContent = '';
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$accessToken = $this->auth->getAccessToken();
			$validateContent = file_get_contents('../tpl/smsvalidation/smsvalidation_validate_template.html');				
			
			// The container for the send parameters
//! [validate]
$sendParameters = new ValidateNumberWithKeywordParameters();

// required settings
$sendParameters->setNumber($phoneNumber);
$sendParameters->setKey($confirmKey);

// Send validation with keyword
$requestObj = $this->client->validateNumberWithKeyword($accessToken, $sendParameters);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$message = 'Your phone number ' . $phoneNumber . ' is now validated!';
		break;
	
	// all others
	default:
		$message = $requestObj->getStatus()->getStatusCode() . ', Message: ' .
			$requestObj->getStatus()->getStatusMessage();
}
//! [validate]
			
			$validateContent = str_replace(
				array('{MESSAGE}'),
				array($message), $validateContent);
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		// output content
		return $validateContent;
	}
	
	/**
	 * Render the get validated numbers page.
	 * @return string $validateContent Complete output HTML data
	 */
	public function renderGetValidatedNumbers(){
		
		$validateContent = '';
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){
			
			$accessToken = $this->auth->getAccessToken();
			
//! [list]
$requestObj = $this->client->getValidatedNumbers($accessToken);
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		$validatedNumbersContent = '';
		$validatedNumbers = $requestObj->getNumbers();
		foreach ($validatedNumbers as $number){
			$validatedNumbersContent .= $number->getNumber() . '<br />';
		}
		
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [list]

			$validateContent = file_get_contents('../tpl/smsvalidation/smsvalidation_validated_template.html');
			$validateContent = str_replace(
					array('{NUMBERS}'),
					array($validatedNumbersContent), $validateContent);

		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		return $validateContent;
	}
	
	/**
	 * Render the invalidate page.
	 * @param string $phoneNumber phone number
	 * @return string $validateContent Complete output HTML data
	 */
	public function renderInvalidate($phoneNumber){
		
		$validateContent = '';
		
		// set the token and validity we have buffered in our session
		$this->setTokenData();
		if ($this->auth->hasValidToken()){

			$accessToken = $this->auth->getAccessToken();
			
//! [invalidate]
$requestObj = $this->client->invalidateNumber($phoneNumber, $accessToken);
switch ($requestObj->getStatus()->getStatusCode()){	
	
	// success
	case '0000':
		break;
	
	// all others
	default:
		throw new TelekomException('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [invalidate]

			$validateContent = file_get_contents('../tpl/smsvalidation/smsvalidation_last_template.html');
			$validateContent = str_replace(
					array('{NUMBER}'),
					array($phoneNumber), $validateContent);
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
		
		// reset the session data
		$_SESSION['__devgarden__'] = array();
		
		return $validateContent;
	}
	
	/**
	 * Sets the token data from buffered session.
	 */
	private function setTokenData(){
		if (isset($_SESSION['__devgarden__']['accessToken']) && isset($_SESSION['__devgarden__']['accessTokenValidUntil'])){
			$this->auth->setAccessToken($_SESSION['__devgarden__']['accessToken']);
			$this->auth->setAccessTokenValidUntil($_SESSION['__devgarden__']['accessTokenValidUntil']);
		}
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleValidateSmsSample();
$sample->simpleValidateSms();

?>