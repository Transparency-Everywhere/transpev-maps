<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the AutoScout24 specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/autoscout24/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Sample class to find autoscout24 articles.
 */
class GetMakeModelTreeSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Autoscout 24 Client Object handles.
	 * @var AutoScout24Client
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'production',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */		
		
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');
		
// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new AutoScout24Client($this->service);
//! [init]
	}
	
	/**
	 * This main function gets the model tree data and prints the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function showModelTree(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Autoscout24 PHP SDK - GetMakeModelTreeSample';
		
		try {
			// get the sub templates
			$mainModelTreeTemplate = file_get_contents('../tpl/autoscout24/model_tree_main_template.html');
			$modelTreeTemplate = file_get_contents('../tpl/autoscout24/model_tree_template.html');			
			
//! [getmakemodeltree_response]
$requestObj = $this->findModelTreeData();
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		
		// get the look up data content
		$modelTreeContent = $this->renderModelTreeData($requestObj->getModelTreeData(),
			$mainModelTreeTemplate, $modelTreeTemplate);
		
		break;
		
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException ('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [getmakemodeltree_response]
			
			// render the main content template
			$mainContent = str_replace(
					array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
					array($topHead, $modelTreeContent), $mainTemplate);			
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}

	/**
	 * Find model tree data.
	 * @return ModelTreeDataObject
	 */
	public function findModelTreeData(){
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()){			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
			// The container for the send parameters
//! [getmakemodeltree]
$sendParameters = new ModelTreeParameters($this->config->getCultureId());
return $this->client->findModelTreeData($accessToken, $sendParameters);
//! [getmakemodeltree]
		}
		else {
			throw new TelekomException('Token is not valid in ' . __METHOD__. ' - line ' . __LINE__);
		}
	}
	
	/**
	 * Render the model tree data content part.
	 * @param ModelTreeDataObject
	 * @param string $mainModelTreeTemplate
	 * @param string $modelTreeTemplate
	 */
//! [render]
public function renderModelTreeData($modelTreeData, $mainModelTreeTemplate, $modelTreeTemplate){
	
	$brandContent = '';
	if (!empty($modelTreeData) && is_array($modelTreeData)){
		foreach ($modelTreeData as $brandObj){
			
			// get all models
			$modelContent = '';
			$models = $brandObj->getModels();
			if (is_array($models)){
				foreach ($models as $model){
					$modelContent .= str_replace(
						array('{MODEL_ID}',
							'{MODEL_NAME}',
							'{BRAND_NAME}'),
						array($model->getId(),
							htmlspecialchars($model->getText()),
							htmlspecialchars($brandObj->getBrandText())),
						$modelTreeTemplate);
				}
			}
			else {
				$modelContent = 'No model data found.';
			}
			
			// brand data
			$brandContent .= str_replace(
				array('{BRAND_ID}',
					'{BRAND_NAME}',
					'{NUM_MODELS}',
					'{MODEL_DATA}'),
				array($brandObj->getBrandId(),
					htmlspecialchars($brandObj->getBrandText()),
					count($models),
					$modelContent),
				$mainModelTreeTemplate);
		}
	}
	else {
		$brandContent = 'No model tree data found.';
	}
	return $brandContent;
}
//! [render]
}

/**
 * start the sample
 */
$sample = new GetMakeModelTreeSample();
$sample->showModelTree();

?>