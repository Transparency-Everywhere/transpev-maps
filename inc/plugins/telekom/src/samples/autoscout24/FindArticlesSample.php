<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the AutoScout24 specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/autoscout24/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to find autoscout24 articles.
 */
class FindArticlesSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Autoscout 24 Client Object handles.
	 * @var AutoScout24Client
	 */
	private $client = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'production',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
		
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');
		
// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new AutoScout24Client($this->service);
//! [init]
	}
	
	/**
	 * This main function gets the model tree data and prints the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function showVehicles(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Autoscout24 PHP SDK - FindArticlesSample';
		
		try {
			
//! [query]
$requestObj = $this->findVehicles();
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		$vehicleTemplate = file_get_contents('../tpl/autoscout24/vehicle_template.html');
		
		// get the vehicle content
		$vehicleContent = $this->renderVehicles($requestObj, $vehicleTemplate);
		
		// render the main content template
		$mainContent = str_replace(
			array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
			array($topHead, $vehicleContent), $mainTemplate);
		break;
		
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// no data found
	case '5000':
		echo 'No vehicles found.';
		break;
		
	// all others
	default:
		throw new TelekomException ('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [query]
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}

	/**
	 * Find and get the articles / vehicles. 
	 * @return FindArticlesDataObject
	 */
	public function findVehicles(){
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()){			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
			// The container for the search parameters
//! [params]
$brands = array(13);
			
$searchParameters = new VehicleSearchParameters($this->config->getCultureId());
$searchParameters->setBrands($brands); // e.g. the brand BMW
$searchParameters->setAddress(null, "100", "20359", "D"); // e.g. the ZIP of Hamburg, 100 km radius
//$searchParameters->setAddress(array('D', 'NL'), null, null, null); // find all cars from D and NL
$searchParameters->setKilowatt(110, null); // e.g. from 110 KW (= 150 PS)
$searchParameters->setPricePublic(4800, null, null); // e.g. from 4800 EUR

return $this->client->findArticles($accessToken, $searchParameters);
//! [params]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}
	
	/**
	 * Render the vehicles content part.
	 * @param FindArticlesDataObject
	 * @param string $vehicleTemplate
	 * @return string $vehicleContent
	 */
//! [render]
public function renderVehicles($requestObj, $vehicleTemplate){
	
	$vehicleContent = '';
	
	// get the root and image path
	$autoscout24Path = $requestObj->getRootPaths()->getAs24UrlRoot();
	$imagePath = $requestObj->getRootPaths()->getImagesMain();
	
	// get the vehicles data array
	$vehicles = $requestObj->getVehicles();
	if (is_array($vehicles)){
		foreach ($vehicles as $vehicle){
			
			// get prices array
			$prices = $vehicle->getPrices();
			
			$vehicleContent .= str_replace(
				array('{VEHICLE_ID}',
					'{PRICE}',
					'{IMAGE}',
					'{LINK}'),
				array($vehicle->getVehicleId(),
					$prices[0]->getValue() . ' ' . $prices[0]->getCurrencyId(),
					$this->buildImage($vehicle->getMediaImages(), $imagePath),
					$this->buildDetailsLink($autoscout24Path, $vehicle->getDetailPageUrl())),
				$vehicleTemplate);
		}
	}
	return $vehicleContent;
}
//! [render]
	
	/**
	 * Helper function to build an link if URL is set.
	 * @param stgring $autoscout24Path
	 * @param stgring $detailsLink
	 * @return string $link
	 */
//! [helper1]
public function buildDetailsLink($autoscout24Path, $detailsLink){
	$link = '';
	if ($detailsLink){
		$link = '<a href="'.$autoscout24Path.'/'.$detailsLink.'" target="_blank">more...</a>';
	}
	return $link;
}
//! [helper1]	
	
	/**
	 * Helper function to build an image tag of the first image.
	 * @param array $images
	 * @return string $image
	 */
//! [helper2]
public function buildImage($images, $imagePath){
	$image = '';
	if (isset($images[0]) && is_object($images[0])){
		$image = '<img src="' . $imagePath . $images[0]->getUri() . '" border="0" alt="" />';
	}
	return $image;
}
//! [helper2]	
}

/**
 * start the sample
 */
$sample = new FindArticlesSample();
$sample->showVehicles();

?>