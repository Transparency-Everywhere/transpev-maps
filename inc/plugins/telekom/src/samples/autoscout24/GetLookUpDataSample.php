<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the AutoScout24 specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/autoscout24/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Sample class to find autoscout24 articles.
 */
class GetLookUpDataSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;
	
	/**
	 * Autoscout 24 Client Object handles.
	 * @var AutoScout24Client
	 */
	private $client = null;
	
	/**
	 * Initialisation in Constructor
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'production',
		 * 'username' 		=> 'myUsername',
		 * 'password' 		=> 'myPassword',
		 */
				
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');
		
// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$this->service = new TelekomJSONService($this->config);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new AutoScout24Client($this->service);
//! [init]
	}
	
	/**
	 * This main function gets the lookup data and prints the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 */
	public function showLookUpData(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Autoscout24 PHP SDK - GetLookUpDataSample';
		
		try {
			
//! [getlookupdata_response]
$requestObj = $this->findLookUpData();
switch ($requestObj->getStatus()->getStatusCode()){
	
	// success
	case '0000':
		$mainLookUpTemplate = file_get_contents('../tpl/autoscout24/look_up_data_main_template.html');
		$lookUpTemplate = file_get_contents('../tpl/autoscout24/look_up_data_template.html');
		
		// get the look up data content
		$lookUpContent = $this->renderLookUpData($requestObj->getLookUpData(), $lookUpTemplate);									

		break;
	
	// no permissions
	case '0093':
		throw new TelekomException('API Service is not activated. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
		break;
	
	// all others
	default:
		throw new TelekomException ('API Request failed. Status Code: '
			. $requestObj->getStatus()->getStatusCode() . ', Message: '
			. $requestObj->getStatus()->getStatusMessage());
}
//! [getlookupdata_response]
			
			$mainLookUpContent = str_replace('{LOOKUP_DATA}', $lookUpContent, $mainLookUpTemplate);
			
			// render the main content template
			$mainContent = str_replace(
					array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
					array($topHead, $mainLookUpContent), $mainTemplate);			
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}

	/**
	 * Find and get the look up data.
	 * @return LookUpDataObject Look up data Object
	 */
	public function findLookUpData(){
		
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()){			
	$accessToken = $this->auth->getAccessToken();
//! [token]
			
			// The container for the request parameters
//! [getlookupdata]			
$sendParameters = new LookUpDataParameters($this->config->getCultureId());
return $this->client->findLookUpData($accessToken, $sendParameters);
//! [getlookupdata]
		}
		else {
			throw new TelekomException('Token is not valid!');
		}
	}
	
	/**
	 * Render the look up data content part.
	 * @param array $lookUpData
	 * @param string $lookUpTemplate
	 * @return string $lookUpContent Rendered HTML content
	 */
//! [render]
public function renderLookUpData($lookUpData, $lookUpTemplate){
	
	$lookUpContent = '';
	if (is_array($lookUpData)){
		foreach ($lookUpData as $data){
			$lookUpContent .= str_replace(
				array('{LOOKUP_ID}',
					'{LOOKUP_NAME}',
					'{LOOKUP_TEXT}'),
				array($data->getId(),
					htmlspecialchars($data->getName()),
					htmlspecialchars($data->getText())),
				$lookUpTemplate);
		}
	}
	return $lookUpContent;
}
//! [render]
}

/**
 * Start the sample.
 */
$sample = new GetLookUpDataSample();
$sample->showLookUpData();

?>