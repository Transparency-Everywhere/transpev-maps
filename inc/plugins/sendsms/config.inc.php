<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Configuration Array including all relevant configuration data.
 * @var array $telekomConfig configuration array
 */
$telekomConfig = array(

	// environment settings
	'environment' 						=> 'sandbox', // the preferred environment (budget / premium / sandbox / mock)

	// OAuth basic configuration - change it, if you use OAuth authentication!
	'oauth_client_id'					=> 'V9EfBdBqT4',
	'oauth_client_secret'				=> '0b7b51bcbedd49568d9a79973b3f50e1-5d65d640cd25f3590b0f996e6a9887ef-6b5243a70db65a1225f220fae382289d',
	'oauth_scope'						=> 'DC0QX4UK',
	
	// OAuth URLs and keys - do not change!
	'api_oauth_url'						=> 'https://global.telekom.com/gcp-web-api',
	'api_oauth_url_authorize'			=> '/oauth',
	'api_oauth_url_tokens'				=> '/oauth',
	'api_oauth_url_revoke'				=> '/oauth',
	'api_oauth_grant_type_auth'			=> 'client_credentials',
	'api_oauth_grant_type_refresh'		=> 'refresh_token',
	
	// required API URLs - do not change!
	'api_base_url' 						=> 'https://gateway.developer.telekom.com',
	'api_sendsms_base_url' 				=> '/plone/sms/rest/%s/smsmessaging/v1',
	'api_sendsms_send_url'				=> '/outbound/%s/requests',
	'api_sendsms_subscribe_url'			=> '/outbound/%s/subscriptions',
	'api_sendsms_unsubscribe_url'		=> '/outbound/subscriptions/%s',
	'api_sendsms_queryreport_url'		=> '/outbound/%s/requests/%s/deliveryInfos',
	'api_sendsms_receive_url'			=> '/inbound/registrations/%s/messages',
	'api_sendsms_receive_subscribe_url'	=> '/inbound/subscriptions',
	'api_sendsms_receive_unsubscribe_url'	=> '/inbound/subscriptions/%s',
);
