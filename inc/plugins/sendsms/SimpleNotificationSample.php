<?php
/*
 * This file is part of the Telekom PHP SDK
 * Copyright 2012 Deutsche Telekom AG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Include the needed classes.
 */
//! [include]
foreach (array('data', 'client') as $part){
	// Include the Basic SDK Telekom classes
	foreach (glob(dirname(__FILE__).'/../../src/common/' . $part . '/*.php') as $file) {
		require_once($file);
	}
	// Include the SendSMS specific client and data classes
	foreach (glob(dirname(__FILE__).'/../../src/sendsms/' . $part . '/*.php') as $file) {
		require_once($file);
	}
}
//! [include]

/**
 * Example class to send SMS.
 */
class SimpleNotificationSample {
	
	/**
	 * Basic Telekom Configuration Object to get the configuration values.
	 * @var TelekomConfig
	 */
	private $config = null;
	
	/**
	 * JSON Service Object handles the API requests.
	 * @var TelekomJSONService
	 */
	private $service = null;
	
	/**
	 * Auth Object handles the authentification.
	 * @var TelekomUPAuth
	 */
	private $auth = null;

	/**
	 * Send SMS Client Object
	 * @var SendSmsClient
	 */
	private $client = null;
	private $client2 = null;

	/**
	 * Initialisation in Constructor.
	 */
	public function __construct(){
		
		/**
		 * Require the configuration file.
		 * Please complete the Basic Configuration data!
		 * Thats what you need to change:
		 * 
		 * 'environment' 	=> 'sandbox',
		 * 'clientId' 		=> 'myClientId',
		 * 'clientSecret' 	=> 'myClientSecret',
		 */
//! [init]
require_once(dirname(__FILE__).'/config.inc.php');

// get and set the configuration
$this->config = new TelekomConfig($telekomConfig);

// init the service class
$additionalProxyOptions = array(
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_SSLVERSION => 3,
	/*CURLOPT_PROXY => '<yourproxy>',*/
);
$this->service = new TelekomJSONService($this->config, $additionalProxyOptions);

// init the Authentification class
$this->auth = new TelekomOAuth($this->service);

// Init the Client class
$this->client = new NotificationSubscribeClient($this->service);

$this->client2 = new NotificationUnsubscribeClient($this->service);
//! [init]
	}
	
	/**
	 * This main function send a SMS and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleNotificationSubscribeSample(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Send SMS PHP SDK - SimpleNotificationSample';
		
		try {
//! [token]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token]
	
				// The container for the send parameters
//! [send]
$sendParameters = new NotificationSubscriptionParameters();

// required settings
$sendParameters->setSenderAddress('0191011');
$sendParameters->setNotifyURL('http://dieseurlgibtes.net');
// additional options available

// subscribe to notifications
$requestObj = $this->client->subscribeNotifications($accessToken, $sendParameters);

if ($requestObj->getSuccess()){
		$message = "Erfolgreich.<pre>";
		foreach ($requestObj->getDeliveryReceiptSubscription() as $key => $value) {
			if (!is_array($value)) {
				$message .= "\n<b>" . $key . "</b>: " . $value;
			} else {
				$message .= "\n<b>" . $key . "</b>: "; 
				foreach ($value as $innerKey1 => $innerVal1) {
					if(!is_array($innerVal1)) {
						$message .= "\n\t<b>" . $innerKey1 . "</b>: " . $innerVal1 . "";
					} else {
						$message .= "\n<b>" . $innerKey1 . "</b>: ";
						foreach ($innerVal1 as $innerKey2 => $innerVal2) {
							$message .= "\n\t\t<b>" . $innerKey2 . "</b>: " . $innerVal2 . "";
						}
					}
				}
			}
		}
		$message .= "</pre>";
} else {
	throw new TelekomException('There was an Error: ' . $requestObj->getCompiledOutput());
}
//! [send]
				
				$mainContent = str_replace(
						array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
						array($topHead, $message), $mainTemplate);				
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}


	/**
	 * This main function send a SMS and print the results.
	 * The integrated view helper is to show you the result a litte more nice.
	 * You have to implement your specified view into it or modify this.
	 * @return string $mainContent Complete output HTML data
	 */
	public function simpleNotificationUnsubscribeSample(){
		
		// get the main template for output
		$mainTemplate = file_get_contents('../tpl/main_template.html');
		$topHead = 'Send SMS PHP SDK - SimpleNotificationSample';
		
		try {
//! [token2]
$this->auth->requestAccessToken();

if ($this->auth->hasValidToken()) {			
	$accessToken = $this->auth->getAccessToken();
//! [token2]
	
				// The container for the send parameters
//! [send2]
$sendParameters = new NotificationUnsubscriptionParameters();

// required settings
$sendParameters->setSubscriptionId('123456');
// additional options available

// unsubscribe from notifications
$requestObj = $this->client2->unsubscribeNotifications($accessToken, $sendParameters);

if ($requestObj->getSuccess()){
		$message = "Erfolgreich.";
} else {
	throw new TelekomException('There was an Error: ' . $requestObj->getCompiledOutput());
}
//! [send2]
				
				$mainContent = str_replace(
						array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
						array($topHead, $message), $mainTemplate);				
			}
			else {
				throw new TelekomException('Token is not valid!');
			}
		}
		catch (TelekomException $e){
			$mainContent = str_replace(
				array('{MAIN_HEAD}', '{MAIN_CONTENT}'),
				array($topHead, $e->getMessage()), $mainTemplate);
		}
		
		// output content
		echo $mainContent;
	}
}

/**
 * Start the sample.
 */
$sample = new SimpleNotificationSample();

/**
 * Call either subscribe or unsubscribe for a usage demo
 */
$sample->simpleNotificationSubscribeSample();
// $sample->simpleNotificationUnsubscribeSample();

?>