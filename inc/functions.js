var categories = [];
var locations = [];
function init(){
	initializeNavigation();
	initializeMap();
	loadLocations();
}

function initializeMap(){
	
	map = new OpenLayers.Map("mapWrap");
	var mapnik         = new OpenLayers.Layer.OSM();
	var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
	var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
	var position       = new OpenLayers.LonLat(9.984586,53.510561).transform( fromProjection, toProjection);
	var zoom           = 14; 
	
	map.getNumZoomLevels = function(){
        return 16;
        };
	map.addLayer(mapnik);
	map.setCenter(position, zoom );
}

function addMarker(map, lon, lat){
	
}

function removeMarkers(){
	
}

function removeMarker(id){
	
}


function searchType(){
	//initalized with onkeyup
}

function submitSearch(){
	
}

function initializeNavigation(){
	$.getJSON('api/categories', function(data) {
	 
	  $.each(data, function(key, val) {
	  	
	  	 //get all categories where parent category = 1
	     if(val.category == "1"){
	     	
	     	//check if categorie has children
	     	if(val.children > 0){
	     		
	     		//initialize dropdown
	     		$('#navList').append('<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'+val.title+' <b class="caret"></b></a><ul class="dropdown-menu" id="dropdown_'+val.id+'"></ul></li>');
	     	
	     	}else{
	     		
	     		//load single item into the list
	     		$('#navList').append('<li><a href="#" onclick="showLocations('+val.id+')">'+val.title+'</a></li>');
	     		
	     		
	     	}
	     	
	     }else{
	     	
	     	$('#dropdown_'+val.category+'').append('<li><a href="#" onclick="showLocations('+val.id+')">'+val.title+'</a></li>');
	     	
	     }
	  });
	
	});
	
}

function loadLocations(){
	$.getJSON('api/locations', function(data) {
	 //id category title street streetNo zip city hours lon lat
	  $.each(data, function(key, val) {
	    var info = [];
	    info['id'] = val.id;
	    info['category'] = val.category;
	    info['title'] = val.title;
	    info['street'] = val.street;
	    info['streetNo'] = val.streetNo;
	    info['zip'] = val.zip;
	    info['city'] = val.city;
	    info['hours'] = val.hours;
	    info['lat'] = val.lat;
	    info['lon'] = val.lon;
	    locations[val.id] = info;
	  });
	
	});
}

//adds a marker for a single location
function showLocation(id){
	//define standard osm stuff
	var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
	var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
	
	//define position with loaded lon and lat and create marker
	var position       = new OpenLayers.LonLat(locations[id].lon,locations[id].lat).transform( fromProjection, toProjection);
	var markers = new OpenLayers.Layer.Markers( "Markers" );
	markers.events.register("click", markers, function() {
	  showLocationDetails(id);
	});
	
	//add marker to map
	map.addLayer(markers);
	markers.addMarker(new OpenLayers.Marker(position));
	
}

function showLocationDetails(id){
	var content = '<p>'+locations[id].street+' '+locations[id].streetNo+'<br>'+locations[id].zip+' '+locations[id].city+'</p><p></p><p>'+locations[id].hours+'</p>';
	$('#detailBox header').html(locations[id].title);
	$('#detailBox div').html(content);
	$('#detailBox').slideDown();
}

//adds markers for a category
function showLocations(cat){
	locations.forEach(function(entry) {
    if(entry.category == cat){
    	
    	showLocation(entry.id)
    	console.log("show location with id "+entry.id);
    }
	});
	
}






   function checkContactForm(){
        var name = $('#contactName').val();
        var surname = $('#contactSurname').val();
        var mail = $('#contactMail').val();
        var phone = $('#contactPhone').val();
        var message = $('#contactMessage').val();
        
        var returner;
        if(name.length == 0 || surname.length == 0){
            $('#contactNameRow').addClass('error');
            $('#contactNameHelpline').slideDown();
            returner =  false;
        }
        if(mail.length == 0 && phone.length == 0){
            $('#contactContactRow').addClass('error');
            $('#contactContactHelpline').slideDown();
            returner =  false;
        }
        if(message.length == 0){
            $('#contactMessageRow').addClass('error');
            $('#contactMessageHelpline').slideDown();
            returner =  false;
        }
        
        return returner;
        
    }
