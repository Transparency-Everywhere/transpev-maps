<!DOCTYPE html>
<html>
  <head>
    <title>Wilhelmsmap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="inc/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="inc/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<script src="inc/plugins/openLayers/OpenLayers.js"></script>
	
	
	
<style>
    body{
    	background: rgb(255, 253, 253);
    	color: rgb(122, 122, 122);
    }
    /*open street map kram*/
    img { 
    	max-width: none; }
    
    div.olControlAttribution, div.olControlScaleLine {
          font-family: Verdana;
          font-size: 0.5em;
          bottom: 3px;
    }
    
    h3{
    	line-height: 24.5px;
    }
    
    
    .row{
    	margin-top: 2em;
    }
    
    .borderRadius{
    	-webkit-border-radius: 7px;
		-moz-border-radius: 7px;
		border-radius: 7px;
    }
    
    .boxShadow{
    	-webkit-box-shadow: 2px 2px 7px rgb(114, 114, 114);
    	-moz-box-shadow: 2px 2px 7px rgb(114, 114, 114);
    	box-shadow: 2px 2px 7px rgb(114, 114, 114);
    }
    
    .box{
    	height: 100px;
    	background: #FFFFFF;
    }
    
    .box p, .box h4{
    	margin:10px;
    }
    
</style>
  </head>
  <body>

	<div class="bodywrap">
		<div>
			<div class="row container">
				<div class="span12">
					<h3 style="float:left;">Wilhelmsmap</h3>
					<div class="pull-right" style="margin-top: 15px;">&copy; 2013 <a href="http://transparency-everywhere.com" target="blank" style="color: #54545B;">Transparency Everywhere</a></div>
					<div class="row" style="clear:both">
						<div class="span3 borderRadius box boxShadow">
							<p>Wilhelmsmap</p>
						</div>
						<div class="span3 borderRadius box boxShadow"><p>Wilhelmsmap</p></div>
						<div class="span3 borderRadius box boxShadow"><p>Wilhelmsmap</p></div>
						<div class="span3 borderRadius box boxShadow">
								<center style="margin-top: 30px;"><input type="text" class="input-medium search-query" placeholder="seach"></center>
						</div>
					</div>
					<div class="row">
						<div class="span7 borderRadius boxShadow" id="mainMap">
							<div class="map">
								
								
								
		                                        <div id="mapDivision" style="width:100%; height: 400px; margin:15px;"></div>
												<script>
												
												        map = new OpenLayers.Map("mapDivision");
												        var mapnik         = new OpenLayers.Layer.OSM();
												        var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
												        var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
												        var position       = new OpenLayers.LonLat(9.984586,53.510561).transform( fromProjection, toProjection);
												        var zoom           = 13; 
												 
												
												        var markers = new OpenLayers.Layer.Markers( "Markers" );
												        map.addLayer(markers);
												
												        markers.addMarker(new OpenLayers.Marker(position));
												        map.addLayer(mapnik);
												        map.setCenter(position, zoom );
												</script>
								
								
							</div>
						</div>
						<ul class="nav nav-tabs nav-stacked span5 borderRadius boxShadow" style="margin-left:15px;">
								<li><a href="#">Was suchst du?</a></li>
							    <li><a href="#">&nbsp;<i class="icon-home"></i> Restaurant</a></li>
							    <li><a href="#">&nbsp;<i class="icon-shopping-cart"></i> Laden</a></li>
							    <li><a href="#">&nbsp;<i class="icon-headphones"></i> Club</a></li>
							    <li><a href="#">&nbsp;<i class="icon-trash"></i> M&uuml;llcontainer</a></li>
							    <li><a href="#">&nbsp;<i class="icon-film"></i> Kino</a></li>
							    <li><a href="#">&nbsp;<i class="icon-envelope"></i> Briefkasten</a></li>
						</ul>
					</div>
		</div>
		
	</div>
  </body>
</html>