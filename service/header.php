<?
include("inc/functions.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Wilhelmsmap Kundencenter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Bootstrap -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="../inc/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="../inc/plugins/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
    <style type="text/css">
      html, body, #mapWrap {
          width: 100%;
      }
      
      body{
      	padding-top: 60px;
      }
      
      #mapWrap{
      	z-index: 1;
      	position: absolute;
      	top: 60px;
      	right: 0px;
      	bottom: 0px;
      	left: 0px;
      }
    /*open street map kram*/
    img { max-width: none; }
    
    div.olControlAttribution, div.olControlScaleLine {
          font-family: Verdana;
          font-size: 0.5em;
          bottom: 3px;
    }
    
    
    #search{
    	position: absolute;
		width: 470px;
		left: 50%;
		margin-left: -235px;
		height: 90px;
		background-color: rgba(0,0,0,0.8);
		z-index: 9999;
		top: 20px;
    }
    
    #search input{
    	margin: 10px;
		height: 60px;
		width: 435px;
		font-size:40px;
    }
    
    
    #nav{
    	position: absolute;
		z-index: 9999;
		top:0px;
		width:100%;
		background: #333233;
		height: 60px;
    }
    
    #nav input{
    	margin-top: 15px;
    }
    
    #detailBox{
    	background: #333233;
		width: 150px;
		height: 200px;
		margin-top: 150px;
		border-radius: 3px;
		color: #FFFFFF;
		padding: 5px;
		font-size: 10pt;
		
		position: absolute;
		z-index:9999;
    }
    
    #detailBox > header{
    	font-size: 12pt;
    	margin-bottom: 5px;
    }
    
    
    
    
    
    
    
    

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 30px auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
    </style>
  </head>
  <body>
    <!-- NAVBAR
    ================================================== -->
    <div class="navbar-wrapper" id="nav">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">

        <div class="navbar navbar-inverse" style="background: #333233;border: none;">
          <div class="navbar-inner" style="background: #333233;border: none;">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#"> <img src="../gfx/logo.png" style="height: 40px;margin-right: 7px;">wilhelmsmap.com Kundencenter</a>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <div class="nav-collapse collapse">
              <ul class="nav" id="navList">
              	
              	<?
              	if(false){?>
              	<li class="dropdown">
              		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Kategorien<b class="caret"></b></a>
              		<ul class="dropdown-menu" id="dropdown_10">
              			<li><a href="?action=categories">&Uuml;bersucht - editieren</a></li>
              			<li><a href="?action=categories&subaction=add"&subaction=add>Hinzuf&uuml;gen</a></li></ul>
              	</li>
              	<li class="dropdown">
              		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Locations<b class="caret"></b></a>
              		<ul class="dropdown-menu" id="dropdown_10">
              			<li><a href="?action=locations">&Uuml;bersucht - editieren</a></li>
              			<li><a href="?action=locations&subaction=add"&subaction=add>Hinzuf&uuml;gen</a></li></ul>
              	</li>
              	<li class="dropdown">
              		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Kunden<b class="caret"></b></a>
              		<ul class="dropdown-menu" id="dropdown_10">
              			<li><a href="?action=customers">&Uuml;bersucht - editieren</a></li>
              			<li><a href="?action=customers&subaction=add"&subaction=add>Hinzuf&uuml;gen</a></li>
              		</ul>
              	</li>
              	<!-- here the navigation will be loaded -->
              	<? } ?>
              	
              </ul>
            </div><!--/.nav-collapse -->
            
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->
<div id="contactModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form method="post" id="contactForm" action="handleContact.php" onsubmit="return checkContactForm();" style="margin-bottom: 0px;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Kontakt</h3>
  </div>
  <div class="modal-body">
  	
  	
    
  	<div class="row-fluid">
            <div class="span10">
              <div class="controls controls-row">
                  <div class="control-group" id="contactNameRow">
                    <input class="span6" type="text" placeholder="Name" name="name" id="contactName">
                    <input class="span6" type="text" placeholder="Surname" name="surname" id="contactSurname">
                    <span class="help-inline" id="contactNameHelpline" style="display: none;">Please let us know with whom do we have the pleasure of corresponding?</span>
                  </div>
              </div>
              <div class="controls controls-row">
                  
                  <div class="control-group">
                    <input class="span6" type="text" placeholder="Company" name="company" id="contactCompany">
                    <input class="span6" type="text" placeholder="Department" name="department" id="contactDepartment">
                 </div>
              </div>
              <div class="controls controls-row">
                  
                  <div class="control-group" id="contactContactRow">
                    <input class="span6" type="text" placeholder="Mail" name="mail" id="contactMail">
                    <input class="span6" type="text" placeholder="Phone" name="phone" id="contactPhone">
                    <span class="help-inline" id="contactContactHelpline" style="display: none;">Please give us a chance to get in touch with you.</span>
                  </div>
              </div>
              <div class="controls controls-row">
                  <div class="control-group" id="contactMessageRow">
                    Message:<br>
                    <textarea rows="5" class="span12" id="contactMessage" name="message"></textarea>
                    <span class="help-inline" id="contactMessageHelpline" style="display: none;">What's on your mind?</span>
                  </div>
              </div>
              <div class="controls controls-row"> 
                <script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=6LenE98SAAAAABcK8p7a-U-W6Hpne_gyL31vypme">
	  			</script>
	  			<noscript>
	     		<iframe src="http://www.google.com/recaptcha/api/noscript?k=6LenE98SAAAAABcK8p7a-U-W6Hpne_gyL31vypme" height="300" width="500" frameborder="0"></iframe><br>
	     		<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
	     		<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
	  			</noscript>
              </div>
            </div><!--/span-->
            <div class="span2">
				<!-- <p>side text</p>-->
            </div>
          </div><!--/row-->
  	
  </div>
  <div class="modal-footer">q
    <a class="btn float-left" data-dismiss="modal" aria-hidden="true">Abbrechen</a>
    <input type="submit" value="Abschicken" class="btn btn-info float-right">
  </div>
  </form>
</div>
    	
    <div id="content" class="container">
