<?
include("../inc/config.php");
include("../inc/functions.php");
$action = $_GET[action];
$subaction = $_GET[subaction];
$customers = new customers();

include("header.php");

//proof login
if(!$customers->authorized()){
	
echo "\n"; 
echo "      <form class=\"form-signin\" action=\"login.php\" method=\"post\">\n"; 
echo "        <h2 class=\"form-signin-heading\">Please sign in</h2>\n"; 
echo "        <input type=\"text\" class=\"input-block-level\" name=\"email\" placeholder=\"Email address\">\n"; 
echo "        <input type=\"password\" class=\"input-block-level\" placeholder=\"Password\" name=\"password\" >\n"; 
echo "        <label class=\"checkbox\">\n"; 
echo "          <input type=\"checkbox\" value=\"remember-me\"> Remember me\n"; 
echo "        </label>\n"; 
echo "        <button class=\"btn btn-large btn-primary\" type=\"submit\">Sign in</button>\n"; 
echo "      </form>\n";
	
}else{
	
	$customerData = $customers->get($_SESSION['customer']);
	
	switch($action){
		default:
				echo "<div class=\"hero-unit\">";
					echo"<h2>";
					echo"Willkommen im wilhelmsmap.com Kundencenter, $customerData[salutation] $customerData[surname]";
					echo"</h2>";
					echo"<h3>Ihnen stehen folgende Optionen zur Auswahl:</h3>";
					echo"<ul style=\"list-style:none;\">";
						echo"<li><a href=\"?action=changeContactData\">Ihre Kundendaten &auml;ndern</a></li>";
						echo"<li><a href=\"?action=changeBankInformation\">Ihre Bankdaten &auml;ndern</a></li>";
						echo"<li><a href=\"?action=changePassword\">Ihr Passwort &auml;ndern</a></li>";
						echo"<li><a href=\"#contactModal\" role=\"button\" data-toggle=\"modal\">Kontakt zu uns aufnehmen</a></li>";
					echo"</ul>";
				echo "</div>";
			break;
		case 'changeContactData':
			if(isset($_POST[submit])){
				$customers->update($id, $_POST[salutation] , $_POST[name] , $_POST[surname] , $_POST[street] ,  $_POST[streetNo],  $_POST[zip],  $_POST[city]);
				echo"Ihre Kontaktdaten wurden erfolgreich ge&auml;ndert";
			}else{
			
				$customerData = $customers->get($_SESSION['customer']);
			
				echo"<h2>Editieren Sie Ihre Kontaktdaten</h2>";
				echo'<form action="" method="post">';
				echo"<table>";
					echo"<tr>";
						echo"<td>";
						echo"Anrede";
						echo"</td>";
						echo"<td>";
						
						echo"<select name=\"salutation\">";
							echo"<option value\"$customerData[salutation]\">$customerData[salutation]</option>";
							echo"<option value\"Herr\">Herr</option>";
							echo"<option value\"Frau\">Frau</option>";
						echo"</select>";
						
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
							echo"Name, Vorname";
						echo"</td>";
						echo"<td>";
							echo"<input type=\"text\" name=\"surname\" value=\"$customerData[surname]\">, <input type=\"text\" name=\"name\" value=\"$customerData[name]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
							echo"Stra&szlige + Nr.";
						echo"</td>";
						echo"<td>";
							echo"<input type=\"text\" name=\"street\" value=\"$customerData[street]\"><input type=\"text\" name=\"streetNo\" size=\"2\" value=\"$customerData[streetNo]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"PLZ Ort";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"text\" name=\"zip\" size=\"5\" value=\"$customerData[zip]\"> <input type=\"text\" name=\"city\" value=\"$customerData[city]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"&nbsp;";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td colspan=\"2\">";
						echo"<input type=\"submit\" name=\"submit\" value=\"Speichern\">";
						echo"</td>";
					echo"</tr>";
				echo"</table>";
				echo'</form>';
				}
				
				
			break;
case 'changeBankInformation':
			if(isset($_POST[submit])){
				$customers->updateBankInformation($id, $_POST[bankName],  $_POST[bic], $_POST[iban]);
				echo"Ihre Bankdaten wurden erfolgreich ge&auml;ndert";
			}else{
			
				$customerData = $customers->get($_SESSION['customer']);
			
				echo"<h2>Editieren Sie Ihre Bankdaten</h2>";
				echo'<form action="" method="post">';
				echo"<table>";
					echo"<tr>";
						echo"<td>";
						echo"Bankverbindung";
						echo"</td>";
						echo"<td>";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"Bank";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"text\" name=\"bankName\" value=\"$customerData[bInfo_bankName]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"IBAN";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"text\" name=\"iban\" value=\"$customerData[bInfo_bankIBAN]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"BIC";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"text\" name=\"bic\" value=\"$customerData[bInfo_bankBIC]\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td colspan=\"2\">";
						echo"<input type=\"submit\" name=\"submit\" value=\"Speichern\">";
						echo"</td>";
					echo"</tr>";
				echo"</table>";
				echo'</form>';
				}
				
	
	break;
case 'changePassword':
	
		if(isset($_POST[submit])){
			$newPassword = $customers->updatePassword($_SESSION['customer'], $_POST[password], $_POST[passwordNew]);
			if($newPassword != true){
				echo $newPassword;
			}else{
				echo "Ihr Passwort wurde erfolgreich ge&auml;ndert";
			}
		}else{
				echo"<h2>&Auml;ndern sie Ihr Passwort</h2>";
				echo'<form action="" method="post">';
				echo"<table>";
					echo"<tr>";
						echo"<td>";
						echo"Altes Passwort";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"password\" name=\"oldPassword\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"Neues Passwort";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"password\" name=\"newPassword\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td>";
						echo"Wiederholen";
						echo"</td>";
						echo"<td>";
						echo"<input type=\"password\" name=\"passwordRepeat\">";
						echo"</td>";
					echo"</tr>";
					echo"<tr>";
						echo"<td colspan=\"2\">";
						echo"<input type=\"submit\" name=\"submit\" value=\"Speichern\">";
						echo"</td>";
					echo"</tr>";
				echo"</table>";
				echo'</form>';
			
		}
	break;
case 'logout':
	$_SESSION['customer'] = '';
	$_SESSION['hash'] = '';
	break;
		
	}
}
echo"</div>";
?>
