<?
include("inc/functions.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Wilhelmsmap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OpenLayers Demo</title>
    <!-- Bootstrap -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="inc/functions.js"></script>
    <script src="inc/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="inc/plugins/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<script src="inc/plugins/openLayers/OpenLayers.js"></script>
    <style type="text/css">
      html, body, #mapWrap {
          width: 100%;
      }
      
      #mapWrap{
      	z-index: 1;
      	position: absolute;
      	top: 60px;
      	right: 0px;
      	bottom: 0px;
      	left: 0px;
      }
    /*open street map kram*/
    img { max-width: none; }
    
    div.olControlAttribution, div.olControlScaleLine {
          font-family: Verdana;
          font-size: 0.5em;
          bottom: 3px;
    }
    
    
    #search{
    	position: absolute;
		width: 470px;
		left: 50%;
		margin-left: -235px;
		height: 90px;
		background-color: rgba(0,0,0,0.8);
		z-index: 9999;
		top: 20px;
    }
    
    #search input{
    	margin: 10px;
		height: 60px;
		width: 435px;
		font-size:40px;
    }
    
    
    #nav{
    	position: absolute;
		z-index: 9999;
		top:0px;
		width:100%;
		background: #333233;
		height: 60px;
    }
    
    #nav input{
    	margin-top: 15px;
    }
    
    #detailBox{
    	background: #333233;
		width: 150px;
		height: 200px;
		margin-top: 150px;
		border-radius: 3px;
		color: #FFFFFF;
		padding: 5px;
		font-size: 10pt;
		
		position: absolute;
		z-index:9999;
    }
    
    #detailBox > header{
    	font-size: 12pt;
    	margin-bottom: 5px;
    }
    </style>
    <script>
    	$(document).ready(function(){
    		init();
    	});
    </script>
  </head>
  <body>
    <div id="mapWrap"></div>
    <!-- 
    <div id="search">
    	<input type="text" value="gastronomie">
    </div> -->
    
    
    
    <!-- NAVBAR
    ================================================== -->
    <div class="navbar-wrapper" id="nav">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">

        <div class="navbar navbar-inverse" style="background: #333233;border: none;">
          <div class="navbar-inner" style="background: #333233;border: none;">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#"> <img src="gfx/logo.png" style="height: 40px;margin-right: 7px;">wilhelmsmap</a>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <div class="nav-collapse collapse">
              <ul class="nav" id="navList">
              	
              	<!-- here the navigation will be loaded -->
              	
              </ul>
           	  <input type="text" class="search-query pull-right" placeholder="Supermarkt">
            </div><!--/.nav-collapse -->
            
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->
    	<div class="container">
    		<div id="detailBox">
    			<header>El Cornholio</header>
    			<div>
    				<p>Dingsstra&szlig;e 4<br>12345 Wilhelmsburg</p>
    				<p></p>
    				<p>08 - 20 Uhr</p>
    			</div>
    		</div>
    	</div>
    
    
    
    
  </body>
</html>