<?php
  require_once('inc/plugins/reCaptcha/recaptchalib.php');
  
  $privatekey = "6LenE98SAAAAABvEUbWg6pdhtPhTEYgqV0I5uCrw";
  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
								
                if (!$resp->is_valid) {
                    // What happens when the CAPTCHA was entered incorrectly
                    echo ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                        "(reCAPTCHA said: " . $resp->error . ")");
                } else {
                    $from = ("$_POST[surname], $_POST[name]");
                    $company = ("$_POST[company]");
                    $department = ("$_POST[department]");
                    $mail = ("$_POST[mail]");
                    $phone = ("$_POST[phone]");
                    $messageInput = ("$_POST[message]");
                    
                    $header = "FROM: Transparency-Everywhere.com  <contact@transparency-everywhere.com> \n";
                                    $header .= 'MIME-Version: 1.0' . "\r\n";
                                    $header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                    $empfaenger = "nic@transparency-everywhere.com";
                    $absendername = "Transparency-Everywhere";
                    $absendermail = "contact@transparency-everywhere.com";
                    $betreff = "New Contact Entry From wilhelmsmap.com";
                    $message = "<b>Hey there,<br>
                        youve got a new Message:<br><br></b>
                        From: $from<br>
                        Company: $company<br>
                        Department: $department<br>
                        Mail: $mail<br>
                        Phone: $phone<br>
                        <br>
                        Message:<br>
                        ".nl2br($messageInput)."<br>";
                    if(mail($empfaenger, $betreff, $message, $header)){
                    	$mail_success = true;
                    }
                    }
					
	
if($mail_success){ ?>
<!DOCTYPE html>
<html>
	<head>
    <title>Wilhelmsmap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OpenLayers Demo</title>
    <!-- Bootstrap -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="inc/functions.js"></script>
    <script src="inc/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="inc/plugins/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<script src="inc/plugins/openLayers/OpenLayers.js"></script>
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
    </style>
    </head>
    <body>

    <div class="container">

      <form class="form-signin">
        <h3 class="form-signin-heading">Vielen Dank f&uuml;r Ihre Kontaktanfrage, wir werden Sie so schnell es geht bearbeiten.</h3>
      </form>

    </div> <!-- /container -->
    	
    </body>
	
<? }
?>